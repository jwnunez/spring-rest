package cl.an.tde.file.storage;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import cl.an.tde.entities.PIT;
import cl.an.tde.propiedades.PropiedadSingleton;

import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService {

	private static final Logger LOG = LoggerFactory.getLogger(FileStorageService.class);

	private Path fileStoragePits;
	private Path fileStorageStaging;

	public String getFileStoragePits() {
		return fileStoragePits.toString();
	}
	public Path getFileStoragePitsPath() {
		return fileStoragePits;
	}
	public void setFileStoragePits(String dir) {
		this.fileStoragePits = Paths.get(dir)
				.toAbsolutePath().normalize();
	}

	public String getFileStorageStaging() {
		return fileStorageStaging.toString();
	}
	public Path getFileStorageStagingPath() {
		return fileStorageStaging;
	}
	public void setFileStorageStaging(String dir) {
		this.fileStorageStaging = Paths.get(dir)
				.toAbsolutePath().normalize();
	}

	@Autowired
	public FileStorageService(PropiedadSingleton env) {
		this.fileStoragePits = Paths.get(env.getUploadPits()).toAbsolutePath().normalize();
		this.fileStorageStaging = Paths.get(env.getUploadStaging()).toAbsolutePath().normalize();
		try {
			Files.createDirectories(this.fileStoragePits);
			Files.createDirectories(this.fileStorageStaging);
		} catch (Exception ex) {
			throw new RuntimeException("Could not create the directory where the uploaded files will be stored.", ex);
		}
	}

	public void createDirectory(Path path) {
		LOG.info("-> Create Path: " + path.toString());
		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			throw new RuntimeException("Could not create the directory", e);
		}
	}

	public void deleteDirectory(Path path) {
		LOG.info("-> Delete Path: " + path.toString());
		try {
			FileUtils.deleteDirectory(path.toFile());
		} catch (IOException e) {
			throw new RuntimeException("Could not delete the directory", e);
		}
	}

	public String storeFile(MultipartFile file, Path dir) {
		LOG.info("-> Uploading File: " + file.getOriginalFilename());
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			// Check if the file's name contains invalid characters
			if ( fileName.contains("..") ) {
				throw new RuntimeException("Sorry! Filename contains invalid path sequence " + fileName);
			}
			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = dir.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			
			return fileName;
			
		} catch (IOException ex) {
			throw new RuntimeException("Could not store file " + fileName + ". Please try again!", ex);
		}
	}

	public void createJSONFile(String metadata, Path dir) {
		LOG.info("-> Creating Metadata File..");
		try {
			FileWriter file = new FileWriter(dir.toString() + "/metadata.json");
			file.write(metadata);
			file.flush();
			file.close();
		}
		catch (IOException e) {
			throw new RuntimeException("Could not create metadata file.", e);
		}
	}

	public Resource loadFileAsResource(PIT pit) {
		final Path path = Paths.get(pit.getRuta());
		try {
			Path filePath = path.normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new RuntimeException("File not found " + pit.getRuta());
			}
		} catch (MalformedURLException ex) {
			throw new RuntimeException("File not found " + pit.getRuta(), ex);
		}
	}	

	public Resource loadFileAsResource(String fileName) {
		try {
			Path filePath = this.fileStoragePits.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new RuntimeException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new RuntimeException("File not found " + fileName, ex);
		}
	}

}