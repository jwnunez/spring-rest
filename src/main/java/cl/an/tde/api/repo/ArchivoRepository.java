package cl.an.tde.api.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import cl.an.tde.entities.Archivo;

public interface ArchivoRepository extends CrudRepository<Archivo, Long> {

	@Query("SELECT a FROM Archivo a WHERE a.pit.id = :pitId")
	List<Archivo> getArchivosByPit(@Param("pitId") Long pitId);
	
	@Query("Update Archivo set correcto = true where id = :idArchivo")
	void updateEstadoArchivo(@Param("idArchivo") Long idArchivo);

}
