package cl.an.tde.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cl.an.tde.entities.Propiedad;

public interface PropiedadRepository extends JpaRepository<Propiedad, Integer> {

	@Query("SELECT p "
			+ " FROM Propiedad p"
			+ " WHERE p.key = :key")
	Propiedad getProperty(@Param("key") String key);

}
