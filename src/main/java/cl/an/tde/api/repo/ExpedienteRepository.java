package cl.an.tde.api.repo;

import org.springframework.data.repository.CrudRepository;

import cl.an.tde.entities.Expediente;

public interface ExpedienteRepository extends CrudRepository<Expediente, Long> {

	

}
