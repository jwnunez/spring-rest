package cl.an.tde.api.integracion.rest.api;

import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.ibm.icu.text.SimpleDateFormat;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import cl.an.tde.Controlador;
import cl.an.tde.api.integracion.dto.AdtDTO;
import cl.an.tde.api.integracion.dto.ArchivoExpediente;
import cl.an.tde.api.integracion.dto.Consolidacion;
import cl.an.tde.api.integracion.dto.Contrato;
import cl.an.tde.api.integracion.dto.Documento;
import cl.an.tde.api.integracion.dto.ErrorTrans;
import cl.an.tde.api.integracion.dto.Expediente;
import cl.an.tde.api.integracion.dto.FormatoArchivoDTO;
import cl.an.tde.api.integracion.dto.MetadataDTO;
import cl.an.tde.api.integracion.dto.RespuestaInicioTrans;
import cl.an.tde.api.integracion.dto.RespuestaVerificacion;
import cl.an.tde.api.integracion.dto.SerieDTO;
import cl.an.tde.api.integracion.dto.TransferDTO;
import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.cc.man.ControladorCuadroClasificacion;
import cl.an.tde.api.integracion.rest.cc.pojo.TablaRetencionPojo;
import cl.an.tde.api.integracion.rest.institucion.ControladorInstitucion;
import cl.an.tde.api.integracion.rest.institucion.InstitucionInterface;
import cl.an.tde.api.integracion.rest.institucion.pojo.InstitucionPojo;
import cl.an.tde.api.integracion.rest.repo.us.man.UsuarioServiceInterface;
import cl.an.tde.api.integracion.rest.transfer.ControladorTransfer;
import cl.an.tde.api.integracion.rest.uaa.ControladorUAA;
import cl.an.tde.api.integracion.rest.us.man.ControladorUsuario;
import cl.an.tde.api.integracion.rest.us.pojo.UsuarioPojo;
import cl.an.tde.api.repo.ArchivoRepository;
import cl.an.tde.api.integracion.rest.transfer.TransferRepository;
import cl.an.tde.api.integracion.rest.at.man.ControladorAcuerdoTransferencia;
import cl.an.tde.api.integracion.rest.repo.at.man.AcuerdoTransferenciaRepository;
import cl.an.tde.api.integracion.rest.repo.cc.man.CuadroClasificacionInterface;
import cl.an.tde.api.integracion.rest.repo.pit.PITRepository;
import cl.an.tde.entities.AcuerdoTransferencia;
import cl.an.tde.entities.Archivo;
import cl.an.tde.entities.CuadroClasificacion;
import cl.an.tde.entities.EstadoAcuerdoTransferencia;
import cl.an.tde.entities.Institucion;
import cl.an.tde.entities.PIT;
import cl.an.tde.entities.Usuario;
import cl.an.tde.enums.TareaAdT;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.file.storage.FileStorageService;
import cl.an.tde.propiedades.PropiedadSingleton;
import cl.an.tde.utils.BagIt;
import cl.an.tde.utils.Global;
import cl.an.tde.utils.HashUtil;
import cl.an.tde.utils.JsonUtil;
import cl.an.tde.utils.Respuesta;
import cl.an.tde.utils.ZipUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;

@RestController
@RequestMapping(value = "/api", name = "/api")
public class ControladorAPI extends Controlador {

	@Autowired
	private ControladorUAA uaa;

	@Autowired
	private ControladorTransfer trans;

	@Autowired
	TransferRepository repoTransfer;

	@Autowired
	PITRepository repoPit;

	@Autowired
	ArchivoRepository repoArchivo;

	@Autowired
	ControladorAcuerdoTransferencia acuerdoController;

	@Autowired
	AcuerdoTransferenciaRepository repoAdt;

	@Autowired
	private InstitucionInterface servicioInst;

	@Autowired
	private ControladorCuadroClasificacion cuadro;

	@Autowired
	private CuadroClasificacionInterface cuadroClasificacionInterface;

	@Autowired
	private UsuarioServiceInterface userService;

	@Autowired
	private ServicioBitacora bitacora;

	@Autowired
	private FileStorageService fileService;

	public static final String FORMATO_FECHA = "yyyy-MM-dd";

	@PostMapping("/login")
	public Respuesta<String> login(@RequestHeader(value = "Authorization") String authorization,
			@RequestParam("grant_type") String grantType, @RequestParam("username") String username,
			@RequestParam("password") String password) {

		LOG.info("<< API-Integracion Login Request: " + username);

		return uaa.login(authorization, grantType, username, password);
	}

	@GetMapping(value = "/getseries/{institucionId}")
	public List<SerieDTO> getSeriesByInstitucion(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable Integer institucionId) {
		LOG.info("<< API-Integracion Get Series: " + institucionId);

		return trans.getSeriesByInstitucion(institucionId);
	}

	@GetMapping(value = "/getadts/{serieId}")
	public List<AdtDTO> getADTsBySerie(@RequestHeader(value = "Authorization") String authorization,
			@PathVariable Integer serieId) {
		LOG.info("<< API-Integracion Get ADTs :" + serieId);

		return trans.getADTsBySerie(serieId);
	}

	@GetMapping(value = "/gettrs/{institucionId}")
	public List<TablaRetencionPojo> getTablasRetencionByInstitucion(
			@RequestHeader(value = "Authorization") String authorization, @PathVariable Integer institucionId) {
		LOG.info("<< API-Integracion Get tablas de retención :" + institucionId);
		return cuadro.tablaRetencionByInstitucion(institucionId).getData().getTablaRetencion();
	}

	@GetMapping(value = "/getformats")
	public List<FormatoArchivoDTO> getFormats(@RequestHeader(value = "Authorization") String authorization) {
		LOG.info("<< API-Integracion Get Formats");

		return trans.getFormats();
	}

	@GetMapping(value = "/getmetadata")
	public List<MetadataDTO> getMetadata(@RequestHeader(value = "Authorization") String authorization) {
		LOG.info("<< API-Integracion Get Metadata");

		return trans.getMetadata();
	}

	@PostMapping(value = { "/consolidacion" }, produces = "application/json")
	@ApiOperation(value = "Consolida los archivos transferidos via sftp y cambia el estado del acuerdo", notes = "notas", response = Long.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consolidación de archivos correcta", response = RespuestaVerificacion.class),
			@ApiResponse(code = 313, message = "Error con id de transferencia no encontrada en ingesta", response = ErrorTrans.class),
			@ApiResponse(code = 314, message = "Error con estado del acuerdo no corresponde", response = ErrorTrans.class),
			@ApiResponse(code = 315, message = "Error con id de transferencia no contiene archivos en ingesta", response = ErrorTrans.class),
			@ApiResponse(code = 316, message = "Error con usuario sftp no encontrado", response = ErrorTrans.class),
			@ApiResponse(code = 317, message = "Error con id de transfer no contiene archivos en ingesta", response = ErrorTrans.class),
			@ApiResponse(code = 318, message = "Error con nombre de archivo no encontrado en los archivos declarados", response = ErrorTrans.class),
			@ApiResponse(code = 319, message = "Error con checksum del archivo", response = ErrorTrans.class)})
	public ResponseEntity<RespuestaVerificacion> consolidacion(
			@RequestHeader(value = "Authorization") String authorization, @RequestBody Consolidacion cons) {

		RespuestaVerificacion respuesta = new RespuestaVerificacion();
		List<ErrorTrans> errores = new ArrayList<ErrorTrans>();
		LOG.info("<< API-Integracion -- Consolidación de transferencia");

		UUID uuidPit = cons.getUuidTransfer();

		// cambiar cuando se arregle el header con id usuario
		Long idUsuario = 18L;
		try {
			LOG.info("Consolidación: obteniendo archivos del pit");

			List<PIT> pits = repoPit.obtenerPitByAdT(cons.getAdtId());
			final AcuerdoTransferencia adt = repoAdt.findById(cons.getAdtId()).get();

			if (adt == null) {
				ErrorTrans error = new ErrorTrans();
				error.setCodigo(313);
				error.setMensaje("Error: id de acuerdo no encontrado en ingesta");
				errores.add(error);
				throw new ValidationException();
			}

			if (adt.getEstado().getId() != 10 || adt.getEstado().getId() != 11) {
				ErrorTrans error = new ErrorTrans();
				error.setCodigo(314);
				error.setMensaje(
						"Error: estado del acuerdo debe ser 'Inicio transferencia Prueba' o 'Inicio transferencia Oficial'");
				errores.add(error);
				throw new ValidationException();
			}

			if (pits.isEmpty()) {
				ErrorTrans error = new ErrorTrans();
				error.setCodigo(315);
				error.setMensaje("Error: id de acuerdo no contiene transferencias en ingesta");
				errores.add(error);
				throw new ValidationException();
			}

			Usuario usuario = userService.findById(idUsuario);
			String usuarioSftp = usuario.getUsuariosftp();

			if (usuarioSftp.isEmpty()) {
				ErrorTrans error = new ErrorTrans();
				error.setCodigo(316);
				error.setMensaje("Error: Usuario sftp no encontrado");
				errores.add(error);
				throw new ValidationException();
			}

			// obtiene el primer pit que es el último de la lista
//				PIT primerPit = pits.get(pits.size() - 1);
//				pits.remove(pits.size() - 1);
//				List<Archivo> archivosPrimerPit = repoArchivo.getArchivosByPit(primerPit.getId());
			for (PIT pit : pits) {
				// List<Archivo> archivos = repoArchivo.getArchivosByPit(pit.getId());

//					for (Archivo a : archivos) {
//						Archivo archivoPrimerPit = archivosPrimerPit.stream()
//								.filter(x -> x.getNombre().equals(a.getNombre())).findFirst().get();
//						
//						if (archivoPrimerPit != null) {
//							if (archivoPrimerPit.isCorrecto()) {
//								
//							}
//						}
//					}

				List<Archivo> archivosPit = repoArchivo.getArchivosByPit(pit.getId());
				if (archivosPit.isEmpty()) {
					ErrorTrans error = new ErrorTrans();
					error.setCodigo(317);
					error.setMensaje("Error: id de transfer no contiene archivos declarados en ingesta");
					errores.add(error);
				}

				// agregar perfil del usuario

				String pathPit = "/sftp/".concat(usuarioSftp).concat("/").concat(uuidPit.toString());
				Path path = Paths.get(pathPit).toAbsolutePath().normalize();

				File f = new File(path.toString());

				File[] archivosFisicos = f.listFiles();

//					if (archivosFisicos.length != archivosPit.size()) {
//						ErrorTrans error = new ErrorTrans();
//						error.setCodigo(316);
//						error.setMensaje(
//								"Error: La cantidad de archivos declarados no coincide con la cantidad de archivos transferidos");
//						errores.add(error);
//						throw new ValidationException();
//					}

				for (File archivo : archivosFisicos) {

					String checksumFile = HashUtil.getFileChecksum(archivo);

					Archivo doc = archivosPit.stream().filter(x -> x.getNombre().equals(archivo.getName())).findFirst()
							.get();
					// validar nombres de los archivos
					if (doc == null) {
						ErrorTrans error = new ErrorTrans();
						error.setCodigo(318);
						error.setMensaje("Error: nombre del archivo " + archivo.getName()
								+ " no encontrado en los archivos declarados");
						errores.add(error);
					} else {
						if (!doc.getChecksum().equals(checksumFile)) {
							ErrorTrans error = new ErrorTrans();
							error.setCodigo(319);
							error.setMensaje(
									"Error: checksum del archivo " + archivo.getName() + " no es igual al declarado");
							errores.add(error);
						} else {
							repoArchivo.updateEstadoArchivo(doc.getId());
						}
					}
				}
				if (!errores.isEmpty()) {
					fileService.deleteDirectory(path);

					// PARA acuerdo de tipo prueba si existen errores eliminar carpeta y cambiar
					// estado a aprobado prueba
				
					String detalle = "API-Integracion Consolidación AdT: " + pit.getAdT().getId();
					String observacion = "Adt: " + pit.getAdT().getId()
							+ " consolidación de transferencia erronea, se eliminó la carpeta de la transferencia";

					acuerdoController.workflow(idUsuario.intValue(), TipoAccion.REJECTED.getValor(),
							pit.getAdT().getId(), observacion);
					bitacora.procesoAdT(TipoAccion.REJECTED, pit.getAdT(), usuario, detalle, observacion);
				} else {

					String detalle = "API-Integracion Consolidación";

					detalle = detalle + " AdT: " + pit.getAdT().getId();
					String observacion = "Adt: " + pit.getAdT().getId() + " consolidación de transferencia realizada";

					acuerdoController.workflow(idUsuario.intValue(), TipoAccion.SEND.getValor(), pit.getAdT().getId(),
							observacion);
					bitacora.procesoAdT(TipoAccion.SEND, pit.getAdT(), usuario, detalle, observacion);
				}
			}

			respuesta.setMensaje("Verificación realizada correctamente");
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		} catch (ValidationException ex) {
			respuesta.setMensaje("Error en verificación de archivos");
			respuesta.setErrores(errores);
			LOG.error("Error en API-Integración Consolidación de transferencia: " + ex.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setMensaje("Error interno del servicio");
			respuesta.setErrores(errores);
			LOG.error("Error en API-Integración Consolidación de transferencia");
			LOG.error(e.getLocalizedMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
	}

	@PostMapping(value = { "/inicioTransferencia" }, produces = "application/json")
	@ApiOperation(value = "Genera un contrato para el inicio de transferencia", notes = "notas", response = Long.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Registro correcto", response = RespuestaInicioTrans.class),
			@ApiResponse(code = 300, message = "error de negocio", response = ErrorTrans.class),
			@ApiResponse(code = 301, message = "Error serie o adt no encontrado", response = ErrorTrans.class),
			@ApiResponse(code = 302, message = "Error con intitución no encontrada en ingesta", response = ErrorTrans.class),
			@ApiResponse(code = 303, message = "Error con institución no habilitada en ingesta", response = ErrorTrans.class),
			@ApiResponse(code = 304, message = "Error con usuario no pertenece a la institución especificada", response = ErrorTrans.class),
			@ApiResponse(code = 305, message = "Error con serie especificada no contiene acuerdos", response = ErrorTrans.class),
			@ApiResponse(code = 306, message = "Error en contrato, no contiene documentos ni expedientes", response = ErrorTrans.class),
			@ApiResponse(code = 307, message = "Error con metadata del documento", response = ErrorTrans.class),
			@ApiResponse(code = 308, message = "Error con metadata del expediente", response = ErrorTrans.class),
			@ApiResponse(code = 309, message = "Error con metadata del documento perteneciente al expediente", response = ErrorTrans.class),
			@ApiResponse(code = 310, message = "Error en el servicio al intentar crear el pit de transferencia", response = ErrorTrans.class) })
	public ResponseEntity<RespuestaInicioTrans> inicioTransferencia(
			@RequestHeader(value = "Authorization") String authorization, @RequestBody Contrato contrato) {

		RespuestaInicioTrans respuesta = new RespuestaInicioTrans();
		List<ErrorTrans> errores = new ArrayList<ErrorTrans>();
		LOG.info("<< API-Integracion -- Inicio transferencia ");
		ObjectMapper mapper = new ObjectMapper();

//        LOG.info("------------ Decode JWT ------------");
//        String[] split_string = authorization.split("\\.");
//        String base64EncodedHeader = split_string[0];
//        String base64EncodedBody = split_string[1];
//        String base64EncodedSignature = split_string[2];
//
//        LOG.info("~~~~~~~~~ JWT Header ~~~~~~~");
//        Base64 base64Url = new Base64(true);
//        String header = new String(base64Url.decode(base64EncodedHeader));
//        LOG.info("JWT Header : " + header);
//
//
//        LOG.info("~~~~~~~~~ JWT Body ~~~~~~~");
//        String body = new String(base64Url.decode(base64EncodedBody));
//        LOG.info("JWT Body : "+body); 

		try {
			LOG.info("<< API-Integracion -- Contrato: " + mapper.writeValueAsString(contrato));
			Integer idInstitucion = contrato.getIdInstitucion();
			Long codigoSerie = Long.parseLong(contrato.getCodigoSerie());
			Long codigoAdt = Long.parseLong(contrato.getCodigoAcuerdoTransferencia());

			Long idUser = Long.parseLong("18");
			PIT pit = new PIT();
			List<Archivo> la = new ArrayList<>();
			Integer cantidadDocs = 0;

			if (idInstitucion > 0 || codigoSerie > 0 || codigoAdt > 0) {
				Institucion institucion = servicioInst.findById(idInstitucion);
				Usuario usuario = userService.findByIdAndInstitucion(idUser, idInstitucion);

				if (institucion == null) {
					ErrorTrans error = new ErrorTrans();
					error.setCodigo(302);
					error.setMensaje("Institucion no registrado con id especificada");
					errores.add(error);
					throw new ValidationException();
				}
				if (!institucion.getEstado().booleanValue()) {
					ErrorTrans error = new ErrorTrans();
					error.setCodigo(303);
					error.setMensaje("Institución no habilitada en Ingesta");
					errores.add(error);
					throw new ValidationException();
				}

				if (usuario == null) {
					ErrorTrans error = new ErrorTrans();
					error.setCodigo(304);
					error.setMensaje("Usuario no pertenece a la institución con id especificada");
					errores.add(error);
					throw new ValidationException();
				}

				final AcuerdoTransferencia adt = repoAdt.findById(codigoAdt).get();
				final CuadroClasificacion serie = cuadroClasificacionInterface.findById(codigoSerie);

				if (adt == null) {
					ErrorTrans error = new ErrorTrans();
					error.setCodigo(305);
					error.setMensaje("Id de serie especificada no contiene acuerdos de transferencia");
					errores.add(error);
					throw new ValidationException();
				}
				// validar el estado del adt
				if (contrato.getListaDocumentos().isEmpty() && contrato.getListaExpedientes().isEmpty()) {
					ErrorTrans error = new ErrorTrans();
					error.setCodigo(306);
					error.setMensaje("El contrato no contiene documentos ni expedientes");
					errores.add(error);
					throw new ValidationException();
				} else {
					JsonObject metaPit = JsonUtil.jsonAutoMetaPit(institucion.getNombre());
					if (!contrato.getListaDocumentos().isEmpty()) {
						cantidadDocs += contrato.getListaDocumentos().size();
						for (Documento documento : contrato.getListaDocumentos()) {
							List<ErrorTrans> erroresMeta = validarMetadataDocumento(documento);
							if (!erroresMeta.isEmpty())
								errores.addAll(erroresMeta);
							else {
								String jsonMetadata = JsonUtil.jsonAutoMetaDocumento(documento).toString();
								metaPit.add("archivos", JsonUtil.jsonAutoMetaDocumento(documento));

								Archivo a = new Archivo();
								a.setPIT(pit);
								a.setInstitucion(institucion);
								a.setUuid(HashUtil.getUUID());
								a.setNombre(documento.getNombreArchivo());
								a.setTipo(documento.getFormato());
								a.setSize(documento.getVolumen());
								a.setChecksum(documento.getChecksum());
								SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
								Date dateOldFormat = format.parse(documento.getFechaDocumento());
								format.applyPattern(FORMATO_FECHA);
								String newDateString = format.format(dateOldFormat);
								Date dateNewFormat = format.parse(newDateString);
								a.setFecha(dateNewFormat);
								a.setMetadata(jsonMetadata);
								la.add(a);
							}
						}

					}
					if (!contrato.getListaExpedientes().isEmpty()) {
						cantidadDocs += contrato.getListaExpedientes().stream()
								.mapToInt(x -> x.getListaArchivos().size()).sum();
						for (Expediente expediente : contrato.getListaExpedientes()) {
							List<ErrorTrans> erroresExp = validarMetadataExpediente(expediente);
							if (!erroresExp.isEmpty())
								errores.addAll(erroresExp);
							else {
								JsonObject metaExp = JsonUtil.jsonAutoMetaExpediente(expediente);
								Long volumenArchivos = expediente.getListaArchivos().stream()
										.mapToLong(x -> x.getVolumen()).sum();

								if (volumenArchivos != expediente.getVolumenExpediente()) {
									ErrorTrans error = new ErrorTrans();
									error.setCodigo(309);
									error.setMensaje("Volumen del expediente: " + expediente.getTituloExpediente()
											+ " es distinto a la suma del volumen de sus archivos");
									errores.add(error);
								} else {
									if (!expediente.getListaArchivos().isEmpty()) {

										for (ArchivoExpediente archivo : expediente.getListaArchivos()) {
											List<ErrorTrans> erroresArchExp = validarMetadataArchivoExpediente(archivo,
													expediente.getTituloExpediente());
											if (!errores.isEmpty())
												errores.addAll(erroresArchExp);
											else {
												String metadataArchivo = JsonUtil.jsonAutoMetaExpedienteArchivo(archivo)
														.toString();
												metaExp.addProperty("archivos", metadataArchivo);

												Archivo a = new Archivo();
												a.setPIT(pit);
												a.setInstitucion(institucion);
												a.setUuid(HashUtil.getUUID());
												a.setNombre(archivo.getNombreArchivo());
												a.setTipo(archivo.getFormato());
												a.setSize(archivo.getVolumen());
												a.setChecksum(archivo.getChecksum());
												SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
												Date dateOldFormat = format.parse(archivo.getFechaDocumento());
												format.applyPattern(FORMATO_FECHA);
												String newDateString = format.format(dateOldFormat);
												Date dateNewFormat = format.parse(newDateString);
												a.setFecha(dateNewFormat);
												a.setMetadata(metadataArchivo);
												la.add(a);
											}
										}
									}

									metaPit.add("expedientes", metaExp);
								}
							}
						}
						;
					}
					if (!errores.isEmpty()) {
						throw new ValidationException();
					}

					LOG.info("Metadata PIT: " + metaPit.toString());
					LOG.info("Creando transfer");
					final UUID uuid = HashUtil.getUUID();

					pit.setArchivos(la);
					pit.setUuid(uuid);
					pit.setRepo(null);
					pit.setInstitucion(institucion);
					pit.setUsuario(usuario);

					String detalle = "API-Integracion inicio de transferencia";

					detalle = detalle + " AdT: " + adt.getId();
					pit.setAdT(adt);
					String observacion = "Adt: " + adt.getId()
							+ " inicio de transferencia de archivos API-integracion realizada";

					acuerdoController.workflow(idUser.intValue(), TipoAccion.CREATE.getValor(), codigoAdt, observacion);
					bitacora.procesoAdT(TipoAccion.SEND, adt, usuario, detalle, observacion);

					pit.setSerie(serie.getSerie());
					pit.setNivel(serie.getNivelDescripcion().getNombre());
					pit.setDocs(cantidadDocs);
					pit.setMetadata(metaPit.toString());
					pit.setFecha(new Date());

					repoTransfer.save(pit);

					if (pit.getId() == null || pit.getId() == 0) {
						ErrorTrans error = new ErrorTrans();
						error.setCodigo(310);
						error.setMensaje("Error al intentar crear el PIT para la transferencia");
						errores.add(error);
					} else {
						respuesta.setUuidTrans(pit.getUuid());
						respuesta.setErrores(null);
					}
				}

			} else {
				ErrorTrans error = new ErrorTrans();
				error.setCodigo(301);
				error.setMensaje("Serie o adt vacio");
				errores.add(error);
				throw new ValidationException();
			}
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		} catch (ValidationException ex) {
			respuesta.setUuidTrans(null);
			respuesta.setErrores(errores);
			LOG.error("Error en API-Integración Inicio de transferencia: " + ex.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setUuidTrans(null);
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(501);
			error.setMensaje("Error interno del servicio: " + e.getLocalizedMessage());
			errores.clear();
			errores.add(error);
			respuesta.setErrores(errores);
			LOG.error("Error en API-Integración Inicio de transferencia");
			LOG.error(e.getLocalizedMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
	}

	private List<ErrorTrans> validarMetadataDocumento(Documento documento) {
		final String ARCHIVO = "Archivo: " + documento.getNombreArchivo();
		List<ErrorTrans> errores = new ArrayList<>();
		if (documento.getComuna() == null || documento.getComuna().isEmpty() || documento.getComuna() == "undefined") {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Comuna es obligatorio");
			errores.add(error);

		}
		if (documento.getDocumentoDigitalizado() == null || documento.getDocumentoDigitalizado().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Documento Digitalizado es obligatorio");
			errores.add(error);
		}
		if (documento.getEtiquetas() == null || documento.getEtiquetas().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Etiquetas es obligatorio");
			errores.add(error);

		}
		if (documento.getFechaDocumento() == null || documento.getFechaDocumento().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Fecha documento es obligatorio");
			errores.add(error);

		}
		if (documento.getFormato() == null || documento.getFormato().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Formato es obligatorio");
			errores.add(error);

		}
		if (documento.getIdentificador() == null || documento.getIdentificador().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Identificador es obligatorio");
			errores.add(error);

		}
		if (documento.getNivelAcceso() == null || documento.getNivelAcceso().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Nivel de acceso es obligatorio");
			errores.add(error);

		}
		if (documento.getNombreArchivo() == null || documento.getNombreArchivo().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Nombre archivo es obligatorio");
			errores.add(error);

		}
		if (documento.getRegion() == null || documento.getRegion().isEmpty() || documento.getRegion() == "undefined") {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(307);
			error.setMensaje(ARCHIVO + " metadato Región es obligatorio");
			errores.add(error);
		}

		return errores;
	}

	private List<ErrorTrans> validarMetadataExpediente(Expediente expediente) {
		final String EXPEDIENTE = "Expediente: " + expediente.getTituloExpediente();
		List<ErrorTrans> errores = new ArrayList<>();
		if (expediente.getComuna() == null || expediente.getComuna().isEmpty()
				|| expediente.getComuna() == "undefined") {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(308);
			error.setMensaje(EXPEDIENTE + " metadato Comuna es obligatorio");
			errores.add(error);

		}
		if (expediente.getFechaCreacionExpediente() == null || expediente.getFechaCreacionExpediente().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(308);
			error.setMensaje(EXPEDIENTE + " metadato fecha creación es obligatorio");
			errores.add(error);
		}
		if (expediente.getEtiquetas() == null || expediente.getEtiquetas().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(308);
			error.setMensaje(EXPEDIENTE + " metadato Etiquetas es obligatorio");
			errores.add(error);

		}
		if (expediente.getFechaFinalizacionExpediente() == null
				|| expediente.getFechaFinalizacionExpediente().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(308);
			error.setMensaje(EXPEDIENTE + " metadato Fecha finalización es obligatorio");
			errores.add(error);

		}
		if (expediente.getVolumenExpediente() == 0) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(308);
			error.setMensaje(EXPEDIENTE + " metadato Volumen es obligatorio");
			errores.add(error);

		}
		if (expediente.getNivelAcceso() == null || expediente.getNivelAcceso().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(308);
			error.setMensaje(EXPEDIENTE + " metadato Nivel de acceso es obligatorio");
			errores.add(error);

		}
		if (expediente.getRegion() == null || expediente.getRegion().isEmpty()
				|| expediente.getRegion() == "undefined") {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(308);
			error.setMensaje(EXPEDIENTE + " metadato Región es obligatorio");
			errores.add(error);
		}

		return errores;
	}

	private List<ErrorTrans> validarMetadataArchivoExpediente(ArchivoExpediente archivo, String nombreExpediente) {
		List<ErrorTrans> errores = new ArrayList<>();
		final String ARCHIVO_EXP = "Archivo: " + archivo.getNombreArchivo() + " del expediente: " + nombreExpediente;

		if (archivo.getDocumentoDigitalizado() == null || archivo.getDocumentoDigitalizado().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(309);
			error.setMensaje(ARCHIVO_EXP + " metadato Documento digitalizado es obligatorio");
			errores.add(error);
		}
		if (archivo.getFechaDocumento() == null || archivo.getFechaDocumento().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(309);
			error.setMensaje(ARCHIVO_EXP + " metadato fecha documento es obligatorio");
			errores.add(error);
		}
		if (archivo.getVolumen() == 0) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(309);
			error.setMensaje(ARCHIVO_EXP + " metadato Volumen es obligatorio");
			errores.add(error);
		}
		if (archivo.getFormato() == null || archivo.getFormato().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(309);
			error.setMensaje(ARCHIVO_EXP + " metadato formato es obligatorio");
			errores.add(error);
		}
		if (archivo.getTitulo() == null || archivo.getTitulo().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(309);
			error.setMensaje(ARCHIVO_EXP + " metadato Título es obligatorio");
			errores.add(error);
		}

		if (archivo.getChecksum() == null || archivo.getChecksum().isEmpty()) {
			ErrorTrans error = new ErrorTrans();
			error.setCodigo(309);
			error.setMensaje(ARCHIVO_EXP + " metadato Checksum es obligatorio");
			errores.add(error);
		}

		return errores;
	}

}