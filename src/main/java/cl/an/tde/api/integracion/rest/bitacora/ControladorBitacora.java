package cl.an.tde.api.integracion.rest.bitacora;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cl.an.tde.Controlador;
import cl.an.tde.api.integracion.dto.BitacoraDTO;
import cl.an.tde.api.integracion.dto.BusquedaDTO;
import cl.an.tde.api.integracion.rest.bitacora.repo.BitacoraServiceInterface;
import cl.an.tde.entities.Bitacora;
import cl.an.tde.utils.Respuesta;

@RestController	
@RequestMapping(value = "/bitacora", name = "/bitacora")
public class ControladorBitacora extends Controlador{
	
	private static final Logger LOG = LoggerFactory.getLogger(ControladorBitacora.class);

	@Autowired
	private BitacoraServiceInterface bitacoraService;

	@GetMapping("/listarAll")
	public Respuesta<List<Bitacora>> listar() {
		LOG.info("listar bitacora");
		Respuesta<List<Bitacora>> respuesta = new Respuesta<List<Bitacora>>((List<Bitacora>) bitacoraService.findAll(), 0);
		return respuesta;
	}

	@PostMapping("/listFilter")
	public Respuesta<List<BitacoraDTO>> listFilter(@RequestBody BusquedaDTO params) {
		LOG.info("-> listFilter:");
		LOG.info(params.toString());
		final List<BitacoraDTO> result = bitacoraService.findFilter(params);
		final Respuesta<List<BitacoraDTO>> response = new Respuesta<List<BitacoraDTO>>(result, 200);
		
		return response;
	}

	@PostMapping("/observaciones")
	public Respuesta<List> observaciones(@RequestParam("idAdt") Long idAdt) {
		final List result = bitacoraService.observaciones(idAdt);
		return new Respuesta<>(result, 200);
	}
}
