package cl.an.tde.api.integracion.rest.repo.dt.man;

import org.springframework.data.repository.CrudRepository;

import cl.an.tde.entities.TipoDocumento;

public interface TipoDocumentoRepository extends CrudRepository<TipoDocumento, Long> {

}
