package cl.an.tde.api.integracion.rest.at.pojo;

import cl.an.tde.entities.Contraparte;

import javax.persistence.Column;

public class ContrapartePojo {

    private Integer id;
    private String nombre;
    private String cargo;
    private String correo;
    private String telefono;

    /**
     * Constructor.
     */
    public ContrapartePojo() {
        super();
    }

    /**
     * Constructor con parametros.
     *
     * @param c {@link Contraparte}
     */
    public ContrapartePojo(Contraparte c) {
        this.id = c.getId();
        this.nombre = c.getNombre();
        this.cargo = c.getCargo();
        this.correo = c.getCorreo();
        this.telefono = c.getTelefono();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Contraparte obtenerContraparte() {
        Contraparte c = new Contraparte();
        c.setId(this.id);
        c.setNombre(this.nombre);
        c.setCargo(this.cargo);
        c.setCorreo(this.correo);
        c.setTelefono(this.telefono);
        return c;
    }
}
