package cl.an.tde.api.integracion.rest.dt.pojo;

import java.util.Date;

import cl.an.tde.entities.TipoDocumento;
import cl.an.tde.utils.ObjectUtil;

/**
 * @author rfuentes
 */
public class TipoDocumentoPojo {

    private Long id;
    private String nombre;
    private String descripcion;
    private Date fechaCreacion;
    private Boolean estado;

    /**
     * constructor.
     */
    public TipoDocumentoPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param id {@link Long}
     */
    public TipoDocumentoPojo(Long id) {
        super();
        this.id = id;
    }

    /**
     * Constructor con parametros.
     * 
     * @param td {@link TipoDocumento}
     */
    public TipoDocumentoPojo(TipoDocumento td) {
        super();
        this.id = td.getId();
        this.nombre = td.getNombre();
        this.descripcion = td.getDescripcion();
        this.fechaCreacion = td.getFechaCreacion();
        this.estado = td.getEstado();
    }

    public TipoDocumento getTipoDocumento() {
        final TipoDocumento td = new TipoDocumento();
        td.setId(this.id);
        td.setNombre(this.nombre);
        td.setDescripcion(this.descripcion);
        td.setFechaCreacion(ObjectUtil.isNull(this.fechaCreacion) ? new Date() : this.fechaCreacion);
        td.setEstado(this.estado);
        return td;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
