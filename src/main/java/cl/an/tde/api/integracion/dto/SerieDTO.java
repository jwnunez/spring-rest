package cl.an.tde.api.integracion.dto;

import java.io.Serializable;

public class SerieDTO implements Serializable {

	private static final long serialVersionUID = -1508812002704180513L;

	private Integer id;
	private String serie;
	private String nivel;
	private Long idTRD;
	private Integer aniosAG;
	private Integer aniosACI;
	private Integer aniosAN;

	public SerieDTO() {
		super();
	}

	public SerieDTO(Integer id, String serie, String nivel, Long idTRD, Integer aniosAG, Integer aniosACI,
			Integer aniosAN) {
		super();
		this.id = id;
		this.serie = serie;
		this.nivel = nivel;
		this.idTRD = idTRD;
		this.aniosAG = aniosAG;
		this.aniosACI = aniosACI;
		this.aniosAN = aniosAN;
	}
	
	public SerieDTO(Long id, String serie, String nivel, Long idTRD, Integer aniosAG, Integer aniosACI,
			Integer aniosAN) {
		super();
		this.id = id.intValue();
		this.serie = serie;
		this.nivel = nivel;
		this.idTRD = idTRD;
		this.aniosAG = aniosAG;
		this.aniosACI = aniosACI;
		this.aniosAN = aniosAN;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public Long getIdTRD() {
		return idTRD;
	}
	public void setIdTRD(Long idTRD) {
		this.idTRD = idTRD;
	}

	public Integer getAniosAG() {
		return aniosAG;
	}
	public void setAniosAG(Integer aniosAG) {
		this.aniosAG = aniosAG;
	}

	public Integer getAniosACI() {
		return aniosACI;
	}
	public void setAniosACI(Integer aniosACI) {
		this.aniosACI = aniosACI;
	}

	public Integer getAniosAN() {
		return aniosAN;
	}
	public void setAniosAN(Integer aniosAN) {
		this.aniosAN = aniosAN;
	}

}
