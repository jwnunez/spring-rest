package cl.an.tde.api.integracion.rest.us.man;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.exception.BusinessException;
import cl.an.tde.api.integracion.rest.repo.us.man.UsuarioServiceInterface;
import cl.an.tde.api.integracion.rest.us.pojo.UsuarioPojo;
import cl.an.tde.entities.Usuario;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;

/**
 * @author rfuentes
 */
@RestController
@RequestMapping(value = "/usuario", name = "/usuario")
@CrossOrigin(origins = "*")
public class ControladorUsuario implements Serializable {

	private static final long serialVersionUID = -8092921168005541229L;
	private static final Logger LOG = LoggerFactory.getLogger(ControladorUsuario.class);

	@Autowired
	private UsuarioServiceInterface usuarioService;

	@Autowired
	ServicioBitacora bitacora;

    @PostMapping("/listar")
    public Respuesta<List<Usuario>> listar(@RequestBody FiltroUsuario fu) {
        LOG.info("listar usuarios");
        final List<Usuario> find = (List<Usuario>) usuarioService.fetchUsuarios(fu.getRut(), fu.getaPaterno(),
                fu.getaMaterno(), fu.getNombreUsuario(), fu.getInstitucionId());
        final Respuesta<List<Usuario>> respuesta = new Respuesta<List<Usuario>>(find, 0);
        return respuesta;
    }

    @PostMapping("/listarFiltro")
    public Respuesta<List<Usuario>> listarFiltro(@RequestBody FiltroUsuario fu) {
        LOG.info("listar usuarios");
        final List<Usuario> find = (List<Usuario>) usuarioService.findAll(fu);
        final Respuesta<List<Usuario>> respuesta = new Respuesta<List<Usuario>>(find, 0);
        return respuesta;
    }

    @GetMapping("/listarAll")
    public Respuesta<List<Usuario>> listar() {
        LOG.info("listar usuarios");
        Respuesta<List<Usuario>> respuesta = new Respuesta<List<Usuario>>((List<Usuario>) usuarioService.findAll(), 0);
        return respuesta;
    }

    @GetMapping(value = "/get/{id}")
    public Respuesta<UsuarioPojo> obtenerUsuarioById(@PathVariable Long id) {
        final Usuario usuario = (Usuario) usuarioService.findById(id);
        return new Respuesta<UsuarioPojo>(new UsuarioPojo(usuario), 0);
    }

	@PutMapping(value = "/update")
	public Respuesta<UsuarioPojo> actualizar(@RequestHeader(value="User") Integer user, @RequestBody UsuarioPojo usuario) {
		usuarioService.save(usuario.obtenerUsuario());
		bitacora.crear(TipoAccion.UPDATE, user, "Usuario:" + usuario.getNombreUsuario());
		return new Respuesta<UsuarioPojo>(usuario, 0);
	}

    @PostMapping(value = "/save")
    public Respuesta<Boolean> crear(@RequestHeader(value="User") Integer user, @RequestBody UsuarioPojo usuarioPojo) {
        Usuario usuario = usuarioPojo.obtenerUsuario();
        final Boolean editar = !ObjectUtil.isNull(usuario.getId()) ? Boolean.TRUE : Boolean.FALSE;
        if (ObjectUtil.isNull(usuarioPojo.getPassword())) {
            final Usuario u = usuarioService.findById(usuario.getId());
            usuario.setPass(u.getPass());
        }
        usuarioService.save(usuario);
        bitacora.crear(editar ? TipoAccion.UPDATE: TipoAccion.CREATE, user, "Usuario:" + usuario.getNombreUsuario());
        return new Respuesta<Boolean>(Boolean.TRUE, 0);
    }

	@PutMapping(value = "/delete/{id}")
	public Respuesta<Boolean> actualizarEstado(@RequestHeader(value="User") Integer idUser, @PathVariable Long id) {
		Usuario usuario = usuarioService.findById(id);
		usuario.setEstado(!usuario.getEstado());
		usuarioService.save(usuario);
		bitacora.crear((usuario.getEstado())? TipoAccion.ACTIVATE : TipoAccion.DISABLE, idUser, "Usuario:" + usuario.getNombreUsuario());
		return new Respuesta<Boolean>(Boolean.TRUE, 0);
	}

    @GetMapping(value = "/rutExiste/{rut}")
    public Respuesta<Boolean> rutExiste(@PathVariable String rut) {
        final Boolean estado = usuarioService.rutExiste(rut);
        return new Respuesta<Boolean>(estado, 0);
    }

    @GetMapping(value = "/nombreUsuarioExiste/{nombreUsuario}")
    public Respuesta<Boolean> nombreUsuarioExiste(@PathVariable String nombreUsuario) {
        final Boolean estado = usuarioService.nombreUsuarioExiste(nombreUsuario);
        return new Respuesta<Boolean>(estado, 0);
    }

    @GetMapping("/error")
    public Respuesta<Usuario> error() {
        LOG.info("Prueba para validar error");
        if (true ) {
            throw new BusinessException("Error esperado................");
        }

        final Respuesta<Usuario> respuesta = new Respuesta<Usuario>(new Usuario(), 0);
        return respuesta;

    }

    @GetMapping(value = "/obtenerDataUsuario/{user}")
    public Respuesta<UsuarioPojo> obtenerDataUsuario(@PathVariable String user) {
        final UsuarioPojo usuario = usuarioService.obtenerDataUsuario(user);
        if (ObjectUtil.isNull(usuario) ) {
            throw new BusinessException("Usuario y/o Password incorrectos.");
        }
        return new Respuesta<UsuarioPojo>(usuario, 0);
    }
    
}
