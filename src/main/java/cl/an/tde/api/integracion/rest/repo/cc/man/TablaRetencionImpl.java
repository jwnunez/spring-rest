package cl.an.tde.api.integracion.rest.repo.cc.man;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.an.tde.entities.TablaRetencion;

/**
 * @author rfuentes
 *
 */
@Service
public class TablaRetencionImpl implements TablaRetencionInterface {

    @Autowired
    private TablaRetencionRepository tablaRetencionRepository;

    @Override
    public TablaRetencion findById(Long id) {
        return tablaRetencionRepository.findById(id).get();
    }

    @Override
    public TablaRetencion save(TablaRetencion tr) {
        return tablaRetencionRepository.save(tr);
    }
}
