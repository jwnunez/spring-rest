package cl.an.tde.api.integracion.rest.bitacora;

public class FiltroBitacora {
	
	public Integer idInstitucion;
	public String fechaDesde;
	public String fechaHasta;
	
	public FiltroBitacora() {
		super();
	}

	public FiltroBitacora(Integer idInstitucion) {
		super();
		this.idInstitucion = idInstitucion;
	}

	public Integer getidInstitucion() {
		return idInstitucion;
	}

	public void setidInstitucion(Integer idInstitucion) {
		this.idInstitucion = idInstitucion;
	}

	public String getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public String getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	
	

}
