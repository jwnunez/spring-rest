package cl.an.tde.api.integracion.rest.repo.pit;

import cl.an.tde.entities.PIT;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PitServiceImpl implements PitServiceInterface {

    @Autowired
    private PITRepository pitRepository;

    @Override
    public PIT obtenerPitByAdT(Long idAdT) {
    	List<PIT> lista = (List<PIT>) pitRepository.obtenerPitByAdT(idAdT);
    	if ( lista != null && lista.size() > 0 ) {
    		return lista.get(0);
    	}
        return null;
    }
}
