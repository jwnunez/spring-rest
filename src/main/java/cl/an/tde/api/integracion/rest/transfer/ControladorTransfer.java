package cl.an.tde.api.integracion.rest.transfer;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import cl.an.tde.Controlador;
import cl.an.tde.api.integracion.dto.AdtDTO;
import cl.an.tde.api.integracion.dto.FormatoArchivoDTO;
import cl.an.tde.api.integracion.dto.MetadataDTO;
import cl.an.tde.api.integracion.dto.NivelDTO;
import cl.an.tde.api.integracion.dto.SerieDTO;
import cl.an.tde.api.integracion.dto.TransferDTO;
import cl.an.tde.api.integracion.rest.at.man.ControladorAcuerdoTransferencia;
import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.institucion.InstitucionRepository;
import cl.an.tde.api.integracion.rest.repo.at.man.AcuerdoTransferenciaRepository;
import cl.an.tde.api.integracion.rest.repo.us.man.UsuarioRepository;
import cl.an.tde.api.repo.ArchivoRepository;
import cl.an.tde.entities.AcuerdoTransferencia;
import cl.an.tde.entities.Archivo;
import cl.an.tde.entities.EstadoAcuerdoTransferencia;
import cl.an.tde.entities.Institucion;
import cl.an.tde.entities.PIT;
import cl.an.tde.entities.Usuario;
import cl.an.tde.enums.TareaAdT;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.file.storage.FileStorageService;
import cl.an.tde.utils.BagIt;
import cl.an.tde.utils.Global;
import cl.an.tde.utils.HashUtil;
import cl.an.tde.utils.JsonUtil;
import cl.an.tde.utils.ZipUtil;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/transfer", name = "/transfer")
public class ControladorTransfer extends Controlador {
//	private static final Logger LOG = LoggerFactory.getLogger(ControladorTransfer.class);

	@Autowired
	TransferRepository repoTransfer;
	
	@Autowired
	InstitucionRepository repoInst;
	
	@Autowired
	ControladorAcuerdoTransferencia acuerdoController;
	
	@Autowired
	UsuarioRepository repoUser;
	
	@Autowired
	AcuerdoTransferenciaRepository repoAdt;
	
	@Autowired
	TransferServices servicio;

	@Autowired
	private ServicioBitacora bitacora;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	ArchivoRepository repoFile;

	@GetMapping(value = "/getseries/{institucionId}")
	public List<SerieDTO> getSeriesByInstitucion(@PathVariable Integer institucionId) {
		LOG.info("-> /getseries/" + institucionId);
		final List<SerieDTO> cc = servicio.obtenerSeriesByInstitucion(institucionId);
		return cc;
	}

	@GetMapping(value = "/getniveles/{institucionId}")
	public List<NivelDTO> getNivelesByInstitucion(@PathVariable Integer institucionId) {
		LOG.info("-> /getniveles/" + institucionId);
		final List<NivelDTO> list = servicio.obtenerNivelesByInstitucion(institucionId);
		return list;
	}

	@GetMapping(value = "/getadts/{serieId}")
	public List<AdtDTO> getADTsBySerie(@PathVariable Integer serieId) {
		LOG.info("-> /getadts/" + serieId);
		final List<AdtDTO> list = servicio.obtenerADTsBySerie(serieId);
		return list;
	}

	@GetMapping(value = "/getformats")
	public List<FormatoArchivoDTO> getFormats() {
		LOG.info("-> /getFormats");
		final List<FormatoArchivoDTO> m = servicio.obetenerFormatosActivos();
		return m;
	}

	@GetMapping(value = "/getmetadata")
	public List<MetadataDTO> getMetadata() {
		LOG.info("-> /obtenerMetadata/");
		final List<MetadataDTO> m = servicio.obtenerMetadata();
		return m;
	}

	@PostMapping("/upload")
	public TransferDTO uploadFiles(
			@RequestHeader(value="User") Integer idUser,
			@RequestParam("files") MultipartFile[] files,
			@RequestParam("chks") String[] chks,
			@RequestParam("serie") String serie,
			@RequestParam("nivel") String nivel,
			@RequestParam("meta") String metadata,
			@RequestParam("adt") Long idAdt) {
		
		LOG.info("---Start---UploadFiles-Service: {}", Global.SERVICE_ID);
		
		final Optional<Usuario> user = repoUser.findById(idUser.longValue());
		final Institucion inst = user.get().getInstitucion();
		
		LOG.info("-> User={} InstCode={}", user.get().getNombreUsuario(), inst.getCodigo());
		LOG.info("-> Files={} Chks={}", files.length, chks.length);
		LOG.info("-> AdT={} Serie={}", idAdt, serie);
		LOG.info("-> Metadata: {}", metadata);
		
		if ( !HashUtil.verifyMD5Checksum(files, chks) ) {
			return null;
		}
		final boolean withAdt = (idAdt != -1) ? true : false;
		final String repo = fileStorageService.getFileStoragePits();
		final UUID uuid = HashUtil.getUUID();
		final Path pathPit = Paths.get(repo.concat("/").concat(uuid.toString()));
		
		fileStorageService.createDirectory(pathPit);
		
		JsonObject metaPit = JsonUtil.jsonAutoMetaPit(inst.getNombre());
		JsonArray metaFiles = new JsonArray();
		
		if ( withAdt ) {
			metaFiles = JsonUtil.stringToJsonArray(metadata);
			metaPit.add("archivos", metaFiles);
		} else {
			metaPit.add("archivos", JsonUtil.stringToJson(metadata));
		}
		
		PIT pit = new PIT();
		List<Archivo> la = new ArrayList<>();
		
		for (int i=0; i<files.length; i++) {
			
			fileStorageService.storeFile(files[i], pathPit);
			
			Archivo a = new Archivo();
			a.setPIT(pit);
			a.setInstitucion(inst);
			a.setUuid(HashUtil.getUUID());
			a.setNombre(files[i].getOriginalFilename());
			a.setTipo(files[i].getContentType());
			a.setSize(files[i].getSize());
			a.setChecksum(HashUtil.getMD5File(files[i]));
			a.setFecha(new Date());
			if ( withAdt ) {
				a.setMetadata(metaFiles.get(i).toString());
				metaFiles.set(i, JsonUtil.jsonAutoMetaFile(a));
				a.setMetadata(metaFiles.get(i).toString());
			}
//			repoFile.save(a);
			la.add(a);
		}
		
		fileStorageService.createJSONFile(metaPit.toString(), pathPit);
		
		BagIt.createBag(pathPit);
		
		try {
			ZipUtil.packagePIT(pathPit.toString());
		} catch (IOException e) {
			LOG.info(e.getMessage());
			return null;
		}
		
		fileStorageService.deleteDirectory(pathPit);
		
		pit.setArchivos(la);
		pit.setUuid(uuid);
		pit.setRepo(repo);
		pit.setInstitucion(user.get().getInstitucion());
		pit.setUsuario(user.get());
		if ( withAdt ) {
			final AcuerdoTransferencia adt = repoAdt.findById(idAdt).get();
			final EstadoAcuerdoTransferencia estado = adt.getEstado();
			final TareaAdT tt = TareaAdT.getTareaAdT(estado.getId());
			String detalle = "";
			
			switch (tt) {
			case TRANSFERENCIAS_PRUEBA:
				detalle = "Transferencia de Prueba";
				break;
			case TRANSFERENCIA_OFICIAL:
				detalle = "Transferencia Oficial";
				pit.setOficial(true);
				break;
			case RECHAZADA_TRANSFERENCIA_OFICIAL:
				detalle = "Transferencia Oficial posterior a Rechazo";
				pit.setOficial(true);
				break;
			default:
				break;
			}
			detalle = detalle + " AdT: " + adt.getId();
			pit.setAdT(adt);
			String observacion = "Adt: " + adt.getId() + " transferencia de archivos realizada";
			acuerdoController.workflow(idUser, TipoAccion.SEND.getValor(), idAdt, observacion);
			bitacora.procesoAdT(TipoAccion.SEND, adt, user.get(), detalle, null);
		}
		pit.setSerie(serie);
		pit.setNivel(nivel);
		pit.setDocs(files.length);
		pit.setMetadata(metaPit.toString());
		pit.setFecha(new Date());
		
		repoTransfer.save(pit);
		
		final String detail = String.format("PIT UUID:%s Archivos:%d", pit.getUuid().toString(), files.length);
		bitacora.crear(TipoAccion.CREATE, idUser, detail);
		
		LOG.info("---Finish--UploadFiles-Service: {}", Global.SERVICE_ID);
		
		return new TransferDTO(pit);
	}

}