package cl.an.tde.api.integracion.rest.repo.at.man;

import cl.an.tde.api.integracion.rest.at.pojo.AcuerdoTransferenciaPojo;
import cl.an.tde.entities.AcuerdoTransferencia;
import cl.an.tde.entities.EstadoAcuerdoTransferencia;
import cl.an.tde.enums.TareaAdT;
import cl.an.tde.enums.TipoAccion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * @author rfuentes
 */
@Service
public class AcuerdoTransferenciaImpl implements AcuerdoTransferenciaInterface {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private AcuerdoTransferenciaRepository acuerdoTransferenciaRepository;

	@Override
	public AcuerdoTransferencia findById(Long acuerdoId) {
		return acuerdoTransferenciaRepository.findById(acuerdoId).get();
	}

	@Override
	public void save(AcuerdoTransferencia acuerdo) {
		acuerdoTransferenciaRepository.save(acuerdo);
	}

	@Override
	public List<AcuerdoTransferenciaPojo> listar(AcuerdoTransferenciaPojo params) {
		final StringBuilder select = new StringBuilder(
				"select new cl.an.tde.api.integracion.rest.at.pojo.AcuerdoTransferenciaPojo(at) ");
		select.append(" FROM AcuerdoTransferencia at");
		select.append(" LEFT JOIN at.cuadroClasificacion cc ");
		select.append(" WHERE cc.id = :cuadroClasificacionId ");

		this.getQueryBusqueda(params, select);
		final Query query = em.createQuery(select.toString());
		this.setParametrosQuery(params, query);

		final List<AcuerdoTransferenciaPojo> lista = (List<AcuerdoTransferenciaPojo>) query.getResultList();
		return lista;
	}

	private void setParametrosQuery(AcuerdoTransferenciaPojo params, Query query) {
		if (params.getEstado() != null) {
			query.setParameter("estadoId", params.getEstado());
		}
		query.setParameter("cuadroClasificacionId", params.getCuadroClasificacionId());

		if (params.getFechaTransferirInicio() != null) {
			query.setParameter("fechaDesde", params.getFechaTransferirInicio());
		}
		if (params.getFechaTransferirTermino() != null) {
			query.setParameter("fechaHasta", params.getFechaTransferirTermino());
		}
	}

	private void getQueryBusqueda(AcuerdoTransferenciaPojo params, StringBuilder select) {
		if (params.getEstado() != null) {
			select.append(" AND at.estado.id = :estadoId ");
		}
		if (params.getFechaTransferirInicio() != null) {
			select.append(" AND at.fechaSesion >= :fechaDesde");
		}
		if (params.getFechaTransferirTermino() != null) {
			select.append(" AND at.fechaSesion <= :fechaHasta");
		}
		select.append(" ORDER BY at.fechaTransferirTermino ASC");
	}

	@Override
	public void workflowAdT(TipoAccion ta, AcuerdoTransferencia adt) {
		final EstadoAcuerdoTransferencia estado = adt.getEstado();
		final TareaAdT tt = TareaAdT.getTareaAdT(estado.getId());

		switch (tt) {
		case BORRADOR:
			if (ta.equals(TipoAccion.SEND)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.PROPUESTA.getValor()));
			}
			break;
		case PROPUESTA:
			if (ta.equals(TipoAccion.APPROVED)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.TRANSFERENCIAS_PRUEBA.getValor()));
			}
			if (ta.equals(TipoAccion.REJECTED)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.RECHAZADA_PROPUESTA.getValor()));
			}
			break;
		case TRANSFERENCIAS_PRUEBA:
			if (ta.equals(TipoAccion.SEND)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.TRANSFERENCIA_ENVIADA_PRUEBA.getValor()));
			}
			if (ta.equals(TipoAccion.CREATE)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.INICIO_TRANSFERENCIA_PRUEBA.getValor()));
			}
			break;
		case TRANSFERENCIA_ENVIADA_PRUEBA:
			if (ta.equals(TipoAccion.APPROVED)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.TRANSFERENCIA_OFICIAL.getValor()));
			}
			break;
		case RECHAZADA_PROPUESTA:
			if (ta.equals(TipoAccion.SEND)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.PROPUESTA.getValor()));
			}
			break;
		case TRANSFERENCIA_OFICIAL:
			if (ta.equals(TipoAccion.SEND)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.TRANSFERENCIA_ENVIADA_OFICIAL.getValor()));
			}
			if (ta.equals(TipoAccion.CREATE)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.INICIO_TRANSFERENCIA_OFICIAL.getValor()));
			}
			break;
		case TRANSFERENCIA_ENVIADA_OFICIAL:
			if (ta.equals(TipoAccion.APPROVED)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.APROBADA_TRANSFERENCIA_OFICIAL.getValor()));
			}
			if (ta.equals(TipoAccion.REJECTED)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.RECHAZADA_TRANSFERENCIA_OFICIAL.getValor()));
			}
			break;
		case INICIO_TRANSFERENCIA_PRUEBA:
			if (ta.equals(TipoAccion.SEND)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.TRANSFERENCIA_ENVIADA_PRUEBA.getValor()));
			}
			if (ta.equals(TipoAccion.REJECTED)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.TRANSFERENCIAS_PRUEBA.getValor()));
			}
			break;
		case INICIO_TRANSFERENCIA_OFICIAL:
			if (ta.equals(TipoAccion.SEND)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.TRANSFERENCIA_ENVIADA_OFICIAL.getValor()));
			}
			if (ta.equals(TipoAccion.REJECTED)) {
				adt.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.RECHAZADA_TRANSFERENCIA_OFICIAL.getValor()));
			}
			break;
			
		default:
			break;
		}

		acuerdoTransferenciaRepository.save(adt);

	}

}
