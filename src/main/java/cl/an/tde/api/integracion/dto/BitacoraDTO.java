package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.util.Date;

import cl.an.tde.entities.Bitacora;

public class BitacoraDTO implements Serializable {

	private static final long serialVersionUID = -6204261504963899086L;

	private Date fecha;
	private Integer idInstitucion;
	private String institucion;
	private Integer idUsuario;
	private String usuario;
	private Integer tipoAccion;
	private String accion;
	private String detalle;
	private String observacion;

	public BitacoraDTO(Bitacora b) {
		this.fecha = b.getFecha();
		this.idInstitucion = b.getIdInstitucion();
		this.institucion = b.getInstitucion();
		this.idUsuario = b.getIdUsuario();
		this.usuario = b.getUsuario();
		this.tipoAccion = b.getTipoAccion();
		this.accion = b.getAccion();
		this.detalle= b.getDetalle();
		this.observacion = b.getObservacion();
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Integer getIdInstitucion() {
		return idInstitucion;
	}
	public void setIdInstitucion(Integer idInstitucion) {
		this.idInstitucion = idInstitucion;
	}
	public String getInstitucion() {
		return institucion;
	}
	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(Integer tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}
