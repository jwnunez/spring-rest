package cl.an.tde.api.integracion.rest.institucion;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import cl.an.tde.entities.Institucion;
import cl.an.tde.entities.Usuario;

public interface InstitucionRepository extends CrudRepository<Institucion, Integer> {

	Institucion findByCodigo(String codigo);

}
