package cl.an.tde.api.integracion.rest.repo.dt.man;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.an.tde.entities.TipoDocumento;

/**
 * @author rfuentes.
 */
@Service
public class TipoDocumentoServiceImpl implements TipoDocumentoServiceInterface {

    @Autowired
    private TipoDocumentoRepository tipoDocumentoRepository;

    @Override
    public List<TipoDocumento> findAll() {
        return (List<TipoDocumento>) tipoDocumentoRepository.findAll();
    }

    @Override
    public TipoDocumento findById(Long id) {
        TipoDocumento td = tipoDocumentoRepository.findById(id).get();
        return td;
    }

    @Override
    public void save(TipoDocumento tipoDocumento) {
        tipoDocumentoRepository.save(tipoDocumento);
    }

}
