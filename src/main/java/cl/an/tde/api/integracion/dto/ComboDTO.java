package cl.an.tde.api.integracion.dto;

import java.io.Serializable;

import cl.an.tde.entities.UnidadInstalacion;
import cl.an.tde.entities.Usuario;

public class ComboDTO implements Serializable {

	private static final long serialVersionUID = 5658347980788638551L;

	private Integer key;
	private String value;

	public ComboDTO() {
		super();
	}

	public ComboDTO(Usuario u) {
		super();
		this.key = u.getId().intValue();
		this.value = u.getNombreUsuario();
	}

	public ComboDTO(UnidadInstalacion ui) {
		super();
		this.key = ui.getId().intValue();
		this.value = ui.getNombre();
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
