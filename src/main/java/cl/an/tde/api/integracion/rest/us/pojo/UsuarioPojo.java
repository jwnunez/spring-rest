package cl.an.tde.api.integracion.rest.us.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.an.tde.api.integracion.rest.institucion.pojo.InstitucionPojo;
import cl.an.tde.entities.Institucion;
import cl.an.tde.entities.Rol;
import cl.an.tde.entities.Usuario;
import cl.an.tde.utils.ObjectUtil;

public class UsuarioPojo implements Serializable {

    private static final long serialVersionUID = -2157499916551214439L;

    private String aMaterno;
    private String aPaterno;
    private String confirmarPassword;
    private String correo;
    private Boolean estado;
    private Integer institucionId;
    private String nombres;
    private String nombreUsuario;
    private String password;
    private String rut;
    private Long id;
    private Date fechaCreacion;
    private InstitucionPojo institucion;
    private List<RolPojo> roles;

    /**
     * Constructor.
     */
    public UsuarioPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param u {@link Usuario}
     */
    public UsuarioPojo(final Usuario u) {
        super();
        this.aMaterno = u.getaMaterno();
        this.aPaterno = u.getaPaterno();
        this.correo = u.getCorreo();
        this.estado = u.getEstado();
        if (!ObjectUtil.isNull(u.getInstitucion()) ) {
            this.institucionId = u.getInstitucion().getId();
            this.institucion = new InstitucionPojo(u.getInstitucion());
        }
        this.nombres = u.getNombres();
        this.nombreUsuario = u.getNombreUsuario();
        this.rut = u.getRut();
        this.id = u.getId();
        this.fechaCreacion = u.getFechaCreacion();
        if (!ObjectUtil.isNull(u.getRoles()) ) {
            final List<RolPojo> roles = new ArrayList<>();
            for (Rol r : u.getRoles()) {
                roles.add(new RolPojo(r));
            }
            this.roles = roles;
        }
    }

    public Usuario obtenerUsuario() {
        Usuario u = new Usuario();
        u.setaMaterno(this.aMaterno);
        u.setaPaterno(this.aPaterno);
        u.setCorreo(this.correo);
        u.setEstado(this.estado);
        u.setInstitucion(new Institucion(this.institucionId));
        u.setNombres(this.nombres);
        u.setNombreUsuario(this.nombreUsuario);
        u.setPass(this.password);
        u.setRut(this.rut);
        u.setId(this.id);
        u.setFechaCreacion(!ObjectUtil.isNull(fechaCreacion) ? this.fechaCreacion : new Date());
        if (!ObjectUtil.isNull(this.roles)) {
            List<Rol> roles = new ArrayList<>();
            for (RolPojo rp : this.roles) {
                Rol r = new Rol();
                r.setId(rp.getId());
                r.setRol(rp.getRol());
                roles.add(r);
            }
            if (roles.isEmpty()) {
                Rol r = new Rol();
                r.setId(2L);
                r.setRol("USER");
                roles.add(r);
            }
            u.setRoles(roles);
        }
        return u;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getConfirmarPassword() {
        return confirmarPassword;
    }

    public void setConfirmarPassword(String confirmarPassword) {
        this.confirmarPassword = confirmarPassword;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getInstitucionId() {
        return institucionId;
    }

    public void setInstitucionId(Integer institucionId) {
        this.institucionId = institucionId;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() { return password; }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public InstitucionPojo getInstitucion() {
        return institucion;
    }

    public void setInstitucion(InstitucionPojo institucion) {
        this.institucion = institucion;
    }

    public List<RolPojo> getRoles() {
        return roles;
    }

    public void setRoles(List<RolPojo> roles) {
        this.roles = roles;
    }

}
