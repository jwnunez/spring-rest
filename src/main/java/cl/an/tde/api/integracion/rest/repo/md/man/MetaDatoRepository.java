package cl.an.tde.api.integracion.rest.repo.md.man;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import cl.an.tde.entities.MetaDato;

public interface MetaDatoRepository extends CrudRepository<MetaDato, Integer> {

	List<MetaDato> findByNombre(String nombre);

	Optional<MetaDato> findById(Integer id);

	@Query("SELECT a FROM MetaDato a WHERE a.nombre=:nombre and a.tipoDato =:tipoDato")
	List<MetaDato> fetchMetaDato(@Param("nombre") String nombre, @Param("tipoDato") Integer tipoDatoId);

	@Query("SELECT a.nombre FROM MetaDato a WHERE a.archivo=true and a.automatico=false order by a.nombre ASC")
	List<MetaDato> getMetadataForFile();
	
	@Query("SELECT a.nombre FROM MetaDato a WHERE a.expediente=true and a.automatico=false order by a.nombre ASC")
	List<MetaDato> getMetadataForFileExp();
}
