package cl.an.tde.api.integracion.pojo;

import java.io.Serializable;

/**
 * @author rfuentes
 *
 */
public class Comun implements Serializable {

	private static final long serialVersionUID = 8088505290928295602L;
	private Number id;
	private String nombre;
	private String llave;
	private Boolean activado;

	/**
	 * Constructor.
	 */
	public Comun() {
		super();
	}

	public Comun(final Long id, final String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public Comun(final Integer id, final String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public Comun(final String llave, final String nombre) {
		super();
		this.llave = llave;
		this.nombre = nombre;
	}

	public Comun(final Long id, final String nombre, final Boolean activado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.activado = activado;
	}

	public Comun(final Integer id, final String nombre, final Boolean activado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.activado = activado;
	}

	public Comun(final String llave, final String nombre, final Boolean activado) {
		super();
		this.llave = llave;
		this.nombre = nombre;
		this.activado = activado;
	}

	public Number getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(final String nombre) {
		this.nombre = nombre;
	}

	public String getLlave() {
		return llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public Boolean getActivado() {
		return activado;
	}

	public void setActivado(Boolean activado) {
		this.activado = activado;
	}
}
