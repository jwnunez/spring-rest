package cl.an.tde.api.integracion.rest.at.man;

import cl.an.tde.api.integracion.pojo.Comun;
import cl.an.tde.api.integracion.rest.at.pojo.AcuerdoTransferenciaPojo;
import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.cc.pojo.CuadroClasificacionPojo;
import cl.an.tde.api.integracion.rest.repo.at.man.AcuerdoTransferenciaInterface;
import cl.an.tde.api.integracion.rest.repo.at.man.AcuerdoTransferenciaRepository;
import cl.an.tde.api.integracion.rest.repo.cc.man.CuadroClasificacionInterface;
import cl.an.tde.api.integracion.rest.repo.pit.PitServiceInterface;
import cl.an.tde.api.integracion.rest.repo.us.man.UsuarioRepository;
import cl.an.tde.entities.AcuerdoTransferencia;
import cl.an.tde.entities.Contraparte;
import cl.an.tde.entities.CuadroClasificacion;
import cl.an.tde.entities.PIT;
import cl.an.tde.entities.Usuario;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;
import net.sf.jasperreports.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/acuerdoTransferencia", name = "/acuerdoTransferencia")
@CrossOrigin(origins = "*")
public class ControladorAcuerdoTransferencia {

	private static final Logger LOG = LoggerFactory.getLogger(ControladorAcuerdoTransferencia.class);

	@Autowired
	UsuarioRepository repoUser;

	@Autowired
	private AcuerdoTransferenciaInterface acuerdoTransferenciaInterface;

	@Autowired
	private AcuerdoTransferenciaRepository repoAdt;

	@Autowired
	private CuadroClasificacionInterface cuadroClasificacionInterface;

	@Autowired
	private PitServiceInterface pitServiceInterface;

	@Autowired
	private ServicioBitacora bitacora;

	@Autowired
	private ResourceLoader resourceLoader;

	@PostMapping(value = "/acuerdo")
	public Respuesta<AcuerdoTransferenciaPojo> acuerdoTransferencia(@RequestBody AcuerdoTransferenciaPojo at) {
		final AcuerdoTransferenciaPojo acuerdo = new AcuerdoTransferenciaPojo(
				acuerdoTransferenciaInterface.findById(at.getId()));
		final PIT pit = pitServiceInterface.obtenerPitByAdT(acuerdo.getId());
		if (!ObjectUtil.isNull(pit)) {
			acuerdo.setFechaTransaccionExitosa(pit.getFecha());
			acuerdo.setPitHash(pit.getUuid().toString());
		}
		return new Respuesta<AcuerdoTransferenciaPojo>(acuerdo, 0);
	}

	@PostMapping(value = "/save")
	public Respuesta<Boolean> save(@RequestHeader(value = "User") Integer user,
			@RequestBody AcuerdoTransferenciaPojo at) {
		final AcuerdoTransferencia acuerdo = at.obtenerAcuerdoTransferencia();
		final Boolean editar = !ObjectUtil.isNull(acuerdo.getId()) ? Boolean.TRUE : Boolean.FALSE;
		acuerdoTransferenciaInterface.save(acuerdo);
		bitacora.crear(editar ? TipoAccion.UPDATE : TipoAccion.CREATE, user,
				"Acuerdo Transferencia:" + acuerdo.getId());
		return new Respuesta<Boolean>(Boolean.TRUE, 0);
	}

	@PostMapping(value = "/workflow")
	public Respuesta<Boolean> workflow(@RequestHeader(value = "User") Integer idUser,
			@RequestParam("acc") Integer idAccion, @RequestParam("adt") Long idAdt,
			@RequestParam(name = "obs", required = false) String observacion) {

		final Optional<Usuario> user = repoUser.findById(idUser.longValue());
		final TipoAccion ta = TipoAccion.getTipoAccion(idAccion);
		LOG.info("-> Workflow AdT id={} User={} Action={}", idAdt, user.get().getNombreUsuario(), ta.getNombre());
		final AcuerdoTransferencia adt = repoAdt.findById(idAdt).get();

		acuerdoTransferenciaInterface.workflowAdT(ta, adt);
		final String detalle = "AdT id: " + idAdt + " Usuario: " + user.get().getNombreUsuario();
		bitacora.procesoAdT(ta, adt, user.get(), detalle, observacion);
		return new Respuesta<Boolean>(Boolean.TRUE, 0);
	}

	@PostMapping(value = "/listarAcuerdoTransferencia")
	public Respuesta<List<AcuerdoTransferenciaPojo>> listar(@RequestBody AcuerdoTransferenciaPojo at) {
		List<AcuerdoTransferenciaPojo> listar = acuerdoTransferenciaInterface.listar(at);
		return new Respuesta<List<AcuerdoTransferenciaPojo>>(listar, 0);
	}

	@PostMapping(value = "/subFondos")
	public Respuesta<List<Comun>> subfondos(@RequestBody AcuerdoTransferenciaPojo atp) {
		List<Comun> lista = cuadroClasificacionInterface.subFondos(atp.getInstitucion());
		return new Respuesta<List<Comun>>(lista, 0);
	}

	@PostMapping(value = "/listaCuadroClasificacion")
	public Respuesta<List<CuadroClasificacionPojo>> obtenerSeries(@RequestBody AcuerdoTransferenciaPojo atp) {
		CuadroClasificacion cc = cuadroClasificacionInterface.findById(atp.getCuadroClasificacionId());
		List<CuadroClasificacionPojo> lista = new ArrayList<>();
		cuadroClasificacionInterface.listaCuadroClasificacion(cc, lista);
		return new Respuesta<List<CuadroClasificacionPojo>>(lista, 0);
	}

	@GetMapping(value = "/pdf")
	public void pdf(HttpServletResponse response, HttpServletRequest request) {
		try {
			response.setContentType("application/x-download");
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"at.pdf\""));
			String path = resourceLoader.getResource("classpath:acuerdoTransferencia.jrxml").getURI().getPath();
			JasperReport jasperReport = JasperCompileManager.compileReport(path);
			Map<String, Object> params = new HashMap<>();
			params.put("id", 1);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, params, new JREmptyDataSource());
			JasperExportManager.exportReportToPdfStream(print, response.getOutputStream());

		} catch (JRException e) {
			LOG.debug("Error en la carga del Jasper");
			e.printStackTrace();
		} catch (IOException e) {
			LOG.debug("Error entrada");
			e.printStackTrace();
		}
	}
}
