package cl.an.tde.api.integracion.rest.repo.combo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;

import cl.an.tde.api.integracion.dto.ComboDTO;
import cl.an.tde.api.integracion.pojo.Comun;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.Global;

@Service
public class ComunServiceImpl implements ComunServiceInterface {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Comun> listarEntity(String entity, String llave, String valor) {
        return this.listarEntity(entity, llave, valor, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Comun> listarEntity(String entity, String llave, String valor, Boolean activado) {
        final StringBuffer select = new StringBuffer(
                "SELECT new cl.an.tde.api.integracion.pojo.Comun(u." + llave + ", u." + valor);
        if (activado) {
            select.append(", u.estado");
        }
        select.append(") from " + entity + " u ");
        final Query query = em.createQuery(select.toString());
        return (List<Comun>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Comun> getNiveles(final Long nivelId) {
        final StringBuffer select = new StringBuffer(
                "SELECT new cl.an.tde.api.integracion.pojo.Comun(p.id, p.nombre) from Taxonomia t ");
        select.append(" left join t.parent p ");
        select.append(" left join t.children c ");
        select.append(" where p.id = :nivelId ");
        final Query query = em.createQuery(select.toString());
        query.setParameter("nivelId", nivelId);
        return (List<Comun>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Comun> funcionesActivas(final Integer institucionId, Boolean estado) {
        final StringBuffer select = new StringBuffer(
                "SELECT new cl.an.tde.api.integracion.pojo.Comun(f.id, f.nombre) from Funcion f ");
        select.append(" left join f.institucion i ");
        select.append(" where i.id = :institucionId ");
        select.append(" and f.estado = true ");

        List<Long> funciones = new ArrayList<Long>();
        if (estado ) {
            funciones = idFuncionesUsadas(institucionId);
            if (!funciones.isEmpty() ) {
                select.append(" and f.id not in (:funciones) ");
            }
        }
        final Query query = em.createQuery(select.toString());

        query.setParameter("institucionId", institucionId);
        if (estado ) {
            if (!funciones.isEmpty() ) {
                query.setParameter("funciones", funciones);
            }
        }
        return (List<Comun>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> idFuncionesUsadas(final Integer institucionId) {
        final StringBuffer select = new StringBuffer("SELECT f.id from CuadroClasificacion c ");
        select.append(" left join c.institucion i ");
        select.append(" join c.funcion f ");
        select.append(" where i.id = :institucionId ");

        final Query query = em.createQuery(select.toString());
        query.setParameter("institucionId", institucionId);
        return (List<Long>) query.getResultList();
    }

	@Override
	public List<Comun> listaTipoAcciones() {
		List<Comun> lista = new ArrayList<Comun>();
		for ( TipoAccion ta : TipoAccion.values() ) { 
			lista.add(new Comun(ta.getValor(), ta.getNombre()));
		}
		return lista;
	}

	@Override
	public List<ComboDTO> usuariosByInstitucion(Integer idInstitucion) {
		final StringBuffer jpql = new StringBuffer("SELECT");
		jpql.append(" new " + Global.PACKAGE_DTO + ".ComboDTO(u)");
		jpql.append(" FROM Usuario u");
		jpql.append(" LEFT JOIN u.institucion i");
		jpql.append(" WHERE i.id = :idInstitucion");
		
		final Query query = em.createQuery(jpql.toString());
		query.setParameter("idInstitucion", idInstitucion);
		List<ComboDTO> dto = (List<ComboDTO>) query.getResultList();
		return dto;
	}

	@Override
	public List<ComboDTO> unidadInstalacion() {
		final StringBuffer jpql = new StringBuffer("SELECT");
		jpql.append(" new " + Global.PACKAGE_DTO + ".ComboDTO(e)");
		jpql.append(" FROM UnidadInstalacion e");
		jpql.append(" WHERE e.estado IS TRUE");
		
		final Query query = em.createQuery(jpql.toString());
		List<ComboDTO> dto = (List<ComboDTO>) query.getResultList();
		return dto;
	}

}
