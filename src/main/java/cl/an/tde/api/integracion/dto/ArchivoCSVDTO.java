package cl.an.tde.api.integracion.dto;

public class ArchivoCSVDTO {
	
	private Long id;
	private Object metadata;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Object getMetadata() {
		return metadata;
	}
	public void setMetadata(Object metadata) {
		this.metadata = metadata;
	}
	
	
}
