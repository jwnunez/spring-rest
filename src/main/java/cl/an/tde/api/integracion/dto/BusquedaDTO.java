package cl.an.tde.api.integracion.dto;

import java.io.Serializable;

public class BusquedaDTO implements Serializable {

	private static final long serialVersionUID = -7951743252205056582L;

	private Integer idInstitucion;
	private Integer idUsuario;
	private String fechaDesde; 
	private String fechaHasta;
	private Short tipoAccion;

	public Integer getIdInstitucion() {
		return idInstitucion;
	}
	public void setIdInstitucion(Integer idInstitucion) {
		this.idInstitucion = idInstitucion;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getFechaDesde() {
		return fechaDesde;
	}
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	public String getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public Short getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(Short tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	@Override
	public String toString() {
		return "BusquedaDTO [idInstitucion=" + idInstitucion + ", idUsuario=" + idUsuario + ", fechaDesde=" + fechaDesde
				+ ", fechaHasta=" + fechaHasta + ", tipoAccion=" + tipoAccion + "]";
	}

}
