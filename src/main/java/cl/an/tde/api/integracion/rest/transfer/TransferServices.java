package cl.an.tde.api.integracion.rest.transfer;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cl.an.tde.Servicio;
import cl.an.tde.api.integracion.dto.AdtDTO;
import cl.an.tde.api.integracion.dto.FormatoArchivoDTO;
import cl.an.tde.api.integracion.dto.MetadataDTO;
import cl.an.tde.api.integracion.dto.NivelDTO;
import cl.an.tde.api.integracion.dto.SerieDTO;
import cl.an.tde.entities.EstadoAcuerdoTransferencia;
import cl.an.tde.utils.Global;

@Service
public class TransferServices extends Servicio {
	private static final Logger LOG = LoggerFactory.getLogger(TransferServices.class);

	public List<SerieDTO> obtenerSeriesByInstitucion(Integer institucionId) {
		LOG.info("-> obtenerSeriesByInstitucion : " + institucionId);
		final StringBuilder select = new StringBuilder(
				"SELECT new cl.an.tde.api.integracion.dto.SerieDTO(cc.id, cc.serie, cc.nombreNivel, t.id, "
				+ "t.archivoGestionAnios, t.archivoCentralInstitucionalAnios, t.archivoNacionalAnios)");
		select.append(" FROM CuadroClasificacion cc");
		select.append(" LEFT JOIN cc.institucion i");
		select.append(" LEFT JOIN cc.tablaRetencion t");
		select.append(" WHERE cc.hoja IS true");
		select.append(" AND i.id = :institucionId");
		
		final Query query = em.createQuery(select.toString());
		query.setParameter("institucionId", institucionId);
		final List<SerieDTO> lista = (List<SerieDTO>) query.getResultList();
		
		return lista;
	}
	
	public List<NivelDTO> obtenerNivelesByInstitucion(Integer institucionId) {
		LOG.info("-> obtenerNivelesByInstitucion : " + institucionId);
		final StringBuilder select = new StringBuilder(
				"SELECT new cl.an.tde.api.integracion.dto.NivelDTO(cc)");
		select.append(" FROM CuadroClasificacion cc");
		select.append(" LEFT JOIN cc.institucion i");
		select.append(" WHERE i.id = :institucionId");
		
		final Query query = em.createQuery(select.toString());
		query.setParameter("institucionId", institucionId);
		final List<NivelDTO> lista = (List<NivelDTO>) query.getResultList();
		
		return lista;
	}
	
	public List<AdtDTO> obtenerADTsBySerie(Integer serieId) {
		final StringBuffer jpql = new StringBuffer("SELECT");
		jpql.append(" new " + Global.PACKAGE_DTO + ".AdtDTO(at)");
		jpql.append(" FROM AcuerdoTransferencia at");
		jpql.append(" LEFT JOIN at.cuadroClasificacion cc");
		jpql.append(" LEFT JOIN at.estado e");
		jpql.append(" WHERE cc.id = ?1");
		jpql.append(" AND (e.id = ?2 OR e.id = ?3)");
		
		final Query query = em.createQuery(jpql.toString());
		query.setParameter(1, serieId.longValue());
		query.setParameter(2, new EstadoAcuerdoTransferencia(3).getId());
		query.setParameter(3, new EstadoAcuerdoTransferencia(5).getId());
		final List<AdtDTO> lista = (List<AdtDTO>) query.getResultList();
		
		return lista;
	}
	
	public List<FormatoArchivoDTO> obetenerFormatosActivos() {
		LOG.info("-> findActives");
		final StringBuilder select = new StringBuilder(
				"SELECT new cl.an.tde.api.integracion.dto.FormatoArchivoDTO(fa)");
		select.append(" FROM FormatoArchivo fa");
		select.append(" WHERE fa.estado IS true");
		
		final Query query = em.createQuery(select.toString());
		final List<FormatoArchivoDTO> lista = (List<FormatoArchivoDTO>) query.getResultList();
		
		return lista;
	}
	
	public List<MetadataDTO> obtenerMetadata() {
		LOG.info("-> obtenerMetadata");
		
		final StringBuilder select = new StringBuilder(
				"SELECT new cl.an.tde.api.integracion.dto.MetadataDTO(m.id, m.nombre, m.definicion, m.tipoDato.nombre)");
		select.append(" FROM MetaDato m");
		select.append(" WHERE m.estado IS true");
		
		final Query query = em.createQuery(select.toString());
		final List<MetadataDTO> lista = (List<MetadataDTO>) query.getResultList();
		
		return lista;
	}

}