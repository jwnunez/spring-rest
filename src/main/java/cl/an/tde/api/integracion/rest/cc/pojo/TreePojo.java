package cl.an.tde.api.integracion.rest.cc.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author rfuentes
 */
public class TreePojo implements Serializable {

    private static final long serialVersionUID = -6818661029071303767L;

    private Long id;
    private Long pid;
    private String name;
    private String nivelDescripcion;
    private Boolean hoja;
    private List<TreePojo> children;
    private Long nivelId;
    private Long nivelIdParent;

    /**
     * Constructor
     */
    public TreePojo() {
        super();
    }

    /**
     * Constructor con parameteros.
     * 
     * @param id {@link Long}
     * @param pid {@link Long}
     * @param name {@link String}
     * @param nivelDescripcion {@link String}
     * @param hoja {@link Boolean}
     * @param nivelId {@link Long}
     */
    public TreePojo(Long id, Long pid, String name, String nivelDescripcion, Boolean hoja, Long nivelId) {
        super();
        this.id = id;
        this.pid = pid;
        this.name = name;
        this.nivelDescripcion = nivelDescripcion;
        this.hoja = hoja;
        this.nivelId = nivelId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TreePojo> getChildren() {
        return children;
    }

    public void setChildren(List<TreePojo> children) {
        this.children = children;
    }

    public String getNivelDescripcion() {
        return nivelDescripcion;
    }

    public void setNivelDescripcion(String nivelDescripcion) {
        this.nivelDescripcion = nivelDescripcion;
    }

    public Boolean getHoja() {
        return hoja;
    }

    public void setHoja(Boolean hoja) {
        this.hoja = hoja;
    }

    public Long getNivelId() {
        return nivelId;
    }

    public void setNivelId(Long nivelId) {
        this.nivelId = nivelId;
    }

    public Long getNivelIdParent() {
        return nivelIdParent;
    }

    public void setNivelIdParent(Long nivelIdParent) {
        this.nivelIdParent = nivelIdParent;
    }

}
