package cl.an.tde.api.integracion.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.Api;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Api
public class Documento {
	private String nivelAcceso;
	private String titulo;
	private String nombreArchivo;
	private String fechaCapturaDocumento;
	private String fechaDocumento;
	private String etiquetas;
	private String identificador;
	private String checksum;
	private Long volumen;
	private String formato;
	private String nombreInteresado;
	private String apellidoMaternoInteresado;
	private String apellidoPaternoInteresado;
	private String rutInteresado;
	private String autor;
	private String destinatario;
	private String region;
	private String comuna;
	private String documentoDigitalizado;
	private List<MetadataExtra> listaMetadata;
	public String getNivelAcceso() {
		return nivelAcceso;
	}
	public void setNivelAcceso(String nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getFechaCapturaDocumento() {
		return fechaCapturaDocumento;
	}
	public void setFechaCapturaDocumento(String fechaCapturaDocumento) {
		this.fechaCapturaDocumento = fechaCapturaDocumento;
	}
	public String getFechaDocumento() {
		return fechaDocumento;
	}
	public void setFechaDocumento(String fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	public String getEtiquetas() {
		return etiquetas;
	}
	public void setEtiquetas(String etiquetas) {
		this.etiquetas = etiquetas;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public Long getVolumen() {
		return volumen;
	}
	public void setVolumen(Long volumen) {
		this.volumen = volumen;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	public String getNombreInteresado() {
		return nombreInteresado;
	}
	public void setNombreInteresado(String nombreInteresado) {
		this.nombreInteresado = nombreInteresado;
	}
	public String getApellidoMaternoInteresado() {
		return apellidoMaternoInteresado;
	}
	public void setApellidoMaternoInteresado(String apellidoMaternoInteresado) {
		this.apellidoMaternoInteresado = apellidoMaternoInteresado;
	}
	public String getApellidoPaternoInteresado() {
		return apellidoPaternoInteresado;
	}
	public void setApellidoPaternoInteresado(String apellidoPaternoInteresado) {
		this.apellidoPaternoInteresado = apellidoPaternoInteresado;
	}
	public String getRutInteresado() {
		return rutInteresado;
	}
	public void setRutInteresado(String rutInteresado) {
		this.rutInteresado = rutInteresado;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public String getDocumentoDigitalizado() {
		return documentoDigitalizado;
	}
	public void setDocumentoDigitalizado(String documentoDigitalizado) {
		this.documentoDigitalizado = documentoDigitalizado;
	}
	public List<MetadataExtra> getListaMetadata() {
		return listaMetadata;
	}
	public void setListaMetadata(List<MetadataExtra> listaMetadata) {
		this.listaMetadata = listaMetadata;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String cheksum) {
		this.checksum = cheksum;
	}
	
	

}
