package cl.an.tde.api.integracion.rest.repo.cc.man;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import cl.an.tde.api.integracion.pojo.Comun;
import cl.an.tde.api.integracion.rest.cc.pojo.CuadroClasificacionPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.TablaRetencionPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.TreePojo;
import cl.an.tde.entities.CuadroClasificacion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;


/**
 * @author rfuentes
 */
public interface CuadroClasificacionInterface {

    void save(CuadroClasificacion cuadroClasificacion);

    List<TreePojo> obtenerTreeByInstitucion(Integer institucionId);

    CuadroClasificacion findById(Long id);

    List<TablaRetencionPojo> tablaRetencionByInstitucion(Integer institucionId);

    List<TablaRetencionPojo> tablaRetencionByInstitucionAndSubfondo(Integer institucionId, Long subfondoId);

    List<Comun> subFondos(Integer institucion);

    void listaCuadroClasificacion(CuadroClasificacion cc, List<CuadroClasificacionPojo> trees);

    JasperPrint exportPdfFile() throws JRException, IOException;
}
