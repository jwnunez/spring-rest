package cl.an.tde.api.integracion.rest.cc.pojo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;

import cl.an.tde.entities.ClasificacionFormatoArchivo;
import cl.an.tde.entities.CuadroClasificacion;
import cl.an.tde.entities.FormatoArchivo;
import cl.an.tde.entities.Funcion;
import cl.an.tde.entities.Institucion;
import cl.an.tde.entities.NivelDescripcion;
import cl.an.tde.entities.TablaRetencion;
import cl.an.tde.entities.Unidad;
import cl.an.tde.entities.UnidadInstalacion;
import cl.an.tde.utils.ObjectUtil;

/**
 * @author rfuentes
 */
public class CuadroClasificacionPojo implements Serializable {

    private static final long serialVersionUID = 766481667918202861L;
    private Long id;
    private Long nivelDescripcionId;
    private String nivelDescripcionNombre;
    private String codigoReferencia;
    private Integer institucionId;
    private String nombreInstitucion;
    private String nombreNivel;
    private Integer fechaDocMasAntigua;
    private Long volumenElectronico;
    private Integer unidadId;
    private List<ClasificacionFormatoArchivoPojo> formatoArchivos;
    private Long volumenPapel;
    private Integer unidadInstalacionId;
    private String nombreProductor;
    private String formaIngresoAN;
    private String alcanceContenido;
    private Long clasificacionId;
    private String clasificacionCodigo;
    private Long tablaRetencionId;
    private Integer disposicionId;
    private String fundamento;
    private String soporte;
    private Integer version;
    private Date fechaCreacionRetencion;
    private Boolean event;
    private Date fechaUltimaModificacion;
    private Integer archivoGestionAnios;
    private Integer archivoCentralInstitucionalAnios;
    private Integer archivoNacionalAnios;
    private Boolean conservacionPermanente;
    private Boolean eliminacion;
    private Boolean transferencia;
    private Long funcionId;
    private String serie;
    private String tipo;
	private String descripcionRestriccion;
    private Boolean restriccionAcceso;
    private String tipoRestriccion;
    private LocalDate fechaCaducidad;
    private String fuenteLegal;
    private Boolean datosPersonalSensibles;
	private String descripcionDatosSensibles;
	private String composicion;

    /**
     * Constructor.
     */
    public CuadroClasificacionPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param cc {@link CuadroClasificacion}
     */
    public CuadroClasificacionPojo(CuadroClasificacion cc) {
        super();
        this.id = cc.getId();
        this.serie = cc.getSerie();
        this.tipo = cc.getTipo();
        if (!ObjectUtil.isNull(cc.getNivelDescripcion()) ) {
            this.nivelDescripcionId = cc.getNivelDescripcion().getId();
            this.nivelDescripcionNombre = cc.getNivelDescripcion().getNombre();
        }
        this.codigoReferencia = cc.getCodigoReferencia();
        if (!ObjectUtil.isNull(cc.getInstitucion()) ) {
            this.institucionId = cc.getInstitucion().getId();
            this.nombreInstitucion = cc.getInstitucion().getNombre();
        }
        this.nombreNivel = cc.getNombreNivel();
        this.fechaDocMasAntigua = cc.getFechaDocMasAntigua();
        this.volumenElectronico = cc.getVolumenElectronico();
        if (!ObjectUtil.isNull(cc.getUnidad()) ) {
            this.unidadId = cc.getUnidad().getId();
        }

        this.volumenPapel = cc.getVolumenPapel();
        if (!ObjectUtil.isNull(cc.getUnidadInstalacion()) ) {
            this.unidadInstalacionId = cc.getUnidadInstalacion().getId();
        }
        this.nombreProductor = cc.getNombreProductor();
        this.formaIngresoAN = cc.getFormaIngresoAN();
        this.alcanceContenido = cc.getAlcanceContenido();
        if (!ObjectUtil.isNull(cc.getClasificacion()) ) {
            this.clasificacionId = cc.getClasificacion().getId();
            this.clasificacionCodigo = cc.getClasificacion().getCodigoReferencia();
        }
        this.fechaUltimaModificacion = cc.getFechaUltimaModificacion();
        if (!ObjectUtil.isNull(cc.getFuncion())) {
            this.funcionId = cc.getFuncion().getId();
        }
        if (!ObjectUtil.isNull(cc.getTablaRetencion()) ) {
            TablaRetencion tr = cc.getTablaRetencion();
            this.tablaRetencionId = tr.getId();
            this.archivoGestionAnios = tr.getArchivoGestionAnios();
            this.archivoCentralInstitucionalAnios = tr.getArchivoCentralInstitucionalAnios();
            this.archivoNacionalAnios = tr.getArchivoNacionalAnios();
            this.conservacionPermanente = tr.getConservacionPermanente();
            this.transferencia = tr.getTransferencia();
            this.fundamento = tr.getFundamento();
            this.soporte = tr.getSoporte();
            this.fechaCreacionRetencion = tr.getFechaCreacion();
        }
        this.restriccionAcceso = cc.getRestriccionAcceso();
        this.descripcionRestriccion = cc.getDescripcionRestriccion();
        this.datosPersonalSensibles = cc.getDatosPersonalSensibles();
        this.descripcionDatosSensibles = cc.getDescripcionDatosSensibles();
        this.fechaCaducidad = cc.getFechaCaducidad();
        this.fuenteLegal = cc.getFuenteLegal();
        this.tipoRestriccion = cc.getTipoRestriccion();
        this.composicion = cc.getComposicion();
        
    }

    /**
     * Constructor con parametros.
     * 
     * @param cc {@link TablaRetencion}
     */
    public CuadroClasificacionPojo(TablaRetencion tr) {
        super();
    }

    /**
     * Metodo que crea Cuadro Clasificaión a partir de este pojo.
     * 
     * @return {@link CuadroClasificacion}
     */
    public CuadroClasificacion obtenerCuadroClasificacion() {
        final CuadroClasificacion cc = new CuadroClasificacion();
        cc.setId(this.id);
        cc.setTipo(this.tipo);
        cc.setNivelDescripcion(new NivelDescripcion(this.nivelDescripcionId));
        cc.setCodigoReferencia(this.codigoReferencia);
        cc.setInstitucion(new Institucion(this.institucionId));
        cc.setNombreNivel(this.nombreNivel);
        cc.setFechaDocMasAntigua(this.fechaDocMasAntigua);
        cc.setVolumenElectronico(this.volumenElectronico);
        cc.setUnidad(new Unidad(this.unidadId));
        cc.setVolumenPapel(this.volumenPapel);
        cc.setRestriccionAcceso(this.restriccionAcceso);
        cc.setDescripcionRestriccion(this.descripcionRestriccion);
        cc.setFuenteLegal(this.fuenteLegal);
        cc.setDatosPersonalSensibles(this.datosPersonalSensibles);
        cc.setDescripcionDatosSensibles(this.descripcionDatosSensibles);
        cc.setFechaCaducidad(this.fechaCaducidad);
        cc.setTipoRestriccion(this.tipoRestriccion);
        cc.setComposicion(this.composicion);
        if (!ObjectUtil.isNull(this.unidadInstalacionId) ) {
            cc.setUnidadInstalacion(new UnidadInstalacion(this.unidadInstalacionId));
        }
        cc.setNombreProductor(this.nombreProductor);
        cc.setFormaIngresoAN(this.formaIngresoAN);
        cc.setAlcanceContenido(this.alcanceContenido);
        if (!ObjectUtil.isNull(this.funcionId) ) {
            cc.setFuncion(new Funcion(this.funcionId));
        }

        List<ClasificacionFormatoArchivo> archivos = new ArrayList<>();

        for (ClasificacionFormatoArchivoPojo fa : formatoArchivos) {
            final ClasificacionFormatoArchivo cfa = new ClasificacionFormatoArchivo();
            cfa.setId(fa.getIdCfa());
            cfa.setEstado(fa.getEstado());
            cfa.setFormatoArchivo(new FormatoArchivo(fa.getFormatoArchivoId()));
            cfa.setCuadroClasificacion(cc);
            archivos.add(cfa);
        }

        cc.setFechaUltimaModificacion(new Date());
        if (!ObjectUtil.isNull(this.clasificacionId) ) {
            cc.setClasificacionParent(new CuadroClasificacion(this.clasificacionId));
        }
        return cc;
    }

    public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNivelDescripcionId() {
        return nivelDescripcionId;
    }

    public void setNivelDescripcionId(Long nivelDescripcionId) {
        this.nivelDescripcionId = nivelDescripcionId;
    }

    public String getNivelDescripcionNombre() { return nivelDescripcionNombre; }

    public void setNivelDescripcionNombre(String nivelDescripcionNombre) { this.nivelDescripcionNombre = nivelDescripcionNombre; }

    public String getCodigoReferencia() {
        return codigoReferencia;
    }

    public void setCodigoReferencia(String codigoReferencia) {
        this.codigoReferencia = codigoReferencia;
    }

    public Integer getInstitucionId() {
        return institucionId;
    }

    public void setInstitucionId(Integer institucionId) {
        this.institucionId = institucionId;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public Integer getFechaDocMasAntigua() {
        return fechaDocMasAntigua;
    }

    public void setFechaDocMasAntigua(Integer fechaDocMasAntigua) {
        this.fechaDocMasAntigua = fechaDocMasAntigua;
    }

    public Long getVolumenElectronico() {
        return volumenElectronico;
    }

    public void setVolumenElectronico(Long volumenElectronico) {
        this.volumenElectronico = volumenElectronico;
    }

    public Integer getUnidadId() {
        return unidadId;
    }

    public void setUnidadId(Integer unidadId) {
        this.unidadId = unidadId;
    }

    public List<ClasificacionFormatoArchivoPojo> getFormatoArchivos() {
        return formatoArchivos;
    }

    public void setFormatoArchivos(List<ClasificacionFormatoArchivoPojo> formatoArchivos) { this.formatoArchivos = formatoArchivos; }

    public Long getVolumenPapel() {
        return volumenPapel;
    }

    public void setVolumenPapel(Long volumenPapel) {
        this.volumenPapel = volumenPapel;
    }

    public Integer getUnidadInstalacionId() {
        return unidadInstalacionId;
    }

    public void setUnidadInstalacionId(Integer unidadInstalacionId) {
        this.unidadInstalacionId = unidadInstalacionId;
    }

    public String getNombreProductor() {
        return nombreProductor;
    }

    public void setNombreProductor(String nombreProductor) {
        this.nombreProductor = nombreProductor;
    }

    public String getFormaIngresoAN() {
        return formaIngresoAN;
    }

    public void setFormaIngresoAN(String formaIngresoAN) {
        this.formaIngresoAN = formaIngresoAN;
    }

    public String getNombreNivel() {
        return nombreNivel;
    }

    public void setNombreNivel(String nombreNivel) {
        this.nombreNivel = nombreNivel;
    }

    public String getAlcanceContenido() {
        return alcanceContenido;
    }

    public void setAlcanceContenido(String alcanceContenido) {
        this.alcanceContenido = alcanceContenido;
    }

    public Long getClasificacionId() {
        return clasificacionId;
    }

    public void setClasificacionId(Long clasificacionId) {
        this.clasificacionId = clasificacionId;
    }

    public String getClasificacionCodigo() { return clasificacionCodigo; }

    public void setClasificacionCodigo(String clasificacionCodigo) { this.clasificacionCodigo = clasificacionCodigo; }

    public Long getTablaRetencionId() {
        return tablaRetencionId;
    }

    public void setTablaRetencionId(Long tablaRetencionId) {
        this.tablaRetencionId = tablaRetencionId;
    }

    public Integer getDisposicionId() {
        return disposicionId;
    }

    public void setDisposicionId(Integer disposicionId) {
        this.disposicionId = disposicionId;
    }

    public String getFundamento() {
        return fundamento;
    }

    public void setFundamento(String fundamento) {
        this.fundamento = fundamento;
    }

    public String getSoporte() {
        return soporte;
    }

    public void setSoporte(String soporte) {
        this.soporte = soporte;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getFechaCreacionRetencion() {
        return fechaCreacionRetencion;
    }

    public void setFechaCreacionRetencion(Date fechaCreacionRetencion) { this.fechaCreacionRetencion = fechaCreacionRetencion; }

    public Boolean getEvent() { return event; }

    public void setEvent(Boolean event) {
        this.event = event;
    }

    public Date getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) { this.fechaUltimaModificacion = fechaUltimaModificacion; }

    public Integer getArchivoGestionAnios() {
        return archivoGestionAnios;
    }

    public void setArchivoGestionAnios(Integer archivoGestionAnios) {
        this.archivoGestionAnios = archivoGestionAnios;
    }

    public Integer getArchivoCentralInstitucionalAnios() {
        return archivoCentralInstitucionalAnios;
    }

    public void setArchivoCentralInstitucionalAnios(Integer archivoCentralInstitucionalAnios) { this.archivoCentralInstitucionalAnios = archivoCentralInstitucionalAnios; }

    public Integer getArchivoNacionalAnios() {
        return archivoNacionalAnios;
    }

    public void setArchivoNacionalAnios(Integer archivoNacionalAnios) { this.archivoNacionalAnios = archivoNacionalAnios; }

    public Boolean getConservacionPermanente() {
        return conservacionPermanente;
    }

    public void setConservacionPermanente(Boolean conservacionPermanente) { this.conservacionPermanente = conservacionPermanente; }

    public Boolean getEliminacion() {
        return eliminacion;
    }

    public void setEliminacion(Boolean eliminacion) {
        this.eliminacion = eliminacion;
    }

    public Boolean getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(Boolean transferencia) {
        this.transferencia = transferencia;
    }

    public Long getFuncionId() {
        return funcionId;
    }

    public void setFuncionId(Long funcionId) {
        this.funcionId = funcionId;
    }

    public String getSerie() { return serie; }

    public void setSerie(String serie) { this.serie = serie; }

	public String getDescripcionRestriccion() {
		return descripcionRestriccion;
	}

	public void setDescripcionRestriccion(String descripcionRestriccion) {
		this.descripcionRestriccion = descripcionRestriccion;
	}

	public Boolean getRestriccionAcceso() {
		return restriccionAcceso;
	}

	public void setRestriccionAcceso(Boolean restriccionAcceso) {
		this.restriccionAcceso = restriccionAcceso;
	}

	public String getTipoRestriccion() {
		return tipoRestriccion;
	}

	public void setTipoRestriccion(String tipoRestriccion) {
		this.tipoRestriccion = tipoRestriccion;
	}

	public LocalDate getFechaCaducidad() {
		return fechaCaducidad;
	}

	public void setFechaCaducidad(LocalDate fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public String getFuenteLegal() {
		return fuenteLegal;
	}

	public void setFuenteLegal(String fuenteLegal) {
		this.fuenteLegal = fuenteLegal;
	}

	public Boolean getDatosPersonalSensibles() {
		return datosPersonalSensibles;
	}

	public void setDatosPersonalSensibles(Boolean datosPersonalSensibles) {
		this.datosPersonalSensibles = datosPersonalSensibles;
	}

	public String getDescripcionDatosSensibles() {
		return descripcionDatosSensibles;
	}

	public void setDescripcionDatosSensibles(String descripcionDatosSensibles) {
		this.descripcionDatosSensibles = descripcionDatosSensibles;
	}

	public String getComposicion() {
		return composicion;
	}

	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}
    
}
