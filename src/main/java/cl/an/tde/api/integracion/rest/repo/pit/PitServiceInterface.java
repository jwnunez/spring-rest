package cl.an.tde.api.integracion.rest.repo.pit;

import cl.an.tde.entities.PIT;

public interface PitServiceInterface {
    PIT obtenerPitByAdT(Long idAdT);
}
