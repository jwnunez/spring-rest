package cl.an.tde.api.integracion.rest.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ServicesException extends ResponseEntityExceptionHandler {

    private static final long serialVersionUID = -8032025896308792270L;

    public ServicesException() {
        super();
    }

    @ExceptionHandler(BusinessException.class)
    public final ResponseEntity<ErrorMsg> handleAllExceptions(BusinessException ex, WebRequest request) {
        final ErrorMsg errorDetails = new ErrorMsg(new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
