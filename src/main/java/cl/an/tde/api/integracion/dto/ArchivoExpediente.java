package cl.an.tde.api.integracion.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.Api;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Api
public class ArchivoExpediente {
	private String titulo;
	private String fechaDocumento;
	private String autor;
	private String destinatario;
	private String identificador;
	private String checksum;
	private Long volumen;
	private String formato;
	private String nombreArchivo;
	private String documentoDigitalizado;
	private List<MetadataExtra> listaMetadata;
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getFechaDocumento() {
		return fechaDocumento;
	}
	public void setFechaDocumento(String fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public Long getVolumen() {
		return volumen;
	}
	public void setVolumen(Long volumen) {
		this.volumen = volumen;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getDocumentoDigitalizado() {
		return documentoDigitalizado;
	}
	public void setDocumentoDigitalizado(String documentoDigitalizado) {
		this.documentoDigitalizado = documentoDigitalizado;
	}
	public List<MetadataExtra> getListaMetadata() {
		return listaMetadata;
	}
	public void setListaMetadata(List<MetadataExtra> listaMetadata) {
		this.listaMetadata = listaMetadata;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	

}
