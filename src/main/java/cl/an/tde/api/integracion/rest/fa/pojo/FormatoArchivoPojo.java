package cl.an.tde.api.integracion.rest.fa.pojo;

import java.io.Serializable;
import java.util.Date;

import cl.an.tde.entities.FormatoArchivo;

/**
 * @author rfuentes
 */
public class FormatoArchivoPojo implements Serializable {

    private static final long serialVersionUID = -3788639976725393526L;
    private Integer id;
    private String descripcion;
    private Boolean estado;
    private Date fechaCreacion;
    private String mimeType;
    private String extension;

    /**
     * constructor.
     */
    public FormatoArchivoPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param fa {@link FormatoArchivo}
     */
    public FormatoArchivoPojo(FormatoArchivo fa) {
        super();
        this.id = fa.getId();
        this.descripcion = fa.getDescripcion();
        this.estado = fa.getEstado();
        this.fechaCreacion = fa.getFechaCreacion();
        this.mimeType = fa.getMimeType();
        this.extension = fa.getExtension();
    }

    public FormatoArchivo getFormatoArchivo() {
        final FormatoArchivo fa = new FormatoArchivo();
        fa.setDescripcion(this.descripcion);
        fa.setEstado(this.estado);
        fa.setFechaCreacion(this.fechaCreacion);
        fa.setId(this.id);
        fa.setMimeType(this.mimeType);
        fa.setExtension(this.extension);
        return fa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

}
