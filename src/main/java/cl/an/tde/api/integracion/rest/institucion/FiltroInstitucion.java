package cl.an.tde.api.integracion.rest.institucion;

public class FiltroInstitucion {
	
	public String nombre;
	public String sigla;
	public String codigo;
	
	public FiltroInstitucion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FiltroInstitucion(String nombre, String sigla, String codigo) {
		super();
		this.nombre = nombre;
		this.sigla = sigla;
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	

}
