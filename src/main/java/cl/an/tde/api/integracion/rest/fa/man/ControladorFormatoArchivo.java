package cl.an.tde.api.integracion.rest.fa.man;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.fa.pojo.FormatoArchivoPojo;
import cl.an.tde.api.integracion.rest.repo.fa.man.FormatoArchivoServiceInterface;
import cl.an.tde.entities.FormatoArchivo;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;

@RestController
@RequestMapping(value = "/formatoArchivo", name = "/formatoArchivo")
@CrossOrigin(origins = "*")
public class ControladorFormatoArchivo implements Serializable {

	private static final long serialVersionUID = -5412784051023702426L;
	
	@Autowired
	private FormatoArchivoServiceInterface formatoArchivoServiceInterface;

	@Autowired
	ServicioBitacora bitacora;

	@GetMapping("/listar")
	public Respuesta<List<FormatoArchivo>> listar() {
		Respuesta<List<FormatoArchivo>> respuesta = new Respuesta<List<FormatoArchivo>>(
				(List<FormatoArchivo>) formatoArchivoServiceInterface.findAll(), 0);
		return respuesta;
	}

	@GetMapping(value = "/get/{id}")
	public Respuesta<FormatoArchivo> obtenerMetaDatoById(@PathVariable Integer id) {
		return new Respuesta<FormatoArchivo>((FormatoArchivo) formatoArchivoServiceInterface.findById(id), 0);
	}

	@PutMapping(value = "/update")
	public Respuesta<FormatoArchivo> actualizar(@RequestHeader(value="User") Integer user, @RequestBody FormatoArchivo formatoArchivo) {
		formatoArchivoServiceInterface.save(formatoArchivo);
		bitacora.crear(TipoAccion.UPDATE, user, "Formato Archivo:" + formatoArchivo.getExtension());
		return new Respuesta<FormatoArchivo>(formatoArchivo, 0);
	}

	@PostMapping(value = "/save")
	public Respuesta<FormatoArchivo> crear(@RequestHeader(value="User") Integer user, @RequestBody FormatoArchivoPojo fa) {
		final FormatoArchivo formatoArchivo = fa.getFormatoArchivo();
		final Boolean editar = !ObjectUtil.isNull(formatoArchivo.getId()) ? Boolean.TRUE : Boolean.FALSE;
		formatoArchivo.setFechaCreacion(new Date());
		formatoArchivoServiceInterface.save(formatoArchivo);
		bitacora.crear(editar ? TipoAccion.UPDATE: TipoAccion.CREATE, user, "Formato Archivo:" + formatoArchivo.getExtension());
		return new Respuesta<FormatoArchivo>(formatoArchivo, 0);
	}
	
	@PutMapping(value = "/delete/{id}")
    public Respuesta<FormatoArchivo> actualizarEstado(@RequestHeader(value="User") Integer user, @PathVariable Integer id) {
	    FormatoArchivo fa = formatoArchivoServiceInterface.findById(id);
        fa.setEstado(!fa.getEstado());
        formatoArchivoServiceInterface.save(fa);
        bitacora.crear(TipoAccion.DISABLE, user, "Formato Archivo:" + fa.getExtension());
        return new Respuesta<FormatoArchivo>(fa, 0);
    }
}
