package cl.an.tde.api.integracion.pojo;

import java.io.Serializable;

public class OAuthParam implements Serializable {

	private static final long serialVersionUID = -7895518854507265057L;
	
	private String endpointUrl;
	private String authorization;
	private String grantType;
	private String username;
	private String password;

	public OAuthParam() {
		super();
	}

	public String getEndpointUrl() {
		return endpointUrl;
	}

	public void setEndpointUrl(String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}

	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	public String getGrantType() {
		return grantType;
	}

	public void setGrantType(String grantType) {
		this.grantType = grantType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "OAuthParam [endpointUrl=" + endpointUrl + ", authorization=" + authorization + ", grantType=" + grantType
				+ ", username=" + username + ", password=****]";
	}

}
