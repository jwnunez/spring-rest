package cl.an.tde.api.integracion.rest.uaa;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import cl.an.tde.api.integracion.pojo.OAuthParam;
import cl.an.tde.utils.CustomException;
import cl.an.tde.utils.JsonUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class ServicioUAA {
	private static final Logger LOG = LoggerFactory.getLogger(ServicioUAA.class);

	@Value("${security.oauth2.client.access-token-uri}")
	private String accessTokenUri;

	protected String getOAuthToken(OAuthParam auth) throws IOException, CustomException {

		auth.setEndpointUrl(accessTokenUri);
		LOG.info("-> OAuth: " + auth.toString());

		URL url = new URL(auth.getEndpointUrl() + "?grant_type=" + auth.getGrantType() + "&username="
				+ auth.getUsername() + "&password=" + auth.getPassword());

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", auth.getAuthorization());
//			conn.setRequestProperty("content-type", "text/xml; charset=UTF-8");
//			conn.setConnectTimeout(10000);
//			conn.connect();

		LOG.info("-> Response: Code={} Message={}", conn.getResponseCode(), conn.getResponseMessage());

		switch (conn.getResponseCode()) {
		case 200:
			break;
		case 401:
			throw new CustomException(1, "Usuario o contraseña inválidos");
		default:
			throw new CustomException(500, "Error Code: " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		String output = null;
		StringBuilder sb = new StringBuilder();

		while ((output = br.readLine()) != null) {
			sb.append(output);
		}
		LOG.info("Output: " + sb.toString());
		conn.disconnect();

		return sb.toString();
	}


}