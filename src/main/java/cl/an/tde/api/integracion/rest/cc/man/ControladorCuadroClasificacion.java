package cl.an.tde.api.integracion.rest.cc.man;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cl.an.tde.api.integracion.pojo.Comun;
import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.cc.pojo.ClasificacionFormatoArchivoPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.ClasificacionPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.CuadroClasificacionPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.TablaRetencionPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.TreePojo;
import cl.an.tde.api.integracion.rest.institucion.InstitucionInterface;
import cl.an.tde.api.integracion.rest.repo.cc.man.ClasificacionInterface;
import cl.an.tde.api.integracion.rest.repo.cc.man.CuadroClasificacionInterface;
import cl.an.tde.api.integracion.rest.repo.combo.ComunServiceInterface;
import cl.an.tde.api.integracion.rest.repo.fa.man.FormatoArchivoServiceInterface;
import cl.an.tde.entities.Clasificacion;
import cl.an.tde.entities.ClasificacionFormatoArchivo;
import cl.an.tde.entities.CuadroClasificacion;
import cl.an.tde.entities.FormatoArchivo;
import cl.an.tde.entities.Funcion;
import cl.an.tde.entities.Institucion;
import cl.an.tde.entities.NivelDescripcion;
import cl.an.tde.entities.Unidad;
import cl.an.tde.entities.UnidadInstalacion;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;

@RestController
@RequestMapping(value = "/cuadroClasificacion", name = "/cuadroClasificacion")
@CrossOrigin(origins = "*")
public class ControladorCuadroClasificacion {

    @Autowired
    private CuadroClasificacionInterface cuadroClasificacionInterface;

    @Autowired
    private ClasificacionInterface clasificacionInterface;

    @Autowired
    private InstitucionInterface institucionInterface;

    @Autowired
    private FormatoArchivoServiceInterface formatoArchivoInterface;

    @Autowired
    private ComunServiceInterface comunServiceInterface;

	@Autowired
	ServicioBitacora bitacora;

    @PostMapping(value = "/save")
    public Respuesta<Boolean> crear(@RequestHeader(value="User") Integer user, @RequestBody CuadroClasificacionPojo cc) {
        if (!cc.getEvent() ) {
            this.crearCuadroClasificacion(cc);
            bitacora.crear(TipoAccion.CREATE, user, "CuadroClasificacion:" + cc.getNombreNivel());
        } else {
            this.actualizarCuadroClasificacion(cc);
            bitacora.crear(TipoAccion.UPDATE, user, "CuadroClasificacion:" + cc.getNombreNivel());
        }
        return new Respuesta<Boolean>(Boolean.TRUE, 0);
    }

    private void crearCuadroClasificacion(CuadroClasificacionPojo cc) {
        StringBuilder serie = new StringBuilder();
        final CuadroClasificacion cuadroClasificacion = cc.obtenerCuadroClasificacion();
        if (!ObjectUtil.isNull(cc.getClasificacionId()) ) {
            CuadroClasificacion parent = cuadroClasificacionInterface.findById(cc.getClasificacionId());
            parent.setHoja(Boolean.FALSE);
            cuadroClasificacionInterface.save(parent);
            serie.append(parent.getSerie());
        }
        cuadroClasificacion.setHoja(Boolean.TRUE);
        serie.append("/" + cuadroClasificacion.getNombreNivel());
        cuadroClasificacion.setSerie(serie.toString());
        final Clasificacion clas = clasificacionInterface.buscaUltimaClasificacionPorInstitucionId(cc.getInstitucionId());
        cuadroClasificacion.setClasificacion(clas);
        if (!ObjectUtil.isNull(cc.getFormatoArchivos()) ) {
            List<ClasificacionFormatoArchivo> archivos = new ArrayList<>();

            for (ClasificacionFormatoArchivoPojo fa : cc.getFormatoArchivos()) {
                ClasificacionFormatoArchivo cfa = new ClasificacionFormatoArchivo();
                cfa.setId(fa.getIdCfa());
                cfa.setCuadroClasificacion(cuadroClasificacion);
                final FormatoArchivo formato = formatoArchivoInterface.findById(fa.getFormatoArchivoId());
                cfa.setEstado(fa.getEstado());
                cfa.setFormatoArchivo(formato);
                archivos.add(cfa);
            }
            cuadroClasificacion.setFormatosArchivo(archivos);
        }
        cuadroClasificacionInterface.save(cuadroClasificacion);
    }

    private void actualizarCuadroClasificacion(CuadroClasificacionPojo cc) {
        final CuadroClasificacion cuadro = cuadroClasificacionInterface.findById(cc.getId());
        if (!ObjectUtil.isNull(cc.getNivelDescripcionId()) ) {
            cuadro.setNivelDescripcion(new NivelDescripcion(cc.getNivelDescripcionId()));
        }
        
        cuadro.setTipo(cc.getTipo());
        cuadro.setCodigoReferencia(cc.getCodigoReferencia());
        cuadro.setFechaDocMasAntigua(cc.getFechaDocMasAntigua());
        cuadro.setComposicion(cc.getComposicion());;
        cuadro.setVolumenElectronico(cc.getVolumenElectronico());
        if (!ObjectUtil.isNull(cc.getUnidadId()) ) {
            cuadro.setUnidad(new Unidad(cc.getUnidadId()));
        }
        cuadro.setVolumenPapel(cc.getVolumenPapel());
        if (!ObjectUtil.isNull(cc.getUnidadInstalacionId()) ) {
            cuadro.setUnidadInstalacion(new UnidadInstalacion(cc.getUnidadInstalacionId()));
        }
        cuadro.setAlcanceContenido(cc.getAlcanceContenido());
        cuadro.setFechaUltimaModificacion(new Date());
        if (!ObjectUtil.isNull(cc.getFuncionId()) ) {
            cuadro.setFuncion(new Funcion(cc.getFuncionId()));
        } else {
            cuadro.setFuncion(null);
        }
        if (!ObjectUtil.isNull(cc.getFormatoArchivos()) ) {
            List<ClasificacionFormatoArchivo> archivos = new ArrayList<>();

            for (ClasificacionFormatoArchivoPojo fa : cc.getFormatoArchivos()) {
                ClasificacionFormatoArchivo cfa = new ClasificacionFormatoArchivo();
                cfa.setId(fa.getIdCfa());
                cfa.setCuadroClasificacion(cuadro);
                final FormatoArchivo formato = formatoArchivoInterface.findById(fa.getFormatoArchivoId());
                cfa.setEstado(fa.getEstado());
                cfa.setFormatoArchivo(formato);
                archivos.add(cfa);
            }
            cuadro.setFormatosArchivo(archivos);
        }
        cuadroClasificacionInterface.save(cuadro);
    }

    @GetMapping(value = "/get/{id}")
    public Respuesta<CuadroClasificacionPojo> obtenerClientePorId(@PathVariable Long id) {
        CuadroClasificacion cc = cuadroClasificacionInterface.findById(id);
        final CuadroClasificacionPojo ccp = new CuadroClasificacionPojo(cc);
        final Clasificacion clas = clasificacionInterface
                .buscaUltimaClasificacionPorInstitucionId(cc.getInstitucion().getId());
        if ( clas != null ) {
            ccp.setVersion(clas.getVersion());
        }
        final List<ClasificacionFormatoArchivoPojo> formatos = new ArrayList<>();

        final List<Comun> comun = (List<Comun>) comunServiceInterface.listarEntity("FormatoArchivo", "id", "extension");
        for (Comun formato : comun) {
            ClasificacionFormatoArchivoPojo cfap = new ClasificacionFormatoArchivoPojo();
            cfap.setFormatoArchivoId(formato.getId().intValue());
            cfap.setExtension(formato.getNombre());
            for (ClasificacionFormatoArchivo cfa : cc.getFormatosArchivo()) {
                if (cfap.getFormatoArchivoId().equals(cfa.getFormatoArchivo().getId()) ) {
                    cfap.setEstado(cfa.getEstado());
                    cfap.setIdCfa(cfa.getId());
                }
            }
            formatos.add(cfap);
        }
        ccp.setFormatoArchivos(formatos);
        return new Respuesta<CuadroClasificacionPojo>(ccp, 0);
    }

    @GetMapping(value = "/obtenerTreeByInstitucion/{institucionId}")
    public Respuesta<ClasificacionPojo> obtenerTreeByInstitucion(@PathVariable Integer institucionId) {
        final List<TreePojo> tree = cuadroClasificacionInterface.obtenerTreeByInstitucion(institucionId);
        Clasificacion clas = clasificacionInterface.buscaUltimaClasificacionPorInstitucionId(institucionId);
        if (ObjectUtil.isNull(clas) ) {
            final Institucion i = institucionInterface.findById(institucionId);
            clas = new Clasificacion();
            clas.setFechaCreacion(new Date());
            clas.setInstitucion(i);
            clas.setVersion(1);
            clas.setCodigoReferencia(i.getCodigo() + ".CC." + clas.getVersion());

            clasificacionInterface.save(clas);
        }
        ClasificacionPojo cp = new ClasificacionPojo(clas.getId(), institucionId, tree, clas.getVersion(),
                clas.getCodigoReferencia());
        return new Respuesta<ClasificacionPojo>(cp, 0);
    }

    @GetMapping(value = "/tablaRetencionByInstitucion/{institucionId}")
    public Respuesta<ClasificacionPojo> tablaRetencionByInstitucion(@PathVariable Integer institucionId) {
        final List<TablaRetencionPojo> tr = cuadroClasificacionInterface.tablaRetencionByInstitucion(institucionId);
        return new Respuesta<ClasificacionPojo>(armarClasificacion(institucionId, tr), 0);
    }

    @GetMapping(value = "/tablaRetencionByInstitucion/{institucionId}/{subfondoId}")
    public Respuesta<ClasificacionPojo> tablaRetencionByInstitucionAndSubfondo(@PathVariable Integer institucionId,
                                                                               @PathVariable Long subfondoId) {
        final List<TablaRetencionPojo> tr = cuadroClasificacionInterface.tablaRetencionByInstitucionAndSubfondo(institucionId, subfondoId);
        return new Respuesta<ClasificacionPojo>(armarClasificacion(institucionId, tr), 0);
    }

    private ClasificacionPojo armarClasificacion(Integer institucionId, List<TablaRetencionPojo> tr) {
        Clasificacion clas = clasificacionInterface.buscaUltimaClasificacionPorInstitucionId(institucionId);
        ClasificacionPojo cp = new ClasificacionPojo(institucionId, tr);
        if (!ObjectUtil.isNull(clas) ) {
            cp.setClasificacionId(clas.getId());
            cp.setCodigo(clas.getCodigoReferencia());
            cp.setVersion(clas.getVersion());
        }
        return cp;
    }

    @GetMapping(value = "/formatosArchivos")
    public Respuesta<List<ClasificacionFormatoArchivoPojo>> formatosArchivos() {
        final List<ClasificacionFormatoArchivoPojo> formatos = new ArrayList<>();
        final List<Comun> comun = (List<Comun>) comunServiceInterface.listarEntity("FormatoArchivo", "id", "extension");
        for (Comun formato : comun) {
            ClasificacionFormatoArchivoPojo cfap = new ClasificacionFormatoArchivoPojo();
            cfap.setFormatoArchivoId(formato.getId().intValue());
            cfap.setExtension(formato.getNombre());

            formatos.add(cfap);
        }

        return new Respuesta<List<ClasificacionFormatoArchivoPojo>>(formatos, 0);
    }

    @GetMapping(value = "/niveles/{nivelId}")
    public Respuesta<List<Comun>> getNiveles(@PathVariable Long nivelId) {
        final List<Comun> niveles = comunServiceInterface.getNiveles(nivelId);
        return new Respuesta<List<Comun>>(niveles, 0);
    }

    @GetMapping(value = "/funcionesActivas/{institucionId}/{estado}")
    public Respuesta<List<Comun>> funcionesActivas(@PathVariable Integer institucionId, @PathVariable Boolean estado) {
        final List<Comun> niveles = comunServiceInterface.funcionesActivas(institucionId, estado);
        return new Respuesta<List<Comun>>(niveles, 0);
    }

}
