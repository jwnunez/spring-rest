package cl.an.tde.api.integracion.rest.repo.fa.man;

import java.util.List;

import cl.an.tde.entities.FormatoArchivo;

public interface FormatoArchivoServiceInterface {

	List<FormatoArchivo> findAll();

	FormatoArchivo findById(Integer id);

	void save(FormatoArchivo formatoArchivo);

}
