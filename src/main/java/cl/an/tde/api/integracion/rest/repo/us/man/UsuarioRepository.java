package cl.an.tde.api.integracion.rest.repo.us.man;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import cl.an.tde.entities.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	List<Usuario> findByNombres(String nombres);

	Optional<Usuario> findById(Long id);

	@Query("SELECT u FROM Usuario u WHERE u.rut=:rut and u.aPaterno like :aPaterno " + "and u.aMaterno like :aMaterno "
			+ "and u.nombreUsuario like :nombreUsuario " + "and u.institucion.id = :institucionId ")
	List<Usuario> fetchUsuarios(@Param("rut") String rut, @Param("aPaterno") String aPaterno,
			@Param("aMaterno") String aMaterno, @Param("nombreUsuario") String nombreUsuario,
			@Param("institucionId") Integer institucionId);

	Optional<Usuario> findByNombreUsuario(String nombreUsuario);

	@Query("SELECT u FROM Usuario u WHERE u.id=:idUsuario and u.institucion.id = :idInstitucion")
	Usuario findByIdAndInstitucion(Long idUsuario, Integer idInstitucion);
}
