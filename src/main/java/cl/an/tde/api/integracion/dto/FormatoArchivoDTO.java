package cl.an.tde.api.integracion.dto;

import java.io.Serializable;

import cl.an.tde.entities.FormatoArchivo;

public class FormatoArchivoDTO implements Serializable {

	private static final long serialVersionUID = -6145380885693203390L;

	private Integer id;
	private String extension;
	private String descripcion;
	private Boolean estado;

	public FormatoArchivoDTO(FormatoArchivo fa) {
		super();
		this.id = fa.getId();
		this.extension = fa.getExtension();
		this.descripcion = fa.getDescripcion();
		this.estado = fa.getEstado();
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Boolean getEstado() {
		return estado;
	}
	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

}
