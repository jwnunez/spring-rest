package cl.an.tde.api.integracion.rest.repo.at.man;

import cl.an.tde.api.integracion.rest.at.pojo.AcuerdoTransferenciaPojo;
import cl.an.tde.entities.AcuerdoTransferencia;
import cl.an.tde.enums.TipoAccion;

import java.util.List;

/**
 * @author rfuentes
 */
public interface AcuerdoTransferenciaInterface {

	AcuerdoTransferencia findById(Long acuerdoId);

	void save(AcuerdoTransferencia acuerdo);

	List<AcuerdoTransferenciaPojo> listar(AcuerdoTransferenciaPojo atp);

	void workflowAdT(TipoAccion ta, AcuerdoTransferencia adt);
	
}
