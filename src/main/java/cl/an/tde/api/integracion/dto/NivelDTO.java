package cl.an.tde.api.integracion.dto;

import java.io.Serializable;

import cl.an.tde.entities.CuadroClasificacion;

public class NivelDTO implements Serializable {

	private static final long serialVersionUID = -5735886770402071285L;

	private Integer id;
	private Integer parentId;
	private Integer nivelDescripId;
	private Integer funcionId;
	private String nombreNivel;
	private String codigoReferencia;
	private String serie;
	private Boolean hoja;

	public NivelDTO(CuadroClasificacion cc) {
		super();
		this.id = cc.getId().intValue();
		this.parentId = (cc.getClasificacionParent() != null) ? cc.getClasificacionParent().getId().intValue() : -1;
		this.nivelDescripId = cc.getNivelDescripcion().getId().intValue();
		this.funcionId = (cc.getFuncion() != null) ? cc.getFuncion().getId().intValue() : -1;
		this.nombreNivel = cc.getNombreNivel();
		this.codigoReferencia = cc.getCodigoReferencia();
		this.serie = cc.getSerie();
		this.hoja = cc.getHoja();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getNivelDescripId() {
		return nivelDescripId;
	}

	public void setNivelDescripId(Integer nivelDescripId) {
		this.nivelDescripId = nivelDescripId;
	}

	public Integer getFuncionId() {
		return funcionId;
	}

	public void setFuncionId(Integer funcionId) {
		this.funcionId = funcionId;
	}

	public String getNombreNivel() {
		return nombreNivel;
	}

	public void setNombreNivel(String nombreNivel) {
		this.nombreNivel = nombreNivel;
	}

	public String getCodigoReferencia() {
		return codigoReferencia;
	}

	public void setCodigoReferencia(String codigoReferencia) {
		this.codigoReferencia = codigoReferencia;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Boolean getHoja() {
		return hoja;
	}

	public void setHoja(Boolean hoja) {
		this.hoja = hoja;
	}

}
