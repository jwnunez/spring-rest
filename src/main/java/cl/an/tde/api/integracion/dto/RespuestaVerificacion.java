package cl.an.tde.api.integracion.dto;

import java.util.List;

public class RespuestaVerificacion {

	private String mensaje;
	private List<ErrorTrans> errores;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<ErrorTrans> getErrores() {
		return errores;
	}
	public void setErrores(List<ErrorTrans> errores) {
		this.errores = errores;
	}
	
	
}
