package cl.an.tde.api.integracion.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.Api;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Api
public class Contrato {
	
	private Integer idInstitucion;
	private String nombreInstitucion;
	private String codigoSerie;
	private String nombreSerie;
	private String codigoAcuerdoTransferencia;
	private String nombreAcuerdoTransferencia;
	
	private List<Expediente> listaExpedientes;
	private List<Documento> listaDocumentos;

	public Integer getIdInstitucion() {
		return idInstitucion;
	}

	public void setIdInstitucion(Integer idInstitucion) {
		this.idInstitucion = idInstitucion;
	}

	public String getNombreInstitucion() {
		return nombreInstitucion;
	}

	public void setNombreInstitucion(String nombreInstitucion) {
		this.nombreInstitucion = nombreInstitucion;
	}

	public String getCodigoSerie() {
		return codigoSerie;
	}

	public void setCodigoSerie(String codigoSerie) {
		this.codigoSerie = codigoSerie;
	}

	public String getNombreSerie() {
		return nombreSerie;
	}

	public void setNombreSerie(String nombreSerie) {
		this.nombreSerie = nombreSerie;
	}

	public String getCodigoAcuerdoTransferencia() {
		return codigoAcuerdoTransferencia;
	}

	public void setCodigoAcuerdoTransferencia(String codigoAcuerdoTransferencia) {
		this.codigoAcuerdoTransferencia = codigoAcuerdoTransferencia;
	}

	public String getNombreAcuerdoTransferencia() {
		return nombreAcuerdoTransferencia;
	}

	public void setNombreAcuerdoTransferencia(String nombreAcuerdoTransferencia) {
		this.nombreAcuerdoTransferencia = nombreAcuerdoTransferencia;
	}

	public List<Expediente> getListaExpedientes() {
		return listaExpedientes;
	}

	public void setListaExpedientes(List<Expediente> listaExpedientes) {
		this.listaExpedientes = listaExpedientes;
	}

	public List<Documento> getListaDocumentos() {
		return listaDocumentos;
	}

	public void setListaDocumentos(List<Documento> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}

	@Override
	public String toString() {
		return "Contrato [idInstitucion=" + idInstitucion + ", nombreInstitucion=" + nombreInstitucion
				+ ", codigoSerie=" + codigoSerie + ", nombreSerie=" + nombreSerie + ", codigoAcuerdoTransferencia="
				+ codigoAcuerdoTransferencia + ", nombreAcuerdoTransferencia=" + nombreAcuerdoTransferencia
				+ ", listaExpedientes=" + listaExpedientes + ", listaDocumentos=" + listaDocumentos + "]";
	}

	
}
