package cl.an.tde.api.integracion.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.Api;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Api
public class Expediente {
	
	private String nivelAcceso;
	private String tituloExpediente;
	private Long volumenExpediente;
	private String fechaCreacionExpediente;
	private String fechaFinalizacionExpediente;
	private String nombreInteresado;
	private String apellidoPaternoInteresado;
	private String apellidoMaternoInteresado;
	private String rutInteresado;
	private String etiquetas;
	private String region;
	private String comuna;
	
	private List<ArchivoExpediente> listaArchivos;

	public String getNivelAcceso() {
		return nivelAcceso;
	}

	public void setNivelAcceso(String nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}

	public String getTituloExpediente() {
		return tituloExpediente;
	}

	public void setTituloExpediente(String tituloExpediente) {
		this.tituloExpediente = tituloExpediente;
	}

	public Long getVolumenExpediente() {
		return volumenExpediente;
	}

	public void setVolumenExpediente(Long volumenExpediente) {
		this.volumenExpediente = volumenExpediente;
	}

	public String getFechaCreacionExpediente() {
		return fechaCreacionExpediente;
	}

	public void setFechaCreacionExpediente(String fechaCreacionExpediente) {
		this.fechaCreacionExpediente = fechaCreacionExpediente;
	}

	public String getFechaFinalizacionExpediente() {
		return fechaFinalizacionExpediente;
	}

	public void setFechaFinalizacionExpediente(String fechaFinalizacionExpediente) {
		this.fechaFinalizacionExpediente = fechaFinalizacionExpediente;
	}

	

	
	public String getEtiquetas() {
		return etiquetas;
	}

	public void setEtiquetas(String etiquetas) {
		this.etiquetas = etiquetas;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getComuna() {
		return comuna;
	}

	public void setComuna(String comuna) {
		this.comuna = comuna;
	}

	public List<ArchivoExpediente> getListaArchivos() {
		return listaArchivos;
	}

	public void setListaArchivos(List<ArchivoExpediente> listaArchivos) {
		this.listaArchivos = listaArchivos;
	}

	public String getNombreInteresado() {
		return nombreInteresado;
	}

	public void setNombreInteresado(String nombreInteresado) {
		this.nombreInteresado = nombreInteresado;
	}

	public String getApellidoPaternoInteresado() {
		return apellidoPaternoInteresado;
	}

	public void setApellidoPaternoInteresado(String apellidoPaternoInteresado) {
		this.apellidoPaternoInteresado = apellidoPaternoInteresado;
	}

	public String getApellidoMaternoInteresado() {
		return apellidoMaternoInteresado;
	}

	public void setApellidoMaternoInteresado(String apellidoMaternoInteresado) {
		this.apellidoMaternoInteresado = apellidoMaternoInteresado;
	}

	public String getRutInteresado() {
		return rutInteresado;
	}

	public void setRutInteresado(String rutInteresado) {
		this.rutInteresado = rutInteresado;
	}

}
