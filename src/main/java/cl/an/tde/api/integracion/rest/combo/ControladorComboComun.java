package cl.an.tde.api.integracion.rest.combo;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.an.tde.Controlador;
import cl.an.tde.api.integracion.dto.ComboDTO;
import cl.an.tde.api.integracion.pojo.Comun;
import cl.an.tde.api.integracion.rest.exception.BusinessException;
import cl.an.tde.api.integracion.rest.repo.combo.ComunServiceInterface;
import cl.an.tde.utils.ComunType;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;

/**
 * @author rfuentes
 *
 */
@RestController
@RequestMapping(value = "/combo", name = "/combo")
public class ControladorComboComun extends Controlador {

	@Autowired
	private ComunServiceInterface comunServiceInterface;

	@PostMapping("/listar")
	public Respuesta<List<Comun>> listar(@RequestBody ComunType comun) {
		List<Comun> find = new ArrayList<>();
		if (!ObjectUtil.isNull(comun.getActivado())) {
			find = (List<Comun>) comunServiceInterface.listarEntity(comun.getEntity(), comun.getLlave(),
					comun.getValor(), comun.getActivado());
		} else {
			find = (List<Comun>) comunServiceInterface.listarEntity(comun.getEntity(), comun.getLlave(),
					comun.getValor());
		}

		final Respuesta<List<Comun>> respuesta = new Respuesta<List<Comun>>(find, 0);
		return respuesta;
	}


	@PostMapping("/listarEnum")
	public Respuesta<List<Comun>> listarEnum(@RequestBody ComunType comun) {
		try {
			Class<?> clazz = Class.forName("cl.an.tde.entities."+ comun.getEntity());
			Method method = clazz.getMethod("values", null);
			Object[] arrs = {};
			Object[] values = (Object[]) method.invoke(null, arrs);
			List<Comun> lista = new ArrayList<>();
			Long i = 0L;
			for (Object s : values) {
				lista.add(new Comun(i, s.toString()));
				i++;
			}
			final Respuesta<List<Comun>> respuesta = new Respuesta<List<Comun>>(lista, 0);
			return respuesta;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException(comun.getEntity() + ", no es un Enum");
		}
	}
	
	@PostMapping("/tipoAcciones")
	public Respuesta<List<Comun>> listare(@RequestBody ComunType comun) {
		final List<Comun> find = comunServiceInterface.listaTipoAcciones();
		final Respuesta<List<Comun>> respuesta = new Respuesta<List<Comun>>(find, 0);
		return respuesta;
	}

	@GetMapping("/usuarios/{institucionId}")
	public Respuesta<List<ComboDTO>> obtenerUsuarios(@PathVariable Integer institucionId) {
		final List<ComboDTO> lista = (List<ComboDTO>) comunServiceInterface.usuariosByInstitucion(institucionId);
		Respuesta<List<ComboDTO>> resp = new Respuesta<List<ComboDTO>>(lista, 0);
		return resp;
	}

	@GetMapping("/unidadInstalacion")
	public Respuesta<List<ComboDTO>> unidadInstalacion() {
		final List<ComboDTO> lista = (List<ComboDTO>) comunServiceInterface.unidadInstalacion();
		Respuesta<List<ComboDTO>> resp = new Respuesta<List<ComboDTO>>(lista, 0);
		return resp;
	}

}
