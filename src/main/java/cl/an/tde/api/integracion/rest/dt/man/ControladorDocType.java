package cl.an.tde.api.integracion.rest.dt.man;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.dt.pojo.TipoDocumentoPojo;
import cl.an.tde.api.integracion.rest.repo.dt.man.TipoDocumentoServiceInterface;
import cl.an.tde.entities.TipoDocumento;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;

/**
 * @author rfuentes
 */
@RestController
@RequestMapping(value = "/doctypes", name = "/doctypes")
@CrossOrigin(origins = "*")
public class ControladorDocType implements Serializable {

	private static final long serialVersionUID = -8092921168005541229L;
	private static final Logger LOG = LoggerFactory.getLogger(ControladorDocType.class);

	@Autowired
	private TipoDocumentoServiceInterface tipoDocumentoServiceInterface;

	@Autowired
	ServicioBitacora bitacora;
	
    @GetMapping("/listar")
    public Respuesta<List<TipoDocumento>> listar() {
        LOG.info("listar usuarios");
        final Respuesta<List<TipoDocumento>> respuesta = new Respuesta<List<TipoDocumento>>(
                (List<TipoDocumento>) tipoDocumentoServiceInterface.findAll(), 0);
        return respuesta;
    }

    @GetMapping(value = "/get/{id}")
    public Respuesta<TipoDocumento> obtenerUsuarioById(@PathVariable Long id) {
        return new Respuesta<TipoDocumento>((TipoDocumento) tipoDocumentoServiceInterface.findById(id), 0);
    }

	@PutMapping(value = "/update")
	public Respuesta<TipoDocumento> actualizar(@RequestHeader(value="User") Integer user, @RequestBody TipoDocumento tipoDocumento) {
		tipoDocumentoServiceInterface.save(tipoDocumento);
		bitacora.crear(TipoAccion.UPDATE, user, "Tipo Documento:" + tipoDocumento.getNombre());
		return new Respuesta<TipoDocumento>(tipoDocumento, 0);
	}

    @PostMapping(value = "/save")
    public Respuesta<Boolean> crear(@RequestHeader(value="User") Integer user, @RequestBody TipoDocumentoPojo tipoDocumento) {
        final TipoDocumento td = tipoDocumento.getTipoDocumento();
        final Boolean editar = !ObjectUtil.isNull(td.getId()) ? Boolean.TRUE : Boolean.FALSE;
        tipoDocumentoServiceInterface.save(td);
        bitacora.crear(editar ? TipoAccion.UPDATE: TipoAccion.CREATE, user, "Tipo Documento:" + tipoDocumento.getNombre());
        return new Respuesta<Boolean>(Boolean.TRUE, 0);
    }

    @PutMapping(value = "/delete/{id}")
    public Respuesta<TipoDocumento> actualizarEstado(@RequestHeader(value="User") Integer user, @PathVariable Long id) {
        final TipoDocumento td = tipoDocumentoServiceInterface.findById(id);
        td.setEstado(!td.getEstado());
        tipoDocumentoServiceInterface.save(td);
        bitacora.crear(TipoAccion.DISABLE, user, "Tipo Documento:" + td.getNombre());
        return new Respuesta<TipoDocumento>(td, 0);
    }

}
