package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class RespuestaInicioTrans implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2366321122839007805L;
	
	private UUID uuidTrans;
	private List<ErrorTrans> errores;
	
	
	public UUID getUuidTrans() {
		return uuidTrans;
	}
	public void setUuidTrans(UUID uuidTrans) {
		this.uuidTrans = uuidTrans;
	}
	public List<ErrorTrans> getErrores() {
		return errores;
	}
	public void setErrores(List<ErrorTrans> errores) {
		this.errores = errores;
	}	
}
