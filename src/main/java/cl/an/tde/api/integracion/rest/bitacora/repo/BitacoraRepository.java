package cl.an.tde.api.integracion.rest.bitacora.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import cl.an.tde.api.integracion.dto.BitacoraDTO;
import cl.an.tde.entities.Bitacora;

public interface BitacoraRepository extends CrudRepository<Bitacora, Long> {
	
	List<Bitacora> findByIdInstitucion(Integer idInstitucion);
	
	@Query("SELECT new cl.an.tde.api.integracion.dto.BitacoraDTO(b) FROM Bitacora b"
			+ " WHERE b.idInstitucion = :idInstitucion"
			+ " AND b.idUsuario = :idUsuario"
			+ " AND CAST(b.fecha AS date) >= CAST(:fechaDesde AS date)"
			+ " AND CAST(b.fecha AS date) <= CAST(:fechaHasta AS date)")
	List<BitacoraDTO> fetchBitacora(
			@Param("idInstitucion") Integer idInstitucion,
			@Param("idUsuario") Integer idUsuario,
			@Param("fechaDesde") String fechaDesde,
			@Param("fechaHasta") String fechaHasta);

}