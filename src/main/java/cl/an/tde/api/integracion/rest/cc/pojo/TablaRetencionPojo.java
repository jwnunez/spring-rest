package cl.an.tde.api.integracion.rest.cc.pojo;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;

import cl.an.tde.entities.Institucion;
import cl.an.tde.entities.TablaRetencion;
import cl.an.tde.utils.ObjectUtil;

/**
 * @author rfuentes
 */
public class TablaRetencionPojo implements Serializable {

	private static final long serialVersionUID = -6617913850019337349L;

	private String serie;
	private Long clasificacionId;
	private String nivelDescripcion;

	private Long tablaRetencionId;
	private Integer disposicionId;
	private String fundamento;
	private Integer institucionId;
	private String soporte;
	private Date fechaCreacion;
	private String fechaCreacionString;
	private Integer archivoGestionAnios;
	private Integer archivoCentralInstitucionalAnios;
	private Integer archivoNacionalAnios;
	private Boolean conservacionPermanente;
	private Boolean transferencia;

	/**
	 * Constructor.
	 */
	public TablaRetencionPojo() {
		super();
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param serie                            {@link String}
	 * @param clasificacionId                  {@link String}
	 * @param nivelDescripcion                 {@link String}
	 * @param tablaRetencionId                 {@link Long}
	 * @param archivoGestionAnios              {@link Integer}
	 * @param archivoCentralInstitucionalAnios {@link Integer}
	 * @param archivoNacionalAnios             {@link Integer}
	 * @param conservacionPermanente           {@link Boolean}
	 * @param transferencia                    {@link Boolean}
	 */
	public TablaRetencionPojo(String serie, Long clasificacionId, String nivelDescripcion, Long tablaRetencionId,
			Integer archivoGestionAnios, Integer archivoCentralInstitucionalAnios, Integer archivoNacionalAnios,
			Boolean conservacionPermanente, Boolean transferencia, Date fechaCreacion, String fundamento,
			String soporte) {
		super();
		this.serie = serie;
		this.clasificacionId = clasificacionId;
		this.nivelDescripcion = nivelDescripcion;
		this.tablaRetencionId = tablaRetencionId;
		this.archivoGestionAnios = archivoGestionAnios;
		this.archivoCentralInstitucionalAnios = archivoCentralInstitucionalAnios;
		this.archivoNacionalAnios = archivoNacionalAnios;
		this.conservacionPermanente = conservacionPermanente;
		this.transferencia = transferencia;
		this.fechaCreacion = fechaCreacion;
		this.fundamento = fundamento;
		this.soporte = soporte;
		if (fechaCreacion != null) {
			this.setFechaCreacionString();			
		} else {
			this.fechaCreacionString = "";
		}
	}

	public TablaRetencion geTablaRetencion() {
		final TablaRetencion tr = new TablaRetencion(this.tablaRetencionId);
		tr.setFechaCreacion(!ObjectUtil.isNull(this.fechaCreacion) ? this.fechaCreacion : new Date());
		tr.setFundamento(this.getFundamento());
		tr.setInstitucion(new Institucion(this.institucionId));
		tr.setSoporte(this.soporte);
		tr.setArchivoGestionAnios(this.archivoGestionAnios);
		tr.setArchivoCentralInstitucionalAnios(this.archivoCentralInstitucionalAnios);
		tr.setArchivoNacionalAnios(this.archivoNacionalAnios);
		tr.setConservacionPermanente(this.conservacionPermanente);
		tr.setTransferencia(this.transferencia);
		return tr;
	}

	public String getFechaCreacionString() {
		return fechaCreacionString;
	}

	public void setFechaCreacionString() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		this.fechaCreacionString = dateFormat.format(this.getFechaCreacion());
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Long getClasificacionId() {
		return clasificacionId;
	}

	public void setClasificacionId(Long clasificacionId) {
		this.clasificacionId = clasificacionId;
	}

	public String getNivelDescripcion() {
		return nivelDescripcion;
	}

	public void setNivelDescripcion(String nivelDescripcion) {
		this.nivelDescripcion = nivelDescripcion;
	}

	public Long getTablaRetencionId() {
		return tablaRetencionId;
	}

	public void setTablaRetencionId(Long tablaRetencionId) {
		this.tablaRetencionId = tablaRetencionId;
	}

	public Integer getDisposicionId() {
		return disposicionId;
	}

	public void setDisposicionId(Integer disposicionId) {
		this.disposicionId = disposicionId;
	}

	public String getFundamento() {
		return fundamento;
	}

	public void setFundamento(String fundamento) {
		this.fundamento = fundamento;
	}

	public Integer getInstitucionId() {
		return institucionId;
	}

	public void setInstitucionId(Integer institucionId) {
		this.institucionId = institucionId;
	}

	public String getSoporte() {
		return soporte;
	}

	public void setSoporte(String soporte) {
		this.soporte = soporte;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Integer getArchivoGestionAnios() {
		return archivoGestionAnios;
	}

	public void setArchivoGestionAnios(Integer archivoGestionAnios) {
		this.archivoGestionAnios = archivoGestionAnios;
	}

	public Integer getArchivoCentralInstitucionalAnios() {
		return archivoCentralInstitucionalAnios;
	}

	public void setArchivoCentralInstitucionalAnios(Integer archivoCentralInstitucionalAnios) {
		this.archivoCentralInstitucionalAnios = archivoCentralInstitucionalAnios;
	}

	public Integer getArchivoNacionalAnios() {
		return archivoNacionalAnios;
	}

	public void setArchivoNacionalAnios(Integer archivoNacionalAnios) {
		this.archivoNacionalAnios = archivoNacionalAnios;
	}

	public Boolean getConservacionPermanente() {
		return conservacionPermanente;
	}

	public void setConservacionPermanente(Boolean conservacionPermanente) {
		this.conservacionPermanente = conservacionPermanente;
	}

	public Boolean getTransferencia() {
		return transferencia;
	}

	public void setTransferencia(Boolean transferencia) {
		this.transferencia = transferencia;
	}

}
