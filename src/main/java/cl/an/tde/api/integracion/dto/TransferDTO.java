package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.util.Date;

import cl.an.tde.entities.PIT;

public class TransferDTO implements Serializable {

	private static final long serialVersionUID = 5855062760675530443L;

	private Long id;
	private String institucion;
	private String usuario;
	private Integer adt;
	private String serie;
	private String nivel;
	private Integer cantidad;
	private String metadata;
	private String uuid;
	private Date fecha;
	private String ruta;
	private Boolean oficial;

	public TransferDTO() {
	}

	public TransferDTO(PIT p) {
		this.id = p.getId();
		this.institucion = p.getInstitucion().getNombre();
		this.usuario = p.getUsuario().getNombreUsuario();
		this.adt = (p.getAdT()!=null) ? p.getAdT().getId().intValue() : -1;
		this.serie = p.getSerie();
		this.nivel = p.getNivel();
		this.cantidad = p.getDocs();
		this.metadata = p.getMetadata();
		this.uuid = p.getUuid().toString();
		this.fecha = p.getFecha();
		this.ruta = p.getRuta();
		this.oficial = p.getOficial();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInstitucion() {
		return institucion;
	}
	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public Integer getAdt() {
		return adt;
	}
	public void setAdt(Integer adt) {
		this.adt = adt;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Boolean getOficial() {
		return oficial;
	}
	public void setOficial(Boolean oficial) {
		this.oficial = oficial;
	}

}
