package cl.an.tde.api.integracion.dto;

import java.util.UUID;

public class Consolidacion {
	
	private UUID uuidTransfer;
	private Long adtId;

	public UUID getUuidTransfer() {
		return uuidTransfer;
	}

	public void setUuidTransfer(UUID uuidTransfer) {
		this.uuidTransfer = uuidTransfer;
	}

	public Long getAdtId() {
		return adtId;
	}

	public void setAdtId(Long adtId) {
		this.adtId = adtId;
	}
	
}
