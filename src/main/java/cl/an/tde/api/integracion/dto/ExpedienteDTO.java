package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cl.an.tde.entities.Archivo;
import cl.an.tde.entities.Expediente;

public class ExpedienteDTO implements Serializable {

	private static final long serialVersionUID = 5587180419031133419L;

	private Long id;
	private String nombre;
	private String descripcion;
    private String nivelAcceso;
    private String metadata;
	private List<ArchivoDTO> archivos;

	//Default builder necessary to create these as part of a post subquery
	public ExpedienteDTO() {
	}

	public ExpedienteDTO(Expediente e) {
		this.id = e.getId();
		this.nombre = e.getNombre();
		this.descripcion = e.getDescripcion();
		this.nivelAcceso = e.getNivelAcceso();
		this.metadata = e.getMetadata();
		this.archivos = cast(e.getArchivos());
	}
	
	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    public String getNivelAcceso() {
        return nivelAcceso;
    }
    public void setNivelAcceso(String nivelAcceso) {
        this.nivelAcceso = nivelAcceso;
    }

    public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<ArchivoDTO> getArchivos() {
		return archivos;
	}
	public void setArchivos(List<ArchivoDTO> archivos) {
		this.archivos = archivos;
	}

	private List<ArchivoDTO> cast(List<Archivo> lst) {
		final List<ArchivoDTO> dto = new ArrayList<>();
		for ( final Archivo a : lst ) {
			ArchivoDTO s = new ArchivoDTO(a);
			dto.add(s);
		}
		return dto;
	}

}
