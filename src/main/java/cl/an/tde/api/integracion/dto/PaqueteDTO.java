package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.an.tde.entities.Archivo;
import cl.an.tde.entities.Expediente;
import cl.an.tde.entities.Paquete;

public class PaqueteDTO implements Serializable {

	private static final long serialVersionUID = -8014373899922338832L;

	private Long id;
	private String usuario;
	private String serie;
	private List<ArchivoDTO> archivos;
	private List<ExpedienteDTO> expedientes;
	private String descripcion;
	private Date fecha;
	private Boolean borrador;
	private Boolean udc;

	public PaqueteDTO(Paquete p) {
		this.id = p.getId();
		this.usuario = p.getUsuario().getNombreUsuario();
		this.serie = p.getSerie().toString();
		this.descripcion = p.getDescripcion();
		this.fecha = p.getFecha();
		this.borrador = p.getBorrador();
		this.udc = p.getUdc();
	}

	public PaqueteDTO(Paquete p, boolean full) {
		this.id = p.getId();
		this.usuario = p.getUsuario().getNombreUsuario();
		this.serie = p.getSerie().toString();
		this.descripcion = p.getDescripcion();
		if ( full ) {
			if ( p.getUdc() ) {
				p.getExpedientes().size();
				this.expedientes = new ArrayList<>();
				for ( final Expediente e : p.getExpedientes() ) {
					e.getArchivos().size();
					this.expedientes.add(new ExpedienteDTO(e));
				}
			} else {
				p.getArchivos().size();
				this.archivos = cast(p.getArchivos());
			}
		}
		this.fecha = p.getFecha();
		this.borrador = p.getBorrador();
		this.udc = p.getUdc();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public List<ArchivoDTO> getArchivos() {
		return archivos;
	}
	public void setArchivos(List<ArchivoDTO> archivos) {
		this.archivos = archivos;
	}

	public List<ExpedienteDTO> getExpedientes() {
		return expedientes;
	}
	public void setExpedientes(List<ExpedienteDTO> expedientes) {
		this.expedientes = expedientes;
	}

	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Boolean getBorrador() {
		return borrador;
	}
	public void setBorrador(Boolean borrador) {
		this.borrador = borrador;
	}

	public Boolean getUdc() {
		return udc;
	}
	public void setUdc(Boolean udc) {
		this.udc = udc;
	}

	private List<ArchivoDTO> cast(List<Archivo> lst) {
		final List<ArchivoDTO> dto = new ArrayList<>();
		for ( final Archivo a : lst ) {
			ArchivoDTO s = new ArchivoDTO(a);
			dto.add(s);
		}
		return dto;
	}

}
