package cl.an.tde.api.integracion.rest.repo.md.man;

import java.util.List;

import cl.an.tde.entities.MetaDato;

public interface MetaDatoServiceInterface {

	List<MetaDato> findAll();

	MetaDato findById(Integer id);

	void save(MetaDato metaDato);
	
	List<MetaDato> findByType(Integer tud);

}
