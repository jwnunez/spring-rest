package cl.an.tde.api.integracion.rest.repo.cc.man;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.an.tde.entities.Clasificacion;

/**
 * @author rfuentes
 */
@Service
public class ClasificacionImpl implements ClasificacionInterface {

    @Autowired
    private ClasificacionRepository clasificacionRepository;

    @PersistenceContext
    private EntityManager em;

    @Override
    public Clasificacion findById(Long id) {
        return clasificacionRepository.findById(id).get();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Clasificacion buscaUltimaClasificacionPorInstitucionId(Integer institucionId) {
        final StringBuilder select = new StringBuilder();
        select.append("SELECT c from Clasificacion c ");
        select.append(" LEFT JOIN c.institucion i ");
        select.append("WHERE i.id = :institucionId ");
        select.append("ORDER BY c.id DESC ");

        final Query query = em.createQuery(select.toString());
        query.setParameter("institucionId", institucionId);
        final List<Clasificacion> lista = (List<Clasificacion>) query.getResultList();
        if (!lista.isEmpty() ) {
            return lista.get(0);
        }
        return null;
    }

    @Override 
    public Clasificacion save(Clasificacion clasificacion) {
        return clasificacionRepository.save(clasificacion);
    }

}
