package cl.an.tde.api.integracion.rest.institucion.pojo;

import java.io.Serializable;

import cl.an.tde.entities.Funcion;
import cl.an.tde.entities.TipoFuncion;
import cl.an.tde.utils.ObjectUtil;

/**
 * @author rfuentes
 */
public class FuncionPojo implements Serializable {

    private static final long serialVersionUID = 4813373456612268222L;

    private Long id;
    private Long tipoFuncionId;
    private String tipoFuncion;
    private String nombre;
    private String descripcion;
    private String legislacion;
    private Integer anioCreacionFuncion;
    private Boolean estado;

    /**
     * Constructor.
     */
    public FuncionPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param id
     */
    public FuncionPojo(Long id) {
        super();
        this.id = id;
    }

    /**
     * Constructor con parametros.
     * 
     * @param funcion
     */
    public FuncionPojo(Funcion funcion) {
        super();
        this.id = funcion.getId();
        if (!ObjectUtil.isNull(funcion.getTipoFuncion()) ) {
            this.tipoFuncionId = funcion.getTipoFuncion().getId();
            this.tipoFuncion = funcion.getTipoFuncion().getNombre();
        }
        this.nombre = funcion.getNombre();
        this.descripcion = funcion.getDescripcion();
        this.legislacion = funcion.getLegislacion();
        this.anioCreacionFuncion = funcion.getAnioCreacion();
        this.estado = funcion.getEstado();
    }

    public Funcion obtenerFuncion() {
        final Funcion funcion = new Funcion(this.id);
        if (!ObjectUtil.isNull(this.tipoFuncionId) ) {
            funcion.setTipoFuncion(new TipoFuncion(this.tipoFuncionId));
        }
        funcion.setNombre(this.nombre);
        funcion.setDescripcion(this.descripcion);
        funcion.setLegislacion(this.legislacion);
        funcion.setAnioCreacion(this.anioCreacionFuncion);
        funcion.setEstado(this.estado);

        return funcion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTipoFuncionId() {
        return tipoFuncionId;
    }

    public void setTipoFuncionId(Long tipoFuncionId) {
        this.tipoFuncionId = tipoFuncionId;
    }

    public String getTipoFuncion() {
        return tipoFuncion;
    }

    public void setTipoFuncion(String tipoFuncion) {
        this.tipoFuncion = tipoFuncion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLegislacion() {
        return legislacion;
    }

    public void setLegislacion(String legislacion) {
        this.legislacion = legislacion;
    }

    public Integer getAnioCreacionFuncion() {
        return anioCreacionFuncion;
    }

    public void setAnioCreacionFuncion(Integer anioCreacionFuncion) {
        this.anioCreacionFuncion = anioCreacionFuncion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
