package cl.an.tde.api.integracion.rest.md.pojo;

import java.io.Serializable;

import cl.an.tde.entities.MetaDato;
import cl.an.tde.entities.TipoDato;

/**
 * @author rfuentes
 */
public class MetaDatoPojo implements Serializable {

    private static final long serialVersionUID = -7339884093989758213L;
    private Integer id;
    private String nombre;
    private String definicion;
    private Integer tipoDatoId;
    private Boolean automatico;
    private Boolean estado;

    /**
     * Constructor.
     */
    public MetaDatoPojo() {
        super();
    }

    public MetaDato getMetaDato() {
        MetaDato md = new MetaDato();
        md.setId(this.getId());
        md.setNombre(this.nombre);
        md.setAutomatico(Boolean.FALSE);
        md.setDefinicion(this.definicion);
        md.setTipoDato(new TipoDato(this.tipoDatoId));
        md.setEstado(estado);
        return md;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDefinicion() {
        return definicion;
    }

    public void setDefinicion(String definicion) {
        this.definicion = definicion;
    }

    public Integer getTipoDatoId() {
        return tipoDatoId;
    }

    public void setTipoDatoId(Integer tipoDatoId) {
        this.tipoDatoId = tipoDatoId;
    }

    public Boolean getAutomatico() {
        return automatico;
    }

    public void setAutomatico(Boolean automatico) {
        this.automatico = automatico;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
