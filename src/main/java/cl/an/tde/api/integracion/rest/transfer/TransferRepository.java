package cl.an.tde.api.integracion.rest.transfer;

import org.springframework.data.repository.CrudRepository;

import cl.an.tde.entities.PIT;
import cl.an.tde.entities.Transfer;

import java.util.List;

public interface TransferRepository extends CrudRepository<PIT, Long> {

	List<Transfer> findBySerie(String serie);

}
