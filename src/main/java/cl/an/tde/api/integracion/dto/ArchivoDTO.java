package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.util.Date;

import cl.an.tde.entities.Archivo;

public class ArchivoDTO implements Serializable {

	private static final long serialVersionUID = -4924096458896580668L;

	private Long id;
	private String uuid;
	private String nombre;
	private String metadata;
	private String checksum;
	private Date fecha;

	public ArchivoDTO(Archivo a) {
		this.id = a.getId();
		this.uuid = (a.getUuid()!=null)?a.getUuid().toString():"";
		this.nombre = a.getNombre();
		this.metadata = a.getMetadata();
		this.checksum = a.getChecksum();
		this.fecha = a.getFecha();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
