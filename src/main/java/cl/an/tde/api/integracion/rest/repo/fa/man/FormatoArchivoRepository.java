package cl.an.tde.api.integracion.rest.repo.fa.man;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.an.tde.entities.FormatoArchivo;

public interface FormatoArchivoRepository extends JpaRepository<FormatoArchivo, Integer> {

}
