package cl.an.tde.api.integracion.rest.repo.fa.man;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import cl.an.tde.entities.FormatoArchivo;

@Service
public class FormatoArchivoServiceImpl implements FormatoArchivoServiceInterface {
	
	@Autowired
	private FormatoArchivoRepository formatoArchivoRepository;

	@Override
	public List<FormatoArchivo> findAll() {
		return (List<FormatoArchivo>) formatoArchivoRepository.findAll(sortByIdExtension());
	}

	@Override
	public FormatoArchivo findById(Integer id) {
		return formatoArchivoRepository.findById(id).get();
	}

	@Override
	public void save(FormatoArchivo formatoArchivo) {
		formatoArchivoRepository.save(formatoArchivo);

	}

	private Sort sortByIdExtension() {
		return new Sort(Sort.Direction.ASC, "extension");
	}

}
