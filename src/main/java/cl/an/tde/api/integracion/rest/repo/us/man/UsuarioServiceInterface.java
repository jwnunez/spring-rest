package cl.an.tde.api.integracion.rest.repo.us.man;

import java.util.List;

import cl.an.tde.api.integracion.rest.us.man.FiltroUsuario;
import cl.an.tde.api.integracion.rest.us.pojo.UsuarioPojo;
import cl.an.tde.entities.Usuario;

public interface UsuarioServiceInterface {

	List<Usuario> findByNombres(String nombres);

	Usuario findById(Long id);

	List<Usuario> fetchUsuarios(String rut, String aPaterno, String aMaterno, String nombreUsuario, Integer institucionId);

	List<Usuario> findAll();

	void save(Usuario usuario);

	List<Usuario> findAll(FiltroUsuario fu);

    Boolean rutExiste(String rut);

    Long cuentaRut(String rut);

    Boolean nombreUsuarioExiste(String nombreUsuario);

    Long cuentaNombreUsuario(String nombreUsuario);

    UsuarioPojo obtenerDataUsuario(String user);
    
    Usuario findByIdAndInstitucion(Long idUsuario, Integer idInstitucion);

}
