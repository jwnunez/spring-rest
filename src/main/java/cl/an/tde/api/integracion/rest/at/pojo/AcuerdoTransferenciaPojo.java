package cl.an.tde.api.integracion.rest.at.pojo;

import cl.an.tde.api.integracion.rest.cc.pojo.CuadroClasificacionPojo;
import cl.an.tde.entities.*;
import cl.an.tde.enums.TareaAdT;
import cl.an.tde.utils.ObjectUtil;

import java.time.LocalDate;
import java.util.Date;

public class AcuerdoTransferenciaPojo {

    private Long id;
    private Short anio;
    private Integer institucion;
    private String nombreInstitucion;
    private String codigoInstitucion;
    private Long tablaRetencion;
    private Long cuadroClasificacionId;
    private CuadroClasificacionPojo cuadroClasificacion;
    private String nombreJefe;
    private String actoAdministrativo;
    private ContrapartePojo jefeArchivo;
    private ContrapartePojo usuario;
    private ContrapartePojo archivoNacional;
    private String unidadResponsable;
    private String descripcionContenido;
    private Integer formaOrdenamiento;
    private LocalDate fechaTransferirInicio;
    private LocalDate fechaTransferirTermino;
    private Integer totalUnidadesDocumentales;
    private Integer volumen;
    private Integer unidadMedida;
    private String unidadMedidaNombre;
    private Boolean restriccionAcceso;
    private String descripcionRestriccion;
    private String tipoRestriccion;
    private LocalDate fechaCaducidad;
    private String fuenteLegal;
    private Boolean datosPersonalSensibles;
    private String descripcionDatosSensibles;
    private Short numeroSesion;
    private LocalDate fechaSesion;
    private String descripcionPITTransferir;
    private Integer estado;
    private String estadoNombre;
    private Date fechaTransaccionExitosa;
    private String pitHash;

    /**
     * Constructor.
     */
    public AcuerdoTransferenciaPojo() {
        super();
    }

    public AcuerdoTransferenciaPojo(AcuerdoTransferencia at) {
        super();

        this.id = at.getId();
        this.anio = at.getAnio();
        if (!ObjectUtil.isNull(at.getInstitucion())) {
            this.institucion = at.getInstitucion().getId();
            this.nombreInstitucion = at.getInstitucion().getNombre();
            this.codigoInstitucion = at.getInstitucion().getCodigo();
        }
        if (!ObjectUtil.isNull(at.getTablaRetencion())) {
            this.tablaRetencion = at.getTablaRetencion().getId();
        }
        if (!ObjectUtil.isNull(at.getCuadroClasificacion())) {
            this.cuadroClasificacionId = at.getCuadroClasificacion().getId();
            this.cuadroClasificacion = new CuadroClasificacionPojo(at.getCuadroClasificacion());
        }
        this.nombreJefe = at.getNombreJefe();
        this.actoAdministrativo = at.getActoAdministrativo();
        if (!ObjectUtil.isNull(at.getJefeArchivo())) {
            this.jefeArchivo = new ContrapartePojo(at.getJefeArchivo());
        }
        if (!ObjectUtil.isNull(at.getUsuario())) {
            this.usuario = new ContrapartePojo(at.getUsuario());
        }
        if (!ObjectUtil.isNull(at.getArchivoNacional())) {
            this.archivoNacional = new ContrapartePojo(at.getArchivoNacional());
        }
        this.unidadResponsable = at.getUnidadResponsable();
        this.descripcionContenido = at.getDescripcionContenido();
        if (!ObjectUtil.isNull(at.getFormaOrdenamiento())) {
            this.formaOrdenamiento = at.getFormaOrdenamiento().getCodigo();
        }
        this.fechaTransferirInicio = at.getFechaTransferirInicio();
        this.fechaTransferirTermino = at.getFechaTransferirTermino();
        this.totalUnidadesDocumentales = at.getTotalUnidadesDocumentales();
        this.volumen = at.getVolumen();
        if (!ObjectUtil.isNull(at.getUnidadMedida())) {
            this.unidadMedida = at.getUnidadMedida().getId();
            this.unidadMedidaNombre = at.getUnidadMedida().getNombre();
        }
        this.restriccionAcceso = at.getRestriccionAcceso();
        this.descripcionRestriccion = at.getDescripcionRestriccion();
        this.tipoRestriccion = at.getTipoRestriccion();
        this.fechaCaducidad = at.getFechaCaducidad();
        this.fuenteLegal = at.getFuenteLegal();
        this.datosPersonalSensibles = at.getDatosPersonalSensibles();
        this.descripcionDatosSensibles = at.getDescripcionDatosSensibles();
        this.numeroSesion = at.getNumeroSesion();
        this.fechaSesion = at.getFechaSesion();
        this.descripcionPITTransferir = at.getDescripcionPITTransferir();
        if (!ObjectUtil.isNull(at.getEstado())) {
            this.estado = at.getEstado().getId();
            this.estadoNombre = at.getEstado().getEstado();
        }
    }

    /**
     * Metodo que crea {@link AcuerdoTransferencia}, dependiendo de {@link AcuerdoTransferenciaPojo}.
     *
     * @return AcuerdoTransferencia
     */
    public AcuerdoTransferencia obtenerAcuerdoTransferencia() {
        AcuerdoTransferencia at = new AcuerdoTransferencia();
        at.setId(this.id);
        at.setAnio(this.anio);
        if (!ObjectUtil.isNull(this.institucion)) {
            at.setInstitucion(new Institucion(this.institucion));
        }
        if (!ObjectUtil.isNull(this.tablaRetencion)) {
            at.setTablaRetencion(new TablaRetencion(this.tablaRetencion));
        }
        if (!ObjectUtil.isNull(this.cuadroClasificacionId)) {
            at.setCuadroClasificacion(new CuadroClasificacion(this.cuadroClasificacionId));
        }
        at.setNombreJefe(this.nombreJefe);
        at.setActoAdministrativo(this.actoAdministrativo);
        if (!ObjectUtil.isNull(this.jefeArchivo)) {
            at.setJefeArchivo(this.jefeArchivo.obtenerContraparte());
        }
        if (!ObjectUtil.isNull(this.usuario)) {
            at.setUsuario(this.usuario.obtenerContraparte());
        }
        if (!ObjectUtil.isNull(this.archivoNacional)) {
            at.setArchivoNacional(this.archivoNacional.obtenerContraparte());
        }
        at.setUnidadResponsable(this.unidadResponsable);
        at.setDescripcionContenido(this.descripcionContenido);
        if (!ObjectUtil.isNull(this.formaOrdenamiento)) {
            FormaOrdenamiento fo = FormaOrdenamiento.fromValue(this.formaOrdenamiento);
            at.setFormaOrdenamiento(fo);
        }
        at.setFechaTransferirInicio(this.fechaTransferirInicio);
        at.setFechaTransferirTermino(this.fechaTransferirTermino);
        at.setTotalUnidadesDocumentales(this.totalUnidadesDocumentales);
        at.setVolumen(this.volumen);
        if (!ObjectUtil.isNull(this.unidadMedida)) {
            at.setUnidadMedida(new Unidad(this.unidadMedida));
        }
        at.setRestriccionAcceso(this.restriccionAcceso);
        at.setDescripcionRestriccion(this.descripcionRestriccion);
        at.setTipoRestriccion(this.tipoRestriccion);
        at.setFechaCaducidad(this.fechaCaducidad);
        at.setFuenteLegal(this.fuenteLegal);
        at.setDatosPersonalSensibles(this.datosPersonalSensibles);
        at.setDescripcionDatosSensibles(this.descripcionDatosSensibles);
        at.setNumeroSesion(this.numeroSesion);
        at.setFechaSesion(this.fechaSesion);
        at.setDescripcionPITTransferir(this.descripcionPITTransferir);
        if (!ObjectUtil.isNull(this.estado)) {
            at.setEstado(new EstadoAcuerdoTransferencia(this.estado));
        } else {
            at.setEstado(new EstadoAcuerdoTransferencia(TareaAdT.BORRADOR.getValor()));
        }

        return at;
    }

    public Long getId() { return id; }

	public Short getAnio() {
		return anio;
	}
	public void setAnio(Short anio) {
		this.anio = anio;
	}

    public void setId(Long id) { this.id = id; }

    public Integer getInstitucion() { return institucion; }

    public void setInstitucion(Integer institucion) { this.institucion = institucion; }

    public String getNombreInstitucion() { return nombreInstitucion; }

    public void setNombreInstitucion(String nombreInstitucion) { this.nombreInstitucion = nombreInstitucion; }

    public String getCodigoInstitucion() { return codigoInstitucion; }

    public void setCodigoInstitucion(String codigoInstitucion) { this.codigoInstitucion = codigoInstitucion; }

    public Long getTablaRetencion() { return tablaRetencion; }

    public void setTablaRetencion(Long tablaRetencion) { this.tablaRetencion = tablaRetencion; }

    public Long getCuadroClasificacionId() { return cuadroClasificacionId; }

    public void setCuadroClasificacionId(Long cuadroClasificacionId) { this.cuadroClasificacionId = cuadroClasificacionId; }

    public CuadroClasificacionPojo getCuadroClasificacion() { return cuadroClasificacion; }

    public void setCuadroClasificacion(CuadroClasificacionPojo cuadroClasificacion) { this.cuadroClasificacion = cuadroClasificacion; }

    public String getNombreJefe() { return nombreJefe; }

    public void setNombreJefe(String nombreJefe) { this.nombreJefe = nombreJefe; }

    public String getActoAdministrativo() { return actoAdministrativo; }

    public void setActoAdministrativo(String actoAdministrativo) { this.actoAdministrativo = actoAdministrativo; }

    public ContrapartePojo getJefeArchivo() { return jefeArchivo; }

    public void setJefeArchivo(ContrapartePojo jefeArchivo) { this.jefeArchivo = jefeArchivo; }

    public ContrapartePojo getUsuario() { return usuario; }

    public void setUsuario(ContrapartePojo usuario) { this.usuario = usuario; }

    public ContrapartePojo getArchivoNacional() { return archivoNacional; }

    public void setArchivoNacional(ContrapartePojo archivoNacional) { this.archivoNacional = archivoNacional; }

    public String getUnidadResponsable() { return unidadResponsable; }

    public void setUnidadResponsable(String unidadResponsable) { this.unidadResponsable = unidadResponsable; }

    public String getDescripcionContenido() { return descripcionContenido; }

    public void setDescripcionContenido(String descripcionContenido) { this.descripcionContenido = descripcionContenido; }

    public Integer getFormaOrdenamiento() { return formaOrdenamiento; }

    public void setFormaOrdenamiento(Integer formaOrdenamiento) { this.formaOrdenamiento = formaOrdenamiento; }

    public LocalDate getFechaTransferirInicio() { return fechaTransferirInicio; }

    public void setFechaTransferirInicio(LocalDate fechaTransferirInicio) { this.fechaTransferirInicio = fechaTransferirInicio; }

    public LocalDate getFechaTransferirTermino() { return fechaTransferirTermino; }

    public void setFechaTransferirTermino(LocalDate fechaTransferirTermino) { this.fechaTransferirTermino = fechaTransferirTermino; }

    public Integer getTotalUnidadesDocumentales() { return totalUnidadesDocumentales; }

    public void setTotalUnidadesDocumentales(Integer totalUnidadesDocumentales) { this.totalUnidadesDocumentales = totalUnidadesDocumentales; }

    public Integer getVolumen() { return volumen; }

    public void setVolumen(Integer volumen) { this.volumen = volumen; }

    public Integer getUnidadMedida() { return unidadMedida; }

    public void setUnidadMedida(Integer unidadMedida) { this.unidadMedida = unidadMedida; }

    public String getUnidadMedidaNombre() { return unidadMedidaNombre; }

    public void setUnidadMedidaNombre(String unidadMedidaNombre) { this.unidadMedidaNombre = unidadMedidaNombre; }

    public Boolean getRestriccionAcceso() { return restriccionAcceso; }

    public void setRestriccionAcceso(Boolean restriccionAcceso) { this.restriccionAcceso = restriccionAcceso; }

    public String getDescripcionRestriccion() { return descripcionRestriccion; }

    public void setDescripcionRestriccion(String descripcionRestriccion) { this.descripcionRestriccion = descripcionRestriccion; }

    public String getTipoRestriccion() { return tipoRestriccion; }

    public void setTipoRestriccion(String tipoRestriccion) { this.tipoRestriccion = tipoRestriccion; }

    public LocalDate getFechaCaducidad() { return fechaCaducidad; }

    public void setFechaCaducidad(LocalDate fechaCaducidad) { this.fechaCaducidad = fechaCaducidad; }

    public String getFuenteLegal() { return fuenteLegal; }

    public void setFuenteLegal(String fuenteLegal) { this.fuenteLegal = fuenteLegal; }

    public Boolean getDatosPersonalSensibles() { return datosPersonalSensibles; }

    public void setDatosPersonalSensibles(Boolean datosPersonalSensibles) { this.datosPersonalSensibles = datosPersonalSensibles; }

    public String getDescripcionDatosSensibles() { return descripcionDatosSensibles; }

    public void setDescripcionDatosSensibles(String descripcionDatosSensibles) { this.descripcionDatosSensibles = descripcionDatosSensibles; }

    public Short getNumeroSesion() { return numeroSesion; }

    public void setNumeroSesion(Short numeroSesion) { this.numeroSesion = numeroSesion; }

    public LocalDate getFechaSesion() { return fechaSesion; }

    public void setFechaSesion(LocalDate fechaSesion) { this.fechaSesion = fechaSesion; }

    public String getDescripcionPITTransferir() { return descripcionPITTransferir; }

    public void setDescripcionPITTransferir(String descripcionPITTransferir) { this.descripcionPITTransferir = descripcionPITTransferir; }

    public Integer getEstado() { return estado; }

    public void setEstado(Integer estado) { this.estado = estado; }

    public String getEstadoNombre() { return estadoNombre; }

    public void setEstadoNombre(String estaoNombre) { this.estadoNombre = estaoNombre; }

    public Date getFechaTransaccionExitosa() { return fechaTransaccionExitosa; }

    public void setFechaTransaccionExitosa(Date fechaTransaccionExitosa) { this.fechaTransaccionExitosa = fechaTransaccionExitosa; }

    public String getPitHash() { return pitHash; }

    public void setPitHash(String pitHash) { this.pitHash = pitHash; }
}
