package cl.an.tde.api.integracion.rest.repo.us.man;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.an.tde.api.integracion.rest.us.man.FiltroUsuario;
import cl.an.tde.api.integracion.rest.us.pojo.UsuarioPojo;
import cl.an.tde.entities.Usuario;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.QueryUtil;
import cl.an.tde.utils.StringUtil;

/**
 * @author rfuentes
 */
@Service
public class UsuarioServiceImpl implements UsuarioServiceInterface {

	@Autowired
	private UsuarioRepository usuarioRepository;

    @PersistenceContext()
    private EntityManager em;

    @Override
    public List<Usuario> findByNombres(String nombres) {
        return usuarioRepository.findByNombres(nombres);
    }

    @Override
    public Usuario findById(Long id) {
        return usuarioRepository.findById(id).get();
    }
    
    public Usuario findByIdAndInstitucion(Long idUsuario, Integer idInstitucion) {
    	return usuarioRepository.findByIdAndInstitucion(idUsuario, idInstitucion);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Usuario> findAll(FiltroUsuario fu) {
        final StringBuffer select = new StringBuffer("SELECT new cl.an.tde.api.integracion.rest.us.pojo.UsuarioPojo(u) ");
        select.append(" FROM Usuario u ");
        this.getQueryUsuario(fu, select);
        final Query query = em.createQuery(select.toString());
        this.setParametrosQuery(fu, query);
        return (List<Usuario>) query.getResultList();
    }

    private void setParametrosQuery(FiltroUsuario fu, Query query) {
        if (!StringUtil.isEmpty(fu.getRut()) ) {
            query.setParameter("rut", QueryUtil.concatPercents(fu.getRut()));
        }
        if (!StringUtil.isEmpty(fu.getaPaterno()) ) {
            query.setParameter("aPaterno", QueryUtil.concatPercents(fu.getaPaterno()));
        }
        if (!StringUtil.isEmpty(fu.getaMaterno()) ) {
            query.setParameter("aMaterno", QueryUtil.concatPercents(fu.getaMaterno()));
        }
        if (!ObjectUtil.isNull(fu.getInstitucionId()) ) {
            query.setParameter("institucionId", fu.getInstitucionId());
        }
        if (!StringUtil.isEmpty(fu.getNombreUsuario()) ) {
            query.setParameter("nombreUsuario", QueryUtil.concatPercents(fu.getNombreUsuario()));
        }
    }

    private void getQueryUsuario(FiltroUsuario fu, StringBuffer select) {
        select.append(" WHERE 1=1 ");
        if (!StringUtil.isEmpty(fu.getRut()) ) {
            select.append(" AND UPPER(u.rut) LIKE UPPER(:rut) ");
        }
        if (!StringUtil.isEmpty(fu.getaPaterno()) ) {
            select.append(" AND ( " + QueryUtil.insensitive("u.aPaterno") + " LIKE " + QueryUtil.insensitive(":aPaterno"));
            select.append(" OR " + QueryUtil.insensitive("u.aMaterno") + " LIKE " + QueryUtil.insensitive(":aPaterno") + " )");
        }
        if (!StringUtil.isEmpty(fu.getaMaterno()) ) {
            select.append(" AND " + QueryUtil.insensitive("u.aMaterno") + " LIKE " + QueryUtil.insensitive(":aMaterno"));
        }
        if (!ObjectUtil.isNull(fu.getInstitucionId()) ) {
            select.append(" AND u.institucion.id = :institucionId ");
        }
        if (!StringUtil.isEmpty(fu.getNombreUsuario()) ) {
            select.append(" AND ( " + QueryUtil.insensitive("u.nombreUsuario") + " LIKE " + QueryUtil.insensitive(":nombreUsuario"));
            select.append(" OR " + QueryUtil.insensitive("u.nombres") + " LIKE " + QueryUtil.insensitive(":nombreUsuario") + " )");
        }
        select.append(" ORDER BY u.institucion.id ASC, u.nombreUsuario ASC");
    }

    @Override
    public List<Usuario> fetchUsuarios(String rut, String aPaterno, String aMaterno, String nombreUsuario,
            Integer institucionId) {
        return usuarioRepository.fetchUsuarios(rut, aPaterno, aMaterno, nombreUsuario, institucionId);
    }

    @Override
    public List<Usuario> findAll() {
        return (List<Usuario>) usuarioRepository.findAll();
    }

    @Override
    public void save(Usuario usuario) {
        usuarioRepository.save(usuario);
    }

    @Override
    public Long cuentaRut(String rut) {
        final StringBuilder select = new StringBuilder();
        select.append("SELECT COUNT(u.id) ");
        select.append("FROM Usuario u ");
        select.append("WHERE UPPER(u.rut) = UPPER(:rut) ");
        Query query = em.createQuery(select.toString());
        query.setParameter("rut", rut);

        Long count = (long) query.getSingleResult();
        return count;
    }

    @Override
    public Boolean rutExiste(String rut) {
        Long count = this.cuentaRut(rut);
        if (count > 0 ) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public Boolean nombreUsuarioExiste(String nombreUsuario) {
        Long count = this.cuentaNombreUsuario(nombreUsuario);
        if (count > 0 ) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public Long cuentaNombreUsuario(String nombreUsuario) {
        final StringBuilder select = new StringBuilder();
        select.append("SELECT COUNT(u.id) ");
        select.append("FROM Usuario u ");
        select.append("WHERE UPPER(u.nombreUsuario) = UPPER(:nombreUsuario) ");
        Query query = em.createQuery(select.toString());
        query.setParameter("nombreUsuario", nombreUsuario);

        Long count = (long) query.getSingleResult();
        return count;
    }

    @Override
    public UsuarioPojo obtenerDataUsuario(String user) {
        final StringBuilder select = new StringBuilder();
        select.append("SELECT new cl.an.tde.api.integracion.rest.us.pojo.UsuarioPojo(u) ");
        select.append("FROM Usuario u ");
        select.append("WHERE UPPER(u.nombreUsuario) = UPPER(:nombreUsuario) ");
        Query query = em.createQuery(select.toString());
        query.setParameter("nombreUsuario", user);

        final List<UsuarioPojo> usuarios = (List<UsuarioPojo>) query.getResultList();
        if (!usuarios.isEmpty() ) {
            return usuarios.get(0);
        }
        
        return null;
    }

}
