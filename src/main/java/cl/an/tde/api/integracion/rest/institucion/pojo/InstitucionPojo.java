package cl.an.tde.api.integracion.rest.institucion.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.an.tde.entities.AmbitoAccion;
import cl.an.tde.entities.EstatutoJuridico;
import cl.an.tde.entities.Funcion;
import cl.an.tde.entities.Institucion;
import cl.an.tde.utils.ObjectUtil;

/**
 * @author rfuentes.
 */
public class InstitucionPojo {

    private Integer id;
    private String nombre;
    private String sigla;
    private String codigo;
    private Integer anioCreacion;
    private String historia;
    private Long ambitoId;
    private String ambito;
    private String ambitoIn;
    private Long estatutoId;
    private String estatuto;
    private String fuentes;
    private String estructura;
    private String registroAuth;
    private Date fechaMod;
    private String notas;
    private List<FuncionPojo> funciones;
    private Boolean estado;

    /**
     * Constructor.
     */
    public InstitucionPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param institucion
     */
    public InstitucionPojo(Institucion institucion) {
        super();
        this.id = institucion.getId();
        this.nombre = institucion.getNombre();
        this.sigla = institucion.getSigla();
        this.codigo = institucion.getCodigo();
        this.anioCreacion = institucion.getAnioCreacion();
        this.historia = institucion.getHistoria();
        if (!ObjectUtil.isNull(institucion.getAmbito()) ) {
            this.ambitoId = institucion.getAmbito().getId();
            this.ambito = institucion.getAmbito().getNombre();
        }
        this.ambitoIn = institucion.getAmbitoIn();
        if (!ObjectUtil.isNull(institucion.getEstatuto()) ) {
            this.estatutoId = institucion.getEstatuto().getId();
            this.estatuto = institucion.getEstatuto().getNombre();
        }
        this.fuentes = institucion.getFuentes();
        this.estructura = institucion.getEstructura();
        this.registroAuth = institucion.getRegistroAuth();
        this.fechaMod = institucion.getFechaMod();
        this.notas = institucion.getNotas();
        if (!ObjectUtil.isNull(institucion.getFunciones()) ) {
            List<FuncionPojo> fp = new ArrayList<FuncionPojo>();
            for (Funcion f : institucion.getFunciones()) {
                fp.add(new FuncionPojo(f));
            }
            this.funciones = fp;
        }
        this.estado = institucion.getEstado();
    }

    public Institucion obtenerInstitucion() {
        final Institucion institucion = new Institucion(this.id);
        institucion.setNombre(this.nombre);
        institucion.setSigla(this.sigla);
        institucion.setCodigo(this.codigo);
        institucion.setAnioCreacion(this.anioCreacion);
        institucion.setHistoria(this.historia);
        if (!ObjectUtil.isNull(this.ambitoId) ) {
            institucion.setAmbito(new AmbitoAccion(this.ambitoId));
        }
        institucion.setAmbitoIn(this.ambitoIn);
        if (!ObjectUtil.isNull(this.estatutoId) ) {
            institucion.setEstatuto(new EstatutoJuridico(this.estatutoId));
        }
        institucion.setFuentes(this.fuentes);
        institucion.setEstructura(this.estructura);
        institucion.setRegistroAuth(this.registroAuth);
        institucion.setFechaMod(new Date());
        institucion.setNotas(this.notas);
        if (!ObjectUtil.isNull(this.funciones) ) {
            List<Funcion> funciones = new ArrayList<Funcion>();
            for (FuncionPojo f : this.funciones) {
                final Funcion funcion = f.obtenerFuncion();
                funcion.setInstitucion(institucion);
                funciones.add(funcion);
            }
            institucion.setFunciones(funciones);
        }
        institucion.setEstado(Boolean.TRUE);
        return institucion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getAnioCreacion() {
        return anioCreacion;
    }

    public void setAnioCreacion(Integer anioCreacion) {
        this.anioCreacion = anioCreacion;
    }

    public String getHistoria() {
        return historia;
    }

    public void setHistoria(String historia) {
        this.historia = historia;
    }

    public Long getAmbitoId() {
        return ambitoId;
    }

    public void setAmbitoId(Long ambitoId) {
        this.ambitoId = ambitoId;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public String getAmbitoIn() {
        return ambitoIn;
    }

    public void setAmbitoIn(String ambitoIn) {
        this.ambitoIn = ambitoIn;
    }

    public Long getEstatutoId() {
        return estatutoId;
    }

    public void setEstatutoId(Long estatutoId) {
        this.estatutoId = estatutoId;
    }

    public String getEstatuto() {
        return estatuto;
    }

    public void setEstatuto(String estatuto) {
        this.estatuto = estatuto;
    }

    public String getFuentes() {
        return fuentes;
    }

    public void setFuentes(String fuentes) {
        this.fuentes = fuentes;
    }

    public String getEstructura() {
        return estructura;
    }

    public void setEstructura(String estructura) {
        this.estructura = estructura;
    }

    public String getRegistroAuth() {
        return registroAuth;
    }

    public void setRegistroAuth(String registroAuth) {
        this.registroAuth = registroAuth;
    }

    public Date getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Date fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public List<FuncionPojo> getFunciones() {
        return funciones;
    }

    public void setFunciones(List<FuncionPojo> funciones) {
        this.funciones = funciones;
    }

    public Boolean getEstado() { return estado; }

    public void setEstado(Boolean estado) { this.estado = estado; }
}
