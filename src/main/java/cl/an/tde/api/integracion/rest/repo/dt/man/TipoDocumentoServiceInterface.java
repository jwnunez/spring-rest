package cl.an.tde.api.integracion.rest.repo.dt.man;

import java.util.List;

import cl.an.tde.entities.TipoDocumento;

public interface TipoDocumentoServiceInterface {

    List<TipoDocumento> findAll();

    TipoDocumento findById(Long id);

    void save(TipoDocumento tipoDocumento);

}
