package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import cl.an.tde.entities.AcuerdoTransferencia;

public class AdtDTO implements Serializable {

	private static final long serialVersionUID = 1718695887359207016L;

	private Integer id;
	private LocalDate fechaSesion;
	private String fechaSesionString;
	private LocalDate fechaTransferirInicio;
	private String fechaTransferirInicioString;
	private LocalDate fechaTransferirTermino;
	private String fechaTransferirTerminoString;
	private String descripcionPit;
	private Integer cantidadDocs;
	private String estado;
	private Integer estadoId;

	public AdtDTO(AcuerdoTransferencia cc) {
		super();
		this.id = cc.getId().intValue();
		this.fechaSesion = cc.getFechaSesion();
		this.fechaTransferirInicio = cc.getFechaTransferirInicio();
		this.fechaTransferirTermino = cc.getFechaTransferirTermino();
		this.setFechaSesionString();
		this.setFechaTransferirInicioString();
		this.setFechaTransferirTerminoString();
		this.descripcionPit = cc.getDescripcionPITTransferir();
		this.cantidadDocs = cc.getTotalUnidadesDocumentales();
		this.estado = cc.getEstado().getEstado();
		this.estadoId = cc.getEstado().getId();
	}
	

	public Integer getId() {
		return id;
	}

	public String getFechaSesionString() {
		return fechaSesionString;
	}

	public void setFechaSesionString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		this.fechaSesionString = this.getFechaSesion().format(formatter);

	}
	

	public LocalDate getFechaTransferirInicio() {
		return fechaTransferirInicio;
	}


	public void setFechaTransferirInicio(LocalDate fechaTransferirInicio) {
		this.fechaTransferirInicio = fechaTransferirInicio;
	}


	public String getFechaTransferirInicioString() {
		return fechaTransferirInicioString;
	}


	public void setFechaTransferirInicioString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		this.fechaTransferirInicioString = this.getFechaTransferirInicio().format(formatter);
	}


	public LocalDate getFechaTransferirTermino() {
		return fechaTransferirTermino;
	}


	public void setFechaTransferirTermino(LocalDate fechaTransferirTermino) {
		this.fechaTransferirTermino = fechaTransferirTermino;
	}


	public String getFechaTransferirTerminoString() {
		return fechaTransferirTerminoString;
	}


	public void setFechaTransferirTerminoString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		this.fechaTransferirTerminoString = this.getFechaTransferirTermino().format(formatter);
	}


	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getFechaSesion() {
		return fechaSesion;
	}

	public void setFechaSesion(LocalDate fechaSesion) {
		this.fechaSesion = fechaSesion;
	}

	public String getDescripcionPit() {
		return descripcionPit;
	}

	public void setDescripcionPit(String descripcionPit) {
		this.descripcionPit = descripcionPit;
	}

	public Integer getCantidadDocs() {
		return cantidadDocs;
	}

	public void setCantidadDocs(Integer cantidadDocs) {
		this.cantidadDocs = cantidadDocs;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(Integer estadoId) {
		this.estadoId = estadoId;
	}
}
