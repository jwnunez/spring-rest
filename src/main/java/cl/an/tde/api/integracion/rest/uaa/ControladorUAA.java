package cl.an.tde.api.integracion.rest.uaa;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.google.gson.JsonObject;

import cl.an.tde.Controlador;
import cl.an.tde.api.integracion.pojo.OAuthParam;
import cl.an.tde.utils.CustomException;
import cl.an.tde.utils.Respuesta;

@RestController
@RequestMapping(value = "/uaa", name = "/uaa")
public class ControladorUAA extends Controlador {
	private static final Logger LOG = LoggerFactory.getLogger(ControladorUAA.class);

	@Autowired
	protected ServicioUAA servicio;

	@PostMapping("/login")
	public Respuesta<String> login(
			@RequestHeader(value="Authorization") String authorization,
			@RequestParam("grant_type") String grantType,
			@RequestParam("username") String username,
			@RequestParam("password") String password ) {
		
		LOG.info("-> Login: " + username);
		
		final OAuthParam auth = new OAuthParam();
		
		auth.setAuthorization(authorization);
		auth.setGrantType(grantType);
		auth.setUsername(username);
		auth.setPassword(password);
		
		Respuesta<String> response = new Respuesta<String>();
		response.setData(new String());
		response.setStatus(500);
		
		try {
			response.setData(servicio.getOAuthToken(auth));
			response.setStatus(200);
			response.setMensaje("OK");
			return response;
		} catch (IOException e) {
			response.setMensaje(e.getMessage());
		} catch (CustomException e) {
			response.setMensaje(e.getMessage());
		}
		
		return response;
	}
	
}
