package cl.an.tde.api.integracion.rest.institucion;

import java.util.List;

import cl.an.tde.api.integracion.rest.institucion.pojo.InstitucionPojo;
import cl.an.tde.entities.Institucion;

/**
 * @author rfuentes
 */
public interface InstitucionInterface {

    Institucion findById(Integer id);
    
    List<Institucion> findAll(FiltroInstitucion fi);

    List<InstitucionPojo> findAll();

}
