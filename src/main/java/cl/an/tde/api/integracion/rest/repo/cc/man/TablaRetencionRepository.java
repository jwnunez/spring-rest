package cl.an.tde.api.integracion.rest.repo.cc.man;

import org.springframework.data.repository.CrudRepository;

import cl.an.tde.entities.TablaRetencion;

/**
 * @author rfuentes
 */
public interface TablaRetencionRepository extends CrudRepository<TablaRetencion, Long> {

}
