package cl.an.tde.api.integracion.rest.repo.md.man;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.an.tde.entities.MetaDato;

@Service
public class MetaDatoServiceImpl implements MetaDatoServiceInterface {

	@Autowired
	private MetaDatoRepository metaDatoRepository;

	@Override
	public List<MetaDato> findAll() {
		return (List<MetaDato>) metaDatoRepository.findAll();
	}

	@Override
	public List<MetaDato> findByType(Integer tud) {
		if (tud == 0)
			return metaDatoRepository.getMetadataForFile();
		else
			return metaDatoRepository.getMetadataForFileExp();

	}

	@Override
	public MetaDato findById(Integer id) {
		return metaDatoRepository.findById(id).get();
	}

	@Override
	public void save(MetaDato metaDato) {
		metaDatoRepository.save(metaDato);
	}
}
