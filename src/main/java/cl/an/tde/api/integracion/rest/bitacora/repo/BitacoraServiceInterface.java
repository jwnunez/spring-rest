package cl.an.tde.api.integracion.rest.bitacora.repo;

import java.util.List;

import cl.an.tde.api.integracion.dto.BitacoraDTO;
import cl.an.tde.api.integracion.dto.BusquedaDTO;
import cl.an.tde.entities.Bitacora;

public interface BitacoraServiceInterface {

	List<Bitacora> findAll();

	List<Bitacora> findLast();

	List<BitacoraDTO> findFilter(BusquedaDTO params);

	List observaciones(Long idAdt);
}