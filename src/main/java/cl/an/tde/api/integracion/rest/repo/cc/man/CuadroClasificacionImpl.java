package cl.an.tde.api.integracion.rest.repo.cc.man;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Comparator;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cl.an.tde.api.integracion.pojo.Comun;
import cl.an.tde.api.integracion.rest.cc.pojo.CuadroClasificacionPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.TablaRetencionPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.TreePojo;
import net.sf.jasperreports.engine.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import cl.an.tde.entities.CuadroClasificacion;
import cl.an.tde.entities.NivelDescripcion;
import cl.an.tde.utils.ObjectUtil;

/**
 * @author rfuentes
 */
@Service
public class CuadroClasificacionImpl implements CuadroClasificacionInterface {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private CuadroClasificacionRepository cuadroClasificacionRepository;

    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public CuadroClasificacion findById(Long id) {
        return cuadroClasificacionRepository.findById(id).get();
    }

    @Override
    public void save(CuadroClasificacion cuadroClasificacion) {
        cuadroClasificacionRepository.save(cuadroClasificacion);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TreePojo> obtenerTreeByInstitucion(Integer institucionId) {
        final StringBuilder select = new StringBuilder("select cc from CuadroClasificacion cc ");
        select.append(" LEFT JOIN cc.institucion i ");
        select.append("WHERE cc.clasificacionParent is null ");
        select.append(" AND i.id = :institucionId ");

        final Query query = em.createQuery(select.toString());
        query.setParameter("institucionId", institucionId);
        final List<CuadroClasificacion> lista = (List<CuadroClasificacion>) query.getResultList();
        return this.cuadroClasificacionToTreePojo(lista, null);
    }

    private List<TreePojo> cuadroClasificacionToTreePojo(List<CuadroClasificacion> lista, TreePojo parent) {
        List<TreePojo> trees = new ArrayList<>();
        for (CuadroClasificacion cc : lista) {
            final TreePojo tree = new TreePojo(cc.getId(),
                    !ObjectUtil.isNull(cc.getClasificacionParent()) ? cc.getClasificacionParent().getId() : null,
                    cc.getNombreNivel(),
                    !ObjectUtil.isNull(cc.getNivelDescripcion()) ? cc.getNivelDescripcion().getNombre() : "Fondo",
                    cc.getHoja(),
                    !ObjectUtil.isNull(cc.getNivelDescripcion()) ? cc.getNivelDescripcion().getId() : NivelDescripcion.FONDO);
            if (!ObjectUtil.isNull(parent)) {
                tree.setNivelIdParent(!ObjectUtil.isNull(parent.getNivelId()) ? parent.getNivelId() : NivelDescripcion.FONDO);
            }

            if (!ObjectUtil.isNull(cc.getClasificaciones()) && !cc.getClasificaciones().isEmpty() ) {
                final List<TreePojo> children = cuadroClasificacionToTreePojo(cc.getClasificaciones(), tree);
                tree.setChildren(children);
            }
            trees.add(tree);
        }
        trees.sort(Comparator.comparing(TreePojo::getName));        // los ordena alfabéticamente
        //trees.sort((o1, o2) -> (int) (o1.getId() - o2.getId()));  // los ordena por Id de menor a mayor
        return trees;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TablaRetencionPojo> tablaRetencionByInstitucion(Integer institucionId) {
        Query query = queryTablaRetencion(institucionId, null);
        final List<TablaRetencionPojo> lista = (List<TablaRetencionPojo>) query.getResultList();
        return lista;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TablaRetencionPojo> tablaRetencionByInstitucionAndSubfondo(Integer institucionId, Long subfondoId) {
        Query query = queryTablaRetencion(institucionId, subfondoId);
        final List<TablaRetencionPojo> lista = (List<TablaRetencionPojo>) query.getResultList();
        return lista;
    }

    private Query queryTablaRetencion(Integer institucionId, Long subfondoId) {
        final StringBuilder select = new StringBuilder(
                "select new cl.an.tde.api.integracion.rest.cc.pojo.TablaRetencionPojo( ");
        select.append(
                " cc.serie, cc.id, nd.nombre, tr.id, tr.archivoGestionAnios, tr.archivoCentralInstitucionalAnios, ");
        select.append(" tr.archivoNacionalAnios, tr.conservacionPermanente, tr.transferencia, tr.fechaCreacion, tr.fundamento, tr.soporte) ");
        select.append(" FROM CuadroClasificacion cc ");
        select.append(" LEFT JOIN cc.nivelDescripcion nd ");
        select.append(" LEFT JOIN cc.tablaRetencion tr ");
        select.append(" LEFT JOIN cc.institucion i ");
        select.append("WHERE i.id = :institucionId ");
        select.append(" AND cc.hoja = true ");
        select.append(" AND nd.id IN (3, 4) ");  // 3 es Serie, 4 es Subserie
        if (subfondoId != null) {
            select.append(" AND (cc.clasificacionParent.id = :subfondoId OR cc.clasificacionParent.clasificacionParent.id = :subfondoId)");
        }
        final Query query = em.createQuery(select.toString());
        query.setParameter("institucionId", institucionId);
        if (subfondoId != null) query.setParameter("subfondoId", subfondoId);

        return query;
    }

    @Override
    public List<Comun> subFondos(Integer institucion) {
        final StringBuilder select = new StringBuilder("select new cl.an.tde.api.integracion.pojo.Comun( ");
        select.append("c.id, c.nombreNivel) FROM CuadroClasificacion c ");
        select.append(" LEFT JOIN c.institucion i ");
        select.append(" LEFT JOIN c.nivelDescripcion nd ");
        select.append(" WHERE i.id = :institucion ");
        select.append(" AND nd.id = 2 ");
        final Query query = em.createQuery(select.toString());
        query.setParameter("institucion", institucion);
        final List<Comun> lista = (List<Comun>) query.getResultList();
        return lista;
    }

    @Override
    public void listaCuadroClasificacion(CuadroClasificacion cc, List<CuadroClasificacionPojo> trees) {
        if (cc.getHoja() && !ObjectUtil.isNull(cc.getTablaRetencion())) {
            trees.add(new CuadroClasificacionPojo(cc));
        } else {
            // La función entra por aquí en el AdT
            if (!ObjectUtil.isNull(cc.getClasificaciones()) && !cc.getClasificaciones().isEmpty()) {
                for (CuadroClasificacion cc1 : cc.getClasificaciones()) {
                    listaCuadroClasificacion(cc1, trees);
                }
            }
        }
    }

    @Override
    public JasperPrint exportPdfFile() throws JRException, IOException {

        String path = resourceLoader.getResource("classpath:acuerdoTransferencia.jrxml").getURI().getPath();

        JasperReport jasperReport = JasperCompileManager.compileReport(path);

        // Parameters for report
        Map<String, Object> parameters = new HashMap<String, Object>();

        JasperPrint print = JasperFillManager.fillReport(jasperReport, parameters);

        return print;
    }
}
