package cl.an.tde.api.integracion.dto;

import java.io.Serializable;

public class ErrorTrans implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4905630163295114543L;

	private int codigo;
	private String mensaje;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
