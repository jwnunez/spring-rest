package cl.an.tde.api.integracion.rest.repo.at.man;

import cl.an.tde.entities.AcuerdoTransferencia;
import org.springframework.data.repository.CrudRepository;

/**
 * @author rfuentes
 *
 */
public interface AcuerdoTransferenciaRepository extends CrudRepository<AcuerdoTransferencia, Long> {
}
