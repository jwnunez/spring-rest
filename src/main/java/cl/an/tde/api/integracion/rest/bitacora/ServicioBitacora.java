package cl.an.tde.api.integracion.rest.bitacora;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import cl.an.tde.api.integracion.rest.bitacora.repo.BitacoraRepository;
import cl.an.tde.api.integracion.rest.repo.us.man.UsuarioRepository;
import cl.an.tde.entities.AcuerdoTransferencia;
import cl.an.tde.entities.Bitacora;
import cl.an.tde.entities.BitacoraAdT;
import cl.an.tde.entities.Usuario;
import cl.an.tde.enums.TipoAccion;

@Service
@Transactional
public class ServicioBitacora {

	@Autowired
	BitacoraRepository bitacora;
	
	@Autowired
	UsuarioRepository repoUser;

	public void crear(TipoAccion tipo, Integer idUser, String detail) {
		
		Optional<Usuario> user = repoUser.findById(idUser.longValue());
		
		Bitacora b = new Bitacora();
		
		b.setFecha(new Date());
		b.setIdInstitucion(user.get().getInstitucion().getId());
		b.setInstitucion(user.get().getInstitucion().getNombre());
		b.setIdUsuario(user.get().getId().intValue());
		b.setUsuario(user.get().getNombreUsuario());
		b.setTipoAccion(tipo.getValor());
		b.setAccion(tipo.getNombre());
		b.setDetalle(detail);
		
		bitacora.save(b);
	}

	public void procesoAdT(TipoAccion tipo, AcuerdoTransferencia adt, Usuario user, String detail, String observacion) {
		
		BitacoraAdT b = new BitacoraAdT();
		
		b.setFecha(new Date());
		b.setIdInstitucion(user.getInstitucion().getId());
		b.setInstitucion(user.getInstitucion().getNombre());
		b.setIdUsuario(user.getId().intValue());
		b.setUsuario(user.getNombreUsuario());
		b.setTipoAccion(tipo.getValor());
		b.setAccion(tipo.getNombre());
		b.setDetalle(detail);
		b.setAdt(adt);
		b.setObservacion(observacion);

		bitacora.save(b);
	}

}