package cl.an.tde.api.integracion.rest.institucion;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import cl.an.tde.Controlador;
import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.institucion.pojo.InstitucionPojo;
import cl.an.tde.entities.Institucion;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.List;

@RestController
@RequestMapping(value = "/institution", name = "/institution")
public class ControladorInstitucion extends Controlador {

	@Autowired
	private InstitucionRepository institucionRepository;

	@Autowired
	private InstitucionInterface InstitucionInterface;

	@Autowired
	ServicioBitacora bitacora;

	@PersistenceContext
	EntityManager em;

	@PostMapping("/create")
	public Respuesta<Boolean> crear(
			@RequestHeader(value="User") Integer user,
			@RequestBody InstitucionPojo obj
			) {
		final Institucion tmp = institucionRepository.findByCodigo(obj.getCodigo());
		if ( !ObjectUtil.isNull(tmp) ) {
			Respuesta<Boolean> r = new Respuesta<Boolean>(Boolean.FALSE, 1);
			r.setMensaje("Código de Institución ya existe");
			return r;
		}
		final Institucion institucion = obj.obtenerInstitucion();
		institucionRepository.save(institucion);
		bitacora.crear(TipoAccion.CREATE, user, "Institución:" + obj.getSigla());
		return new Respuesta<Boolean>(Boolean.TRUE, 0);
	}

	@PutMapping(value = "/update")
	public Respuesta<InstitucionPojo> actualizar(@RequestHeader(value="User") Integer user, @RequestBody InstitucionPojo obj) {
		final Institucion institucion = obj.obtenerInstitucion();
		final Boolean editar = !ObjectUtil.isNull(institucion.getId()) ? Boolean.TRUE : Boolean.FALSE;
		institucionRepository.save(institucion);
		bitacora.crear(editar ? TipoAccion.UPDATE: TipoAccion.CREATE, user, "Institución:" + obj.getSigla());
		return new Respuesta<InstitucionPojo>(obj, 0);
	}

	@GetMapping("/list")
	public Respuesta<List<InstitucionPojo>> listar() {
		return new Respuesta<List<InstitucionPojo>>((List<InstitucionPojo>) InstitucionInterface.findAll(), 0);
	}

	@GetMapping(value = "/get/{id}")
	public Respuesta<InstitucionPojo> obtenerInstitucionPorId(@PathVariable Integer id) {
		final Institucion institucion = institucionRepository.findById(id).get();
		return new Respuesta<InstitucionPojo>(new InstitucionPojo(institucion), 0);
	}

	@PutMapping(value = "/delete/{id}")
	public Respuesta<Boolean> eliminar(@RequestHeader(value="User") Integer user, @PathVariable Integer id) {
		final Institucion institucion = institucionRepository.findById(id).get();
		institucion.setEstado(!institucion.getEstado());
		institucionRepository.save(institucion);
		bitacora.crear(TipoAccion.DISABLE, user, "Institución:" + institucion.getSigla());
		return new Respuesta<Boolean>(Boolean.TRUE, 0);
	}

	@GetMapping("/get")
	public Institucion obtenerClientePorNombre(@RequestParam(name = "nombre", required = true) String nombre) {
		Query query = em.createQuery("Select i From Institucion i Where i.nombre Like ?");
		query.setParameter(0, nombre);
		try {
			return (Institucion) query.getSingleResult();
		} catch (Exception ex) {
			return null;
		}
	}

	@PostMapping("/listarFiltro")
	public Respuesta<List<Institucion>> listarFiltro(@RequestBody FiltroInstitucion fi) {
		final List<Institucion> find = (List<Institucion>) InstitucionInterface.findAll(fi);
		final Respuesta<List<Institucion>> respuesta = new Respuesta<List<Institucion>>(find, 0);
		return respuesta;
	}

}
