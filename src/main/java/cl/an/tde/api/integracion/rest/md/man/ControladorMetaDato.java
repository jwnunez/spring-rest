package cl.an.tde.api.integracion.rest.md.man;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.md.pojo.MetaDatoPojo;
import cl.an.tde.api.integracion.rest.repo.md.man.MetaDatoServiceInterface;
import cl.an.tde.entities.MetaDato;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;

@RestController
@RequestMapping(value = "/metadato", name = "/metadato")
@CrossOrigin(origins = "*")
public class ControladorMetaDato implements Serializable {

    private static final long serialVersionUID = 8995341384600331388L;

    @Autowired
    private MetaDatoServiceInterface metaDatoServiceInterface;

	@Autowired
	ServicioBitacora bitacora;

    @GetMapping("/listar")
    public Respuesta<List<MetaDato>> listar() {
        Respuesta<List<MetaDato>> respuesta = new Respuesta<List<MetaDato>>(
                (List<MetaDato>) metaDatoServiceInterface.findAll(), 0);
        return respuesta;
    }
    
    @GetMapping("/getMetaForFile/{tud}")
    public Respuesta<List<MetaDato>> obtenerMetadatoParaArchivo(@PathVariable Integer tud) {
        Respuesta<List<MetaDato>> respuesta = new Respuesta<List<MetaDato>>(
                (List<MetaDato>) metaDatoServiceInterface.findByType(tud), 0);
        return respuesta;
    }

    @GetMapping(value = "/get/{id}")
    public Respuesta<MetaDato> obtenerMetaDatoById(@PathVariable Integer id) {
        return new Respuesta<MetaDato>((MetaDato) metaDatoServiceInterface.findById(id), 0);
    }

    @PutMapping(value = "/update")
    public Respuesta<MetaDato> actualizar(@RequestHeader(value="User") Integer user, @RequestBody MetaDato metaDato) {
        metaDatoServiceInterface.save(metaDato);
        bitacora.crear(TipoAccion.UPDATE, user, "Metadato:" + metaDato.getNombre());
        return new Respuesta<MetaDato>(metaDato, 0);
    }

    @PostMapping(value = "/save")
    public Respuesta<MetaDato> crear(@RequestHeader(value="User") Integer user, @RequestBody MetaDatoPojo md) {
        final MetaDato metaDato = md.getMetaDato();
        final Boolean editar = !ObjectUtil.isNull(metaDato.getId()) ? Boolean.TRUE : Boolean.FALSE;
        metaDatoServiceInterface.save(metaDato);
        bitacora.crear(editar ? TipoAccion.UPDATE: TipoAccion.CREATE, user, "Metadato:" + metaDato.getNombre());
        return new Respuesta<MetaDato>(metaDato, 0);
    }

    @PutMapping(value = "/delete/{id}")
    public Respuesta<MetaDato> actualizarEstado(@RequestHeader(value="User") Integer user, @PathVariable Integer id) {
        MetaDato md = metaDatoServiceInterface.findById(id);
        md.setEstado(!md.getEstado());
        metaDatoServiceInterface.save(md);
        bitacora.crear(TipoAccion.DISABLE, user, "Metadato:" + md.getNombre());
        return new Respuesta<MetaDato>(md, 0);
    }
}
