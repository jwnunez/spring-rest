package cl.an.tde.api.integracion.rest.cc.pojo;

import java.util.List;

/**
 * @author rfuentes.
 */
public class ClasificacionPojo {

    private Long clasificacionId;
    private Integer institucionId;
    private Integer version;
    private String codigo;
    private List<TreePojo> tree;
    private List<TablaRetencionPojo> tablaRetencion;

    /**
     * Constructor.
     */
    public ClasificacionPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param clasificacionId {@link Long}
     * @param institucionId {@link Long}
     * @param tree {@link List}{@code<}{@link TreePojo}{@code>}
     * @param version {@link Integer}
     * @param codigo {@link StringBuilder}
     */
    public ClasificacionPojo(Long clasificacionId, Integer institucionId, List<TreePojo> tree, Integer version,
            String codigo) {
        super();
        this.clasificacionId = clasificacionId;
        this.institucionId = institucionId;
        this.tree = tree;
        this.version = version;
        this.codigo = codigo;
    }

    /**
     * Constructor con parametros.
     * 
     * @param clasificacionId {@link Long}
     * @param institucionId {@link Long}
     * @param version {@link Integer}
     * @param codigo {@link StringBuilder}
     * @param tablaRetencion {@link List}{@code<}{@link TablaRetencionPojo}{@code>}
     */
    public ClasificacionPojo(Long clasificacionId, Integer institucionId, Integer version, String codigo,
            List<TablaRetencionPojo> tablaRetencion) {
        super();
        this.clasificacionId = clasificacionId;
        this.institucionId = institucionId;
        this.version = version;
        this.codigo = codigo;
        this.tablaRetencion = tablaRetencion;
    }

    /**
     * Constructor con parametros.
     * 
     * @param institucionId {@link Integer}
     * @param tablaRetencion {@link List}{@code<}{@link TablaRetencionPojo}{@code>}
     */
    public ClasificacionPojo(Integer institucionId, List<TablaRetencionPojo> tablaRetencion) {
        this.institucionId = institucionId;
        this.tablaRetencion = tablaRetencion;
    }

    public Long getClasificacionId() {
        return clasificacionId;
    }

    public void setClasificacionId(Long clasificacionId) {
        this.clasificacionId = clasificacionId;
    }

    public Integer getInstitucionId() {
        return institucionId;
    }

    public void setInstitucionId(Integer institucionId) {
        this.institucionId = institucionId;
    }

    public List<TreePojo> getTree() {
        return tree;
    }

    public void setTree(List<TreePojo> tree) {
        this.tree = tree;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<TablaRetencionPojo> getTablaRetencion() {
        return tablaRetencion;
    }

    public void setTablaRetencion(List<TablaRetencionPojo> tablaRetencion) {
        this.tablaRetencion = tablaRetencion;
    }

}
