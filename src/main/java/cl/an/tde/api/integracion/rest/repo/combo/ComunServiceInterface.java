package cl.an.tde.api.integracion.rest.repo.combo;

import java.util.List;

import cl.an.tde.api.integracion.dto.ComboDTO;
import cl.an.tde.api.integracion.pojo.Comun;

public interface ComunServiceInterface {

	List<Comun> listarEntity(String entity, String llave, String valor);

	List<Comun> listarEntity(String entity, String llave, String valor, Boolean activado);

	List<Comun> getNiveles(Long nivelId);

	List<Comun> funcionesActivas(Integer institucionId, Boolean estado);

	List<Long> idFuncionesUsadas(Integer institucionId);

	List<Comun> listaTipoAcciones();

	List<ComboDTO> usuariosByInstitucion(Integer idInstitucion);

	List<ComboDTO> unidadInstalacion();

}
