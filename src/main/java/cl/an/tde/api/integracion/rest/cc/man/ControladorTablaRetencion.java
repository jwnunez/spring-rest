package cl.an.tde.api.integracion.rest.cc.man;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.an.tde.api.integracion.rest.bitacora.ServicioBitacora;
import cl.an.tde.api.integracion.rest.cc.pojo.CuadroClasificacionPojo;
import cl.an.tde.api.integracion.rest.cc.pojo.TablaRetencionPojo;
import cl.an.tde.api.integracion.rest.repo.cc.man.ClasificacionInterface;
import cl.an.tde.api.integracion.rest.repo.cc.man.CuadroClasificacionInterface;
import cl.an.tde.api.integracion.rest.repo.cc.man.TablaRetencionInterface;
import cl.an.tde.entities.Clasificacion;
import cl.an.tde.entities.CuadroClasificacion;
import cl.an.tde.entities.TablaRetencion;
import cl.an.tde.enums.TipoAccion;
import cl.an.tde.utils.ObjectUtil;
import cl.an.tde.utils.Respuesta;

@RestController
@RequestMapping(value = "/tablaRetencion", name = "/tablaRetencion")
@CrossOrigin(origins = "*")
public class ControladorTablaRetencion {

    @Autowired
    private TablaRetencionInterface tablaRetencionInterface;

    @Autowired
    private ClasificacionInterface clasificacionInterface;

    @Autowired
    private CuadroClasificacionInterface cuadroClasificacionInterface;

	@Autowired
	ServicioBitacora bitacora;

    @PostMapping(value = "/save")
    public Respuesta<Boolean> crear(@RequestHeader(value="User") Integer user, @RequestBody TablaRetencionPojo tr) {
        final TablaRetencion tabla = tr.geTablaRetencion();
        final Boolean editar = !ObjectUtil.isNull(tabla.getId()) ? Boolean.TRUE : Boolean.FALSE;
        tablaRetencionInterface.save(tabla);
        final CuadroClasificacion cc = cuadroClasificacionInterface.findById(tr.getClasificacionId());
        cc.setTablaRetencion(tabla);
        cuadroClasificacionInterface.save(cc);
        bitacora.crear(editar ? TipoAccion.UPDATE: TipoAccion.CREATE, user, "TRD:" + cc.getNombreNivel());
        return new Respuesta<Boolean>(Boolean.TRUE, 0);
    }

    @GetMapping(value = "/get/{id}")
    public Respuesta<CuadroClasificacionPojo> obtenerClientePorId(@PathVariable Long id) {
        final TablaRetencion tr = tablaRetencionInterface.findById(id);
        final CuadroClasificacionPojo ccp = new CuadroClasificacionPojo(tr);
        final Clasificacion clas = clasificacionInterface
                .buscaUltimaClasificacionPorInstitucionId(tr.getInstitucion().getId());
        ccp.setCodigoReferencia(clas.getCodigoReferencia());
        ccp.setVersion(clas.getVersion());
        return new Respuesta<CuadroClasificacionPojo>(ccp, 0);
    }
}
