package cl.an.tde.api.integracion.rest.repo.pit;

import cl.an.tde.entities.PIT;
import cl.an.tde.entities.Propiedad;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PITRepository extends CrudRepository<PIT, Long> {

	@Query("SELECT p FROM PIT p WHERE p.adt.id=:adtId ORDER BY p.fecha DESC")
	List<PIT> obtenerPitByAdT(@Param("adtId") Long adtId);

	@Query("SELECT p "
			+ " FROM PIT p"
			+ " WHERE p.uuid = :uuid")
	PIT findPitByUUID(@Param("uuid") UUID uuid);

	
}
