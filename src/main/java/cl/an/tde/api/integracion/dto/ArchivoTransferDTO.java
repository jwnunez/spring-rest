package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.util.Date;

import cl.an.tde.entities.Archivo;

public class ArchivoTransferDTO implements Serializable {

	private static final long serialVersionUID = 4700868975162732388L;

	private String uuid;
	private String nombre;
	private String tipo;
	private Long size;
	private String metadata;
	private String checksum;
	private Date fecha;
	private String pit;

	public ArchivoTransferDTO(Archivo a) {
		this.uuid = (a.getUuid()!=null)?a.getUuid().toString():"";
		this.nombre = a.getNombre();
		this.tipo = a.getTipo();
		this.size = a.getSize();
		this.metadata = a.getMetadata();
		this.checksum = a.getChecksum();
		this.fecha = a.getFecha();
		if ( a.getPIT() != null ) {
			this.pit = (a.getPIT().getUuid()!=null)?a.getPIT().getUuid().toString():"";
		}
	}

	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}

	public String getMetadata() {
		return metadata;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getPit() {
		return pit;
	}
	public void setPit(String pit) {
		this.pit = pit;
	}

}
