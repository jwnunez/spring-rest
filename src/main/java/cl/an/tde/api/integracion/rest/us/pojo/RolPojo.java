package cl.an.tde.api.integracion.rest.us.pojo;

import cl.an.tde.entities.Rol;

/**
 * @author rfuentes.
 */
public class RolPojo {

    private Long id;
    private String rol;

    /**
     * Constructor.
     */
    public RolPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param id
     * @param rol
     */
    public RolPojo(Long id, String rol) {
        super();
        this.id = id;
        this.rol = rol;
    }

    /**
     * Constructor con parametros.
     * 
     * @param rol {@link Rol}
     */
    public RolPojo(Rol rol) {
        super();
        this.id = rol.getId();
        this.rol = rol.getRol();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

}
