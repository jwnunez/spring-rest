package cl.an.tde.api.integracion.rest.institucion;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.an.tde.api.integracion.rest.institucion.pojo.InstitucionPojo;
import cl.an.tde.entities.Institucion;
import cl.an.tde.utils.QueryUtil;
import cl.an.tde.utils.StringUtil;

/**
 * @author bzamora
 */
@Service
public class InstitucionImpl implements InstitucionInterface {

    private static final Logger LOG = LoggerFactory.getLogger(ControladorInstitucion.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private InstitucionRepository institucionRepository;

    @Override
    public Institucion findById(Integer id) {
        return institucionRepository.findById(id).get();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<InstitucionPojo> findAll() {
        final StringBuffer select = new StringBuffer(
                "SELECT new cl.an.tde.api.integracion.rest.institucion.pojo.InstitucionPojo(i) FROM Institucion i ");
        final Query query = em.createQuery(select.toString());
        return (List<InstitucionPojo>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Institucion> findAll(FiltroInstitucion fi) {
        final StringBuffer select = new StringBuffer(
                "SELECT new cl.an.tde.api.integracion.rest.institucion.pojo.InstitucionPojo(i) FROM Institucion i ");
        this.getQueryBusqueda(fi, select);
        final Query query = em.createQuery(select.toString());
        this.setParametrosQuery(fi, query);
        LOG.info(query.toString());
        return (List<Institucion>) query.getResultList();
    }

	private void getQueryBusqueda(FiltroInstitucion fi, StringBuffer select) {
		select.append(" WHERE 1=1 ");
		if (!StringUtil.isEmpty(fi.getNombre()) ) {
			select.append(" AND " + QueryUtil.insensitive("i.nombre") + " LIKE " + QueryUtil.insensitive(":nombre"));
		}
		if (!StringUtil.isEmpty(fi.getSigla()) ) {
			select.append(" AND " + QueryUtil.insensitive("i.sigla") + " LIKE " + QueryUtil.insensitive(":sigla"));
		}
		if (!StringUtil.isEmpty(fi.getCodigo()) ) {
			select.append(" AND " + QueryUtil.insensitive("i.codigo") + " LIKE " + QueryUtil.insensitive(":codigo"));
		}
		select.append(" ORDER BY i.nombre");
	}

    private void setParametrosQuery(FiltroInstitucion fi, Query query) {
        if (!StringUtil.isEmpty(fi.getNombre()) ) {
            query.setParameter("nombre", "%" + fi.getNombre().toUpperCase() + "%");
        }
        if (!StringUtil.isEmpty(fi.getSigla()) ) {
            query.setParameter("sigla", "%" + fi.getSigla().toUpperCase() + "%");
        }
        if (!StringUtil.isEmpty(fi.getCodigo()) ) {
            query.setParameter("codigo", "%" + fi.getCodigo().toUpperCase() + "%");
        }
    }

}
