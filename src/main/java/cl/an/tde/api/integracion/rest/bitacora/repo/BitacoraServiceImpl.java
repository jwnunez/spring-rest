package cl.an.tde.api.integracion.rest.bitacora.repo;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import cl.an.tde.enums.TipoAccion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.an.tde.api.integracion.dto.BitacoraDTO;
import cl.an.tde.api.integracion.dto.BusquedaDTO;
import cl.an.tde.api.integracion.rest.bitacora.ControladorBitacora;
import cl.an.tde.entities.Bitacora;
import cl.an.tde.utils.Global;
import cl.an.tde.utils.StringUtil;

@Service
public class BitacoraServiceImpl implements BitacoraServiceInterface {
	
	private static final Logger LOG = LoggerFactory.getLogger(ControladorBitacora.class);
	
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private BitacoraRepository bitacoraRepository;
	
	
	public List<Bitacora> findAll() {
		return (List<Bitacora>) bitacoraRepository.findAll();
	}
	
	@SuppressWarnings("unchecked")
	public List<Bitacora> findLast() {
		final StringBuffer sql = new StringBuffer(
				"SELECT b FROM Bitacora b ORDER BY b.id DESC ");
		final Query query = em.createQuery(sql.toString());
		query.setMaxResults(10);
		return (List<Bitacora>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<BitacoraDTO> findFilter(BusquedaDTO params) {
		final StringBuffer jpql = new StringBuffer("SELECT");
		jpql.append(" new " + Global.PACKAGE_DTO + ".BitacoraDTO(b)");
		jpql.append(" FROM Bitacora b");
		this.setCondiciones(jpql, params);
		final Query query = em.createQuery(jpql.toString());
		this.setParametrosQuery(query, params);
		List<BitacoraDTO> dto = (List<BitacoraDTO>) query.getResultList();
		return dto;
	}

	@Override
	public List observaciones(Long idAdt) {
		final StringBuffer jpql = new StringBuffer("SELECT b.observacion FROM BitacoraAdT b ");
		jpql.append(" WHERE b.adt.id = :idAdt ");
		jpql.append(" AND b.tipoAccion = :rejected ");
		jpql.append(" ORDER BY b.id ASC ");
		final Query query = em.createQuery(jpql.toString());
		query.setParameter("idAdt", idAdt);
		query.setParameter("rejected", TipoAccion.REJECTED.getValor().shortValue());
		return query.getResultList();
	}

	private void setCondiciones(StringBuffer jpql, BusquedaDTO params) {
		jpql.append(" WHERE 1=1");
		if ( params.getIdInstitucion() != null ) {
			jpql.append(" AND b.idInstitucion = :idInstitucion");
		}
		if ( params.getIdUsuario() != null ) {
			jpql.append(" AND b.idUsuario = :idUsuario");
		}
		if ( !StringUtil.isEmpty(params.getFechaDesde()) ) {
			jpql.append(" AND CAST(b.fecha AS date) >= :fechaDesde");
		}
		if ( !StringUtil.isEmpty(params.getFechaHasta()) ) {
			jpql.append(" AND CAST(b.fecha AS date) <= :fechaHasta");
		}
		if ( params.getTipoAccion() != null ) {
			jpql.append(" AND b.tipoAccion = :tipoAccion");
		}
		jpql.append(" ORDER BY b.fecha DESC");
	}
	
	private void setParametrosQuery(Query query, BusquedaDTO params) {
		if ( params.getIdInstitucion() != null ) {
			query.setParameter("idInstitucion", params.getIdInstitucion());
		}
		if ( params.getIdUsuario() != null ) {
			query.setParameter("idUsuario", params.getIdUsuario());
		}
		if ( !StringUtil.isEmpty(params.getFechaDesde()) ) {
			query.setParameter("fechaDesde", Date.valueOf(params.getFechaDesde()));
		}
		if ( !StringUtil.isEmpty(params.getFechaHasta()) ) {
			query.setParameter("fechaHasta", Date.valueOf(params.getFechaHasta()));
		}
		if ( params.getTipoAccion() != null ) {
			query.setParameter("tipoAccion", params.getTipoAccion());
		}
	}

}
