package cl.an.tde.api.integracion.rest.exception;

import java.util.Date;

/**
 * @author rfuentes.
 */
public class ErrorMsg {

    private Date fecha;
    private String mensaje;
    private String detalle;
    private String url;
    private Boolean redirec = Boolean.FALSE;

    /**
     * Constructor.
     */
    public ErrorMsg() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param fecha {@link Date}
     * @param mensaje {@link String}
     * @param detalle {@link String}
     */
    public ErrorMsg(Date fecha, String mensaje, String detalle) {
        super();
        this.fecha = fecha;
        this.mensaje = mensaje;
        this.detalle = detalle;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getRedirec() {
        return redirec;
    }

    public void setRedirec(Boolean redirec) {
        this.redirec = redirec;
    }

}
