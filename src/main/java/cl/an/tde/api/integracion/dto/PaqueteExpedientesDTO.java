package cl.an.tde.api.integracion.dto;

import java.io.Serializable;
import java.util.List;

public class PaqueteExpedientesDTO implements Serializable {

	private static final long serialVersionUID = -4960934336907449752L;

	private Integer serie;
	private String desc;
	private List<ExpedienteDTO> expedientes;

	public Integer getSerie() {
		return serie;
	}
	public void setSerie(Integer serie) {
		this.serie = serie;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public List<ExpedienteDTO> getExpedientes() {
		return expedientes;
	}
	public void setExpedientes(List<ExpedienteDTO> expedientes) {
		this.expedientes = expedientes;
	}

}
