package cl.an.tde.api.integracion.rest.repo.cc.man;

import org.springframework.data.repository.CrudRepository;

import cl.an.tde.entities.CuadroClasificacion;

/**
 * @author rfuentes
 *
 */
public interface CuadroClasificacionRepository extends CrudRepository<CuadroClasificacion, Long> {

}
