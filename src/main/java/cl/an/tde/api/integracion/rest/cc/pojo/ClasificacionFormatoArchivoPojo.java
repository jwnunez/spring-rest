package cl.an.tde.api.integracion.rest.cc.pojo;

import java.io.Serializable;

/**
 * @author rfuentes
 */
public class ClasificacionFormatoArchivoPojo implements Serializable {

    private static final long serialVersionUID = -4984929165115655312L;
    private Long idCfa;
    private Integer formatoArchivoId;
    private String extension;
    private Boolean estado;
    private Boolean value;

    /**
     * Constructor.
     */
    public ClasificacionFormatoArchivoPojo() {
        super();
    }

    /**
     * Constructor con parametros.
     * 
     * @param formatoArchivoId
     * @param extension
     */
    public ClasificacionFormatoArchivoPojo(Integer formatoArchivoId, String extension) {
        super();
        this.formatoArchivoId = formatoArchivoId;
        this.extension = extension;
    }

    /**
     * Constructor con parametros.
     * 
     * @param idCfa
     * @param formatoArchivoId
     * @param extension
     * @param estado
     */
    public ClasificacionFormatoArchivoPojo(Long idCfa, Integer formatoArchivoId, String extension, Boolean estado) {
        super();
        this.idCfa = idCfa;
        this.formatoArchivoId = formatoArchivoId;
        this.extension = extension;
        this.estado = estado;
    }

    public Long getIdCfa() {
        return idCfa;
    }

    public void setIdCfa(Long idCfa) {
        this.idCfa = idCfa;
    }

    public Integer getFormatoArchivoId() {
        return formatoArchivoId;
    }

    public void setFormatoArchivoId(Integer formatoArchivoId) {
        this.formatoArchivoId = formatoArchivoId;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

}
