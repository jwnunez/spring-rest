package cl.an.tde.api.integracion.dto;

import java.io.Serializable;

public class MetadataDTO implements Serializable {

	private static final long serialVersionUID = -5643787778173692355L;

	private Integer id;
	private String nombre;
	private String definicion;
	private String tipo;

	public MetadataDTO() {
		super();
	}

	public MetadataDTO(Integer id, String nombre, String definicion, String tipo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.definicion = definicion;
		this.tipo = tipo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDefinicion() {
		return definicion;
	}

	public void setDefinicion(String definicion) {
		this.definicion = definicion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
