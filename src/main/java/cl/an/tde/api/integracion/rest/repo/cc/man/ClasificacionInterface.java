package cl.an.tde.api.integracion.rest.repo.cc.man;

import cl.an.tde.entities.Clasificacion;

/**
 * @author rfuentes
 *
 */
public interface ClasificacionInterface {

    Clasificacion findById(Long id);

    Clasificacion buscaUltimaClasificacionPorInstitucionId(Integer institucionId);

    Clasificacion save(Clasificacion clasificacion);

}
