package cl.an.tde.api.integracion.rest.repo.cc.man;

import org.springframework.data.repository.CrudRepository;

import cl.an.tde.entities.Clasificacion;

/**
 * @author rfuentes
 *
 */
public interface ClasificacionRepository extends CrudRepository<Clasificacion, Long>{

}
