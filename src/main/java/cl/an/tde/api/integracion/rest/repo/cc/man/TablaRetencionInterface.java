package cl.an.tde.api.integracion.rest.repo.cc.man;

import cl.an.tde.entities.TablaRetencion;

/**
 * @author rfuentes
 *
 */
public interface TablaRetencionInterface {

    TablaRetencion findById(Long id);

    TablaRetencion save(TablaRetencion tr);

}
