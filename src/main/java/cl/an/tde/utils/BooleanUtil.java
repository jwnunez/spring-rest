package cl.an.tde.utils;

public class BooleanUtil {
	
	public static final Character TRUE_CHARACTER = 'S';
	public static final Character FALSE_CHARACTER = 'N';
	
	public static final Character valueOfCharacter(Boolean value) {
		if (!ObjectUtil.isNull(value)) {
			if (value)
				return BooleanUtil.TRUE_CHARACTER;
		}
		return BooleanUtil.FALSE_CHARACTER;
	}

	public static final Boolean valueOf(Character character) {
		if (!ObjectUtil.isNull(character)) {
			if (TRUE_CHARACTER.equals(character))
				return true;
		}
		return false;
	}
	
}
