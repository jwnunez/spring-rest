package cl.an.tde.utils;

public final class IntegerUtil extends ObjectUtil {
	
	public static final boolean isEmpty(Integer integer, boolean ifZero) {
		if (integer == null)
			return true;
		if (isZero(integer))
			return ifZero;
		else
			return false;
	}
	
	public static final boolean isEmpty(Integer integer) {
		return isEmpty(integer, false);
	}
	
	public static final boolean isZero(Integer integer) {
		if (isNull(integer))
			return false;
		else if (integer.equals(0))
			return true;
		else
			return false;
	}
}
