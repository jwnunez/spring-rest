package cl.an.tde.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.loc.repository.bagit.creator.BagCreator;
import gov.loc.repository.bagit.domain.Bag;
import gov.loc.repository.bagit.exceptions.CorruptChecksumException;
import gov.loc.repository.bagit.exceptions.FileNotInPayloadDirectoryException;
import gov.loc.repository.bagit.exceptions.InvalidBagitFileFormatException;
import gov.loc.repository.bagit.exceptions.InvalidPayloadOxumException;
import gov.loc.repository.bagit.exceptions.MaliciousPathException;
import gov.loc.repository.bagit.exceptions.MissingBagitFileException;
import gov.loc.repository.bagit.exceptions.MissingPayloadDirectoryException;
import gov.loc.repository.bagit.exceptions.MissingPayloadManifestException;
import gov.loc.repository.bagit.exceptions.UnparsableVersionException;
import gov.loc.repository.bagit.exceptions.UnsupportedAlgorithmException;
import gov.loc.repository.bagit.exceptions.VerificationException;
import gov.loc.repository.bagit.hash.StandardSupportedAlgorithms;
import gov.loc.repository.bagit.reader.BagReader;
import gov.loc.repository.bagit.verify.BagVerifier;
import gov.loc.repository.bagit.writer.BagWriter;

public class BagIt {

	private static final Logger LOG = LoggerFactory.getLogger(BagIt.class);
	
	private static boolean includeHiddenFiles = false;
	private static boolean ignoreHiddenFiles = !includeHiddenFiles;
	private static StandardSupportedAlgorithms algorithm = StandardSupportedAlgorithms.MD5;
	
	private static BagVerifier verifier = new BagVerifier();

	public static void createBag(String ruta) {
		createBag(Paths.get(ruta));
	}

	public static void createBag(Path path) {
		try {
			BagCreator.bagInPlace(path, Arrays.asList(algorithm), includeHiddenFiles);
		} catch (
				NoSuchAlgorithmException |
				IOException e
				) {
			LOG.error(e.getMessage());
		}
	}
	
	public static Bag readBag(String rutaBag) {
		Path rootDir = Paths.get(rutaBag);
		BagReader reader = new BagReader();
		try {
			Bag bag = reader.read(rootDir);
			return bag;
		} catch (
				IOException |
				UnparsableVersionException |
				MaliciousPathException |
				UnsupportedAlgorithmException |
				InvalidBagitFileFormatException e
				) {
			LOG.error(e.getMessage());
		}
		return null;
	}
	
	public static void writeBag(String rutaBag, String rutaWrite) {
		Path outputDir = Paths.get(rutaWrite);
		Bag bag = readBag(rutaBag);
		try {
			BagWriter.write(bag, outputDir);
		} catch (
				NoSuchAlgorithmException |
				IOException e
				) {
			LOG.error(e.getMessage());
		}
	}
	
	public static boolean verifyBag(String rutaBag) {
		Bag bag = readBag(rutaBag);
		try {
			verifier.isComplete(bag, ignoreHiddenFiles);
			return true;
		} catch (
				IOException |
				MissingPayloadManifestException |
				MissingBagitFileException |
				MissingPayloadDirectoryException |
				FileNotInPayloadDirectoryException |
				InterruptedException |
				MaliciousPathException |
				UnsupportedAlgorithmException |
				InvalidBagitFileFormatException e
				) {
			LOG.error(e.getMessage());
		}
		return false;
	}
	
	public static Boolean quicklyVerifyBag(String rutaBag) {
		Bag bag = readBag(rutaBag);
		if ( BagVerifier.canQuickVerify(bag) ){
			try {
				BagVerifier.quicklyVerify(bag);
				return Boolean.TRUE;
			} catch (
					IOException |
					InvalidPayloadOxumException e
					) {
				LOG.error(e.getMessage());
			}
			return Boolean.FALSE;
		}
		return null;
	}
	
	public static boolean validBag(String rutaBag) {
		Bag bag = readBag(rutaBag);
		try {
			verifier.isValid(bag, ignoreHiddenFiles);
			return true;
		} catch (
				IOException |
				MissingPayloadManifestException |
				MissingBagitFileException |
				MissingPayloadDirectoryException |
				FileNotInPayloadDirectoryException |
				InterruptedException |
				MaliciousPathException |
				CorruptChecksumException |
				VerificationException |
				UnsupportedAlgorithmException |
				InvalidBagitFileFormatException e
				) {
			LOG.error(e.getMessage());
		}
		return false;
	}

}
