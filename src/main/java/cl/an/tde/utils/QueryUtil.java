package cl.an.tde.utils;

public class QueryUtil {

	/**
	* Agrega signo '%' al inicio y al final de la cadena
	*/
	public static String concatPercents(String input) {
		return Global.PORCENTAJE.concat(input).concat(Global.PORCENTAJE);
	}

	/**
	* Agrega UPPER y TRASLATE al parámetro indicado
	*/
	public static String insensitive(String input) {
		return " UPPER(TRANSLATE("+ input + ",'ÀÁáàÉÈéèÍíÓóÒòÚú','AAaaEEeeIiOoOoUu')) ";
	}

}
