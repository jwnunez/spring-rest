package cl.an.tde.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;

public class FileUtil {

	protected static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);

	public static byte[] recuperarBytesArchivo(File file) throws IOException {
		long length = file.length();
		
		if (length > Integer.MAX_VALUE) {
			throw new IOException("Archivo demasiado grande");
		}
		
		byte[] bytes = new byte[(int) length];
		int offset = 0;
		int numRead = 0;
		InputStream is = new FileInputStream(file);
		
		try {
			while ((offset < bytes.length) && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}
		} finally {
			is.close();
		}
		
		if (offset < bytes.length) {
			throw new IOException("No se pudo leer completamente el archivo "
					+ file.getName());
		}
		
		return bytes;
	}

	public static byte[] recuperarBytesEntrada(InputStream is) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length = 0;

		while ((length = is.read(buffer)) != -1) {
			baos.write(buffer, 0, length);
		}
		
		return baos.toByteArray();
	}
	
	public static void crearDirectorio(String ruta) throws Exception {
		File dir = new File(ruta);
		
		if (!dir.exists()) {
			try {
				dir.mkdir();
			} catch (SecurityException se) {
				throw new Exception("No fue posible crear el directorio: " + ruta, se);
			}
		}
	}
		
	public static void generarArchivo(byte [] datos, String ruta) throws Exception {
		BufferedOutputStream bs = null;
		File file = new File(ruta);
		
		boolean existeRuta = file.getParentFile().exists();
		
		if (!existeRuta) {
			boolean resultado = file.getParentFile().mkdirs();
			if (!resultado) 
				throw new Exception("No fue posible obtener la siguente ruta: " + file.getParentFile());
		}
		
		try {
		    FileOutputStream fs = new FileOutputStream(file);
		    bs = new BufferedOutputStream(fs);
		    bs.write(datos);
		    
		    bs.close();
		    bs = null;
		} catch (FileNotFoundException fnfe) {
		    throw new Exception("No se encuentra la siguente ruta: " + ruta, fnfe);
		} catch (IOException e) {
			throw new Exception("No se pudo escribir el archivo en: " + ruta, e);
		} finally {
			if (bs != null) try { bs.close(); } catch (Exception e) {}
		}
	}
	
	public static void generarArchivo(InputStream uploadedInputStream, String uploadedFileLocation) throws Exception {
		
		try {
			OutputStream out = new FileOutputStream(new File(
					uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new Exception("No se pudo escribir el archivo en: " + uploadedFileLocation, e);
		}
	}
	
	public static void renombrarArchivo(String rutaActual, String rutaNueva) throws Exception {
		File fileOld = new File(rutaActual), fileNew = new File(rutaNueva);

		try {
			if (fileNew.exists()) throw new java.io.IOException("Ya existe el archivo: " + rutaNueva);

			fileOld.renameTo(fileNew);
		} catch (Exception e) {
			throw new Exception("No fue posible cambiar el nombre del archivo: " + rutaActual, e);
		}
	}
	
	public static void borrarArchivo(String ruta) throws Exception {
		File file = new File(ruta);

		try {
			if (file.exists()) {
				file.delete();
			}
		} catch (Exception e) {
			throw new Exception("No fue posible eliminar el archivo: " + ruta, e);
		}
	}
	
	public static String obtenerUltimaLineaArchivo(byte[] datos) throws Exception {
		ByteArrayInputStream bais = new ByteArrayInputStream(datos);
		BufferedReader br = new BufferedReader(new InputStreamReader(bais));
		String ultima = null, linea = null;

		try {
			while ((linea = br.readLine()) != null) {
				ultima = linea;
			}
		} catch (IOException e) {
			throw new Exception("Error al leer las líneas desde los datos", e);
		}

		return ultima;
	}
	
	public static String leeArchivo(String filename) throws UnsupportedEncodingException {
		File f = new File(filename);
		byte[] bytes = null;
		try {
			 bytes = Files.readAllBytes(f.toPath());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bytes !=null && !"".equals(bytes)? new String(bytes, "UTF-8"):"";
	}

	public static boolean copyDirectory(String source, String destination) {
		LOG.info("-> FileUtil.copyDirectory");
		LOG.info("-> Source: " + source);
		LOG.info("-> Destin: " + destination);
		File srcDir = new File(source);
		File destDir = new File(destination);
		
		// Copy the source directory to the destination directory.
		// The destination directory must not exists prior to the copy process.
		try {
			FileUtils.copyDirectory(srcDir, destDir);
		} catch (IOException e) {
			LOG.error(e.getMessage());
			return false;
		}
		return true;
	}

	public static boolean moveDirectory(String source, String destination) {
		LOG.info("-> FileUtil.moveDirectory");
		LOG.info("-> Source: " + source);
		LOG.info("-> Destin: " + destination);
		File srcDir = new File(source);
		File destDir = new File(destination);
		
		// Move the source directory to the destination directory.
		// The destination directory must not exists prior to the move process.
		try {
			FileUtils.moveDirectory(srcDir, destDir);
		} catch (IOException e) {
			LOG.error(e.getMessage());
			return false;
		}
		return true;
	}

}
