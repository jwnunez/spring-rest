package cl.an.tde.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

public class NumberUtil {

	public static double redondearMedioInclusive(double numero, int cifras) {
		BigDecimal bd = new BigDecimal(numero).setScale(cifras,
				RoundingMode.HALF_EVEN);

		return bd.doubleValue();
	}

	public static double redondearHaciaArriba(double numero, int cifras) {
		BigDecimal bd = new BigDecimal(numero)
				.setScale(cifras, RoundingMode.UP);

		return bd.doubleValue();
	}

	public static double redondearHaciaAbajo(double numero, int cifras) {
		BigDecimal bd = new BigDecimal(numero).setScale(cifras,
				RoundingMode.FLOOR);

		return bd.doubleValue();
	}

	public static boolean esNumeroEntero(String valor) {
		if (valor == null) return false;
		
		try {
			Long.parseLong(valor);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	public static boolean esNumeroDecimal(String valor) {
		if (valor == null) return false;
		
		try {
			Double.parseDouble(valor);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	
	public static Double obtenerMaximoDoble(Double...numeros) {
		if (numeros.length == 0)
			return null;
		else if (numeros.length == 1) 
			return numeros[0];
		else {
			Double[] params = new Double[numeros.length - 1];
			
			params[0] = Math.max(numeros[0], numeros[1]);
			
			if (numeros.length > 2) {
				numeros = Arrays.copyOfRange(numeros, 2, numeros.length);
			
				for (int i = 0; i < numeros.length; i++) {
					params[i + 1] = numeros[i];
				}
			}
			
			return obtenerMaximoDoble(params);
		}
	}
	
}
