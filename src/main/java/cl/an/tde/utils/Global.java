package cl.an.tde.utils;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import cl.an.tde.api.integracion.dto.ComboDTO;

public class Global {

	public static final String APP_VERSION = Global.class.getPackage().getImplementationVersion();
	
	public static final String SERVICE_ID = ServletUriComponentsBuilder.fromCurrentContextPath().toUriString();
	
	public static final String PACKAGE_DTO = new ComboDTO().getClass().getPackage().getName();
	
	public static final String PORCENTAJE = "%";
	
}
