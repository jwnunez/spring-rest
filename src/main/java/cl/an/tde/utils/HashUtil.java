package cl.an.tde.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.io.Files;

public class HashUtil {

	private static final Logger LOG = LoggerFactory.getLogger(HashUtil.class);

	public static UUID getUUID() {
		return UUID.randomUUID();
	}

	public static String getUniqueUUID() {
		MessageDigest salt = null;
		try {
			salt = MessageDigest.getInstance("SHA-256");
			salt.update(UUID.randomUUID().toString().getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			LOG.error(e.getMessage());
		}
		return bytesToHex(salt.digest());
	}

	public static String bytesToHex(final byte[] hashInBytes) {
		final StringBuilder sb = new StringBuilder();
		for (byte b : hashInBytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

	public static String getMD5File(MultipartFile file) {
		try {
			return DigestUtils.md5Hex(file.getInputStream());
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		return null;
	}

	public static boolean verifyMD5Checksum(MultipartFile[] files, String[] checksum) {
		LOG.info("-- Verificando MD5 Checksum Archivos --");
		if (files.length != checksum.length) {
			LOG.error(" -> ERROR length Files={} Chks={}", files.length, checksum.length);
			return false;
		}
		for (int i = 0; i < files.length; i++) {
			final String chks = HashUtil.getMD5File(files[i]);
			if (!checksum[i].equals(chks)) {
				LOG.error(" -> ERROR Cheksum File={}", files[i].getOriginalFilename());
				return false;
			}
			LOG.info(" -> OK Checksum File={}", files[i].getOriginalFilename());
		}
		LOG.info("-- MD5 Checksums Archivos Correctos --");
		return true;
	}

	public static String getFileChecksum(File file) throws IOException, NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("MD5");

		byte[] byteArchivo = Files.toByteArray(file);
		// Get the hash's bytes
		byte[] bytes = digest.digest(byteArchivo);

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		// return complete hash
		return sb.toString();
	}

}
