package cl.an.tde.utils;

import java.io.Serializable;

/**
 * @author rfuentes
 *
 */
public class ComunType implements Serializable {

	private static final long serialVersionUID = 9015227782313830875L;
	private String entity;
	private String llave;
	private String valor;
	private Boolean activado;

	/**
	 * Constructor.
	 */
	public ComunType() {
		super();
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getLlave() {
		return llave;
	}

	public void setLlave(String llave) {
		this.llave = llave;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Boolean getActivado() { return activado; }

	public void setActivado(Boolean activado) { this.activado = activado; }
}
