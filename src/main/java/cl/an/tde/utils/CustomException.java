package cl.an.tde.utils;

public class CustomException extends Exception {
	
	private static final long serialVersionUID = -7787794229618993018L;

	private Integer id;
	private String message;
	
	public CustomException(Integer id, String message) {
		super();
		this.id = id;
		this.message = message;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
