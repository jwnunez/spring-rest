package cl.an.tde.utils;

import java.util.List;

/**
 * Clase utilizada para Retornar Lista de Objetos con el Numero de Elementos en un Servicio.
 * 
 * @author rfuentes.
 * @param <T> tipo de dato.
 */
public class Lista<T> {

    private List<T> items;
    private Long total;

    /**
     * Constructor.
     */
    public Lista() {
        super();
    }

    /**
     * Constructor que recibe la Lista de objetos y el Numetro Total de Estos.
     * 
     * @param items Lista de Objetos
     * @param total Numero total de Objetos en la Lista
     */
    public Lista(final List<T> items, final Long total) {
        super();
        this.items = items;
        this.total = total;
    }

    /**
     * Retorna la Lista de Objectos.
     * 
     * @return List<T>
     */
    public List<T> getItems() {
        return items;
    }

    /**
     * Setea la Lista de objetos.
     * 
     * @param items Lista de Objetos
     */
    public void setItems(List<T> items) {
        this.items = items;
    }

    /**
     * Retorna el Numero total de objetos contenidos en la Lista.
     * 
     * @return Integer
     */
    public Long getTotal() {
        return total;
    }

    /**
     * Setea el Numero Total de objetos contenido en la Lista.
     * 
     * @param total Numero total de objetos contenidos en la Lista
     */
    public void setTotal(Long total) {
        this.total = total;
    }

}
