package cl.an.tde.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZipUtil {

	private static final Logger LOG = LoggerFactory.getLogger(ZipUtil.class);

	public static void packagePIT(String sourceDir) throws IOException {
		LOG.info("-> packagePIT: " + sourceDir);
		ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(sourceDir.concat(".zip")));
		zipFile.setLevel(ZipOutputStream.STORED);
		directoryToZipFile(sourceDir, sourceDir, zipFile);
		IOUtils.closeQuietly(zipFile);
	}

	private static void directoryToZipFile(String rootDir, String source, ZipOutputStream out) throws IOException {
		LOG.info("-> directoryToZipFile: " + source);
		for ( File file : new File(source).listFiles() ) {
			LOG.info("-> File: " + file.getName());
			if ( file.isDirectory() ) {
				directoryToZipFile(rootDir, source + File.separator + file.getName(), out);
			} else {
				final String zipEntryPath = source.replace(rootDir, "");
				ZipEntry entry = null;
				if ( zipEntryPath.isEmpty() ) {
					entry = new ZipEntry(file.getName());
				} else {
					entry = new ZipEntry(source.replace(rootDir + File.separator, "") + File.separator + file.getName());
				}
				out.putNextEntry(entry);
				
				FileInputStream in = new FileInputStream(source + File.separator + file.getName());
				IOUtils.copy(in, out);
				IOUtils.closeQuietly(in);
			}
		}
	}

}
