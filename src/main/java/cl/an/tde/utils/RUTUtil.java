package cl.an.tde.utils;

import java.util.StringTokenizer;


public class RUTUtil {

	public static String obtenerRUT(String rut) {
		String aux = null;
		
		if (rut != null) {
			aux = rut.replaceAll("\\.", "");
			
			StringTokenizer st = new StringTokenizer(aux, "-");
			
			if (st.hasMoreTokens())
				return st.nextToken();
			else 
				return null;
		} else {
			return null;
		}
	}

	public static String obtenerDV(String rut) {		
		if (rut != null) {			
			StringTokenizer st = new StringTokenizer(rut, "-");
			
			if (st.hasMoreTokens()) {
				st.nextToken();
				
				if (st.hasMoreTokens())
					return st.nextToken();
				else 
					return null;
			} else 
				return null;
		} else {
			return null;
		}
	}
	
	public static Long obtenerNumeroRUT(String rut) {
		String numeroRUT = obtenerRUT(rut);
		
		if (numeroRUT != null) {
			try {
				return Long.parseLong(numeroRUT);
			} catch (NumberFormatException nfe) {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public static String calcularDV(Long rut) {
		if (rut != null) {
			long m = 0, s = 1;
			
			for (; rut != 0; rut /= 10) {
				s = (s + rut % 10 * (9 - m++ % 6)) % 11;
			}

			long dv = (s != 0 ? s + 47 : 75);

			return String.valueOf((char) dv);
		} else {
			return null;
		}
	}
	
	public static boolean validarRut(String rut) throws Exception {
		boolean validacion = false;
		
		if (rut != null) {
			try {
				rut = rut.toUpperCase();
				rut = rut.replace(".", "");
				rut = rut.replace("-", "");
	
				long rutAux = Long.parseLong(rut.substring(0, rut.length() - 1));
				
				String dv = String.valueOf(rut.charAt(rut.length() - 1));
				
				if (dv != null) {
					String dvAux = calcularDV(rutAux);
			
					validacion = dv.equals(dvAux);
				}
			} catch (NumberFormatException e) {
				throw new Exception("Error de formato en el RUT");
			}
		}	
		
		return validacion;
	}
}
