package cl.an.tde.utils;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import cl.an.tde.api.integracion.dto.ArchivoExpediente;
import cl.an.tde.api.integracion.dto.Documento;
import cl.an.tde.entities.Archivo;
import cl.an.tde.entities.Expediente;

public class JsonUtil {

	private static final Logger LOG = LoggerFactory.getLogger(JsonUtil.class);

	public static JsonObject stringToJson(String str) {
		LOG.info("-> stringToJson: " + str);
		final JsonObject converted = new Gson().fromJson(str, JsonObject.class);
		return converted;
	}

	public static JsonArray stringToJsonArray(String str) {
		LOG.info("-> stringToJsonArray: " + str);
		final JsonArray converted = new Gson().fromJson(str, JsonArray.class);
		return converted;
	}

	public static JsonObject jsonAutoMetaPit(String productor) {
		LOG.info("-> jsonAutoMetaPit: " + productor);
		final JsonObject json = new JsonObject();
		json.addProperty("Productor", productor);
		json.addProperty("Identificación y versión del software", Global.APP_VERSION);
		return json;
	}

	public static JsonObject jsonAutoMetaFile(Archivo file) {
		LOG.info("-> jsonAutoMetaFile: " + file.toString());
		final JsonObject json = stringToJson(file.getMetadata());
		json.addProperty("Identificador", file.getUuid().toString());
		json.addProperty("checksum", file.getChecksum());
		json.addProperty("Volumen", file.getSize());
		json.addProperty("Formato", file.getTipo());
		return json;
	}

	public static JsonObject jsonAutoMetaDocumento(Documento doc) {
		LOG.info("-> jsonAutoMetaDocumento: " + doc.toString());
		final JsonObject json = new JsonObject();
		json.addProperty("titulo_documento", doc.getTitulo());
		json.addProperty("f_documento", doc.getFechaDocumento());

		if (!doc.getNombreInteresado().isEmpty())
			json.addProperty("interesado", doc.getNombreInteresado());

		if (!doc.getRutInteresado().isEmpty())
			json.addProperty("rut", doc.getRutInteresado());

		if (!doc.getAutor().isEmpty())
			json.addProperty("autor", doc.getAutor());

		if (!doc.getDestinatario().isEmpty())
			json.addProperty("destinatario", doc.getDestinatario());

		json.addProperty("digitalizado", doc.getDocumentoDigitalizado());
		json.addProperty("etiquetas_tematicas", doc.getEtiquetas());
		json.addProperty("etiquetas_geograficas_region", doc.getRegion());
		json.addProperty("etiquetas_geograficas_coumna", doc.getComuna());
		json.addProperty("nombre", doc.getNombreArchivo());
		json.addProperty("volumen", doc.getVolumen());
		json.addProperty("formato", doc.getFormato());
		return json;
	}

	public static JsonObject jsonAutoMetaExpediente(cl.an.tde.api.integracion.dto.Expediente exp) {
		LOG.info("-> jsonAutoMetaExpediente: " + exp.toString());
		final JsonObject json = new JsonObject();
		json.addProperty("titulo_expediente", exp.getTituloExpediente());
		json.addProperty("f_creacion", exp.getFechaCreacionExpediente());
		json.addProperty("f_finalizacion", exp.getFechaCreacionExpediente());

		if (!exp.getNombreInteresado().isEmpty())
			json.addProperty("interesado", exp.getNombreInteresado());

		if (!exp.getRutInteresado().isEmpty())
			json.addProperty("rut", exp.getRutInteresado());

		json.addProperty("etiquetas_tematicas", exp.getEtiquetas());
		json.addProperty("etiquetas_geograficas_region", exp.getRegion());
		json.addProperty("etiquetas_geograficas_coumna", exp.getComuna());
		return json;
	}

	public static JsonObject jsonAutoMetaExpedienteArchivo(ArchivoExpediente arch) {
		LOG.info("-> jsonAutoMetaExpedienteArchivo: " + arch.toString());
		final JsonObject json = new JsonObject();
		json.addProperty("titulo_documento", arch.getTitulo());
		json.addProperty("f_documento", arch.getFechaDocumento());

		if (!arch.getAutor().isEmpty())
			json.addProperty("autor", arch.getAutor());

		if (!arch.getDestinatario().isEmpty())
			json.addProperty("destinatario", arch.getDestinatario());

		json.addProperty("digitalizado", arch.getDocumentoDigitalizado());
		json.addProperty("nombre", arch.getNombreArchivo());
		json.addProperty("volumen", arch.getVolumen());
		json.addProperty("formato", arch.getFormato());

		return json;
	}

	public static JsonObject jsonAutoMetaExp(JsonObject json, Expediente e, Integer volumen) {
		LOG.info("-> jsonAutoMetaExp: ");
		json.addProperty("nombre", e.getNombre());
		json.addProperty("Volumen", volumen);
		return json;
	}

	public static String genSemiAutoMetadata(String fullFilename, String nivelAgrup) {
		LOG.info("-> genSemiAutoMetadata: " + fullFilename + " " + nivelAgrup);
		final JsonObject json = new JsonObject();
		json.addProperty("Título", FilenameUtils.getBaseName(fullFilename));
		json.addProperty("Nivel Agrupación", nivelAgrup);
		return json.toString();
	}

	public static String jsonAutoMetaFileUds(Archivo file) {
		LOG.info("-> jsonAutoMetaFile: " + file.toString());
		final JsonObject json = stringToJson(file.getMetadata());
		json.addProperty("Nombre-Archivo", file.getNombre());
		json.addProperty("Identificador", file.getUuid().toString());
		json.addProperty("Checksum", file.getChecksum());
		json.addProperty("Volumen", file.getSize());
		json.addProperty("Formato", file.getTipo());
		return json.toString();
	}

	public static String jsonAutoMetaFileUdsCSV(Archivo file) {
		LOG.info("-> jsonAutoMetaFileCSV: " + file.toString());
		final JsonObject json = stringToJson(file.getMetadata());
		json.addProperty("Checksum", file.getChecksum());
		json.addProperty("Volumen", file.getSize());
		json.addProperty("Formato", file.getTipo());
		return json.toString();
	}
}
