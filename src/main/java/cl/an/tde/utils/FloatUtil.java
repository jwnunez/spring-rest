package cl.an.tde.utils;

public class FloatUtil extends ObjectUtil{

	public static final boolean isEmpty(Float decimal, boolean ifZero) {
		if (decimal == null)
			return true;
		if (isZero(decimal))
			return ifZero;
		else
			return false;
	}
	
	public static final boolean isEmpty(Float decimal) {
		return isEmpty(decimal, false);
	}
	
	public static final boolean isZero(Float decimal) {
		if (isNull(decimal))
			return false;
		else if (decimal.equals(0))
			return true;
		else
			return false;
	}
}