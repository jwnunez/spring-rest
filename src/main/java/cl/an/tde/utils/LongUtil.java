package cl.an.tde.utils;

public class LongUtil extends ObjectUtil {

    public static final boolean isEmpty(Long numero, boolean ifZero) {
        if (numero == null ) {
            return true;
        }
        if (isZero(numero) ) {
            return ifZero;
        } else {
            return false;
        }
    }

    public static final boolean isEmpty(Long numero) {
        return isEmpty(numero, false);
    }

    public static final boolean isZero(Long numero) {
        if (isNull(numero) ) {
            return false;
        } else if (numero.equals(0) ) {
            return true;
        } else {
            return false;
        }
    }
}
