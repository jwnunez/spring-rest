package cl.an.tde;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class Servicio {

	protected static final Logger LOG = LoggerFactory.getLogger(Servicio.class);

	@PersistenceContext
	protected EntityManager em;

}