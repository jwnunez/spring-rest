package cl.an.tde;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;


import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

@Configuration
@SpringBootApplication
@EnableEurekaClient
@EnableResourceServer
@EntityScan("cl.an.tde.entities")
@Import(SecurityProblemSupport.class)
//@CrossOrigin("*")
public class Aplicacion extends ResourceServerConfigurerAdapter {

	private static final String[] SWAGGER_WHITELIST = {
			// Swagger UI
			"/v2/api-docs",
			"/swagger-resources",
			"/swagger-resources/**",
			"/configuration/ui",
			"/configuration/security",
			"/swagger-ui.html",
			"/webjars/**"
		};

	public static void main(String[] args) {
		SpringApplication.run(Aplicacion.class, args);
	}

	private final SecurityProblemSupport problemSupport;

    public Aplicacion(SecurityProblemSupport problemSupport) {
        this.problemSupport = problemSupport;
    }

    @Override
	public void configure(HttpSecurity http) throws Exception {
		http
			.csrf()
			.disable()
			.exceptionHandling()
			.authenticationEntryPoint(problemSupport)
			.accessDeniedHandler(problemSupport)
			.and()
			.headers()
			.frameOptions()
			.disable()
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.OPTIONS, "/uaa/login").permitAll()
			.antMatchers(HttpMethod.POST, "/uaa/login").permitAll()
			.antMatchers(HttpMethod.OPTIONS, "/api/login").permitAll()
			.antMatchers(HttpMethod.POST, "/api/login").permitAll()
			.antMatchers(SWAGGER_WHITELIST).permitAll()
//			.antMatchers(HttpMethod.POST, "/api/inicioTransferencia").permitAll()
//			.antMatchers(HttpMethod.OPTIONS, "/api/inicioTransferencia").permitAll()
//			.antMatchers(HttpMethod.GET, "/api/getformats").permitAll()
//			.antMatchers(HttpMethod.OPTIONS, "/api/getformats").permitAll()
//			.antMatchers(HttpMethod.GET, "/api/gettrs/").permitAll()
//			.antMatchers(HttpMethod.OPTIONS, "/api/gettrs/").permitAll()
//			.antMatchers(HttpMethod.GET, "/api/getadts/").permitAll()
//			.antMatchers(HttpMethod.OPTIONS, "/api/getadts/").permitAll()
//			.antMatchers(HttpMethod.GET, "/api/getseries/").permitAll()
//			.antMatchers(HttpMethod.OPTIONS, "/api/getseries/").permitAll()
			.anyRequest().authenticated();
			//.antMatchers("/usuario/**").access("#oauth2.hasScope('read')")
			//.antMatchers("/institution/**").access("#oauth2.hasScope('read')");
	}

}
