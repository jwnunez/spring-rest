package cl.an.tde.propiedades;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import cl.an.tde.api.repo.PropiedadRepository;

@Component
@Scope(value = "singleton")
public class PropiedadSingleton {
	private static final Logger LOG = LoggerFactory.getLogger(PropiedadSingleton.class);

	public static final String KEY_UPLOAD_PITS = "upload_pits";
	public static final String KEY_UPLOAD_STAGING = "upload_staging";
	public static final String KEY_MAX_SINGLE_UPLOAD = "max_single_upload";
	public static final String KEY_UPLOAD_PITS_GESTOR = "upload_pits_gestor";

	@Autowired
	private PropiedadRepository repoPropiedad;

	private String uploadPits;
	private String uploadStaging;
	private String maxSingleUpload;
	private String uploadPitsGestor;

	private PropiedadSingleton() {
		LOG.info("-> PropiedadSingleton...");
	}

	@PostConstruct
	public void load() {
		uploadPits = repoPropiedad.getProperty(KEY_UPLOAD_PITS).getValue();
		uploadStaging = repoPropiedad.getProperty(KEY_UPLOAD_STAGING).getValue();
		uploadPitsGestor = repoPropiedad.getProperty(KEY_UPLOAD_PITS_GESTOR).getValue();
		LOG.info(this.toString());
	}

	public String getUploadPits() {
		return uploadPits;
	}
	public void setUploadPits(String uploadPits) {
		this.uploadPits = uploadPits;
	}

	public String getUploadStaging() {
		return uploadStaging;
	}
	public void setUploadStaging(String uploadStaging) {
		this.uploadStaging = uploadStaging;
	}

	public String getMaxSingleUpload() {
		return maxSingleUpload;
	}
	public void setMaxSingleUpload(String maxSingleUpload) {
		this.maxSingleUpload = maxSingleUpload;
	}

	public String getUploadPitsGestor() {
		return uploadPitsGestor;
	}

	public void setUploadPitsGestor(String uploadPitsGestor) {
		this.uploadPitsGestor = uploadPitsGestor;
	}

	@Override
	public String toString() {
		return "PropiedadSingleton [uploadPits=" + uploadPits + ", uploadStaging=" + uploadStaging
				+ ", maxSingleUpload=" + maxSingleUpload + "]";
	}

}
